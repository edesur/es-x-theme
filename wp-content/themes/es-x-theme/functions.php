<?php

// =============================================================================
// FUNCTIONS.PHP
// -----------------------------------------------------------------------------
// Overwrite or add your own custom functions to X in this file.
// =============================================================================

// =============================================================================
// TABLE OF CONTENTS
// -----------------------------------------------------------------------------
//   01. Enqueue Parent Stylesheet
//   02. Additional Functions
// =============================================================================

// Enqueue Parent Stylesheet
// =============================================================================

add_filter( 'x_enqueue_parent_stylesheet', '__return_true' );

@ini_set('upload_max_filesize', '128M');
@ini_set('post_max_size', '128M');
@ini_set('memory_limit', '1024M');
@ini_set('max_execution_time', '600');

// Additional Functions
// =============================================================================

  // add_action( 'init', 'es_stop_heartbeat', 1 );
  function es_stop_heartbeat() {
  wp_deregister_script('heartbeat');
  }

  function es_disable_ie_compatability_mode() {
    echo '<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">';
}
add_action( 'wp_head', 'es_disable_ie_compatability_mode', -9);

  function es_x_theme_styles_footer() {

    if ( !is_admin() ) {

      wp_enqueue_style( 'es-x-theme-css', get_stylesheet_directory_uri() . '/framework/css/dist/site/stacks/es-integrity.css', array(), null, 'screen' );

      wp_enqueue_style( 'foundation-css', get_stylesheet_directory_uri() . '/framework/foundation/css/app.css', array(), null, 'screen' );

      // wp_enqueue_style( 'bootstrap-css', get_stylesheet_directory_uri() . '/framework/bootstrap/dist/css/bootstrap.min.css', array(), null, 'screen' );
    }

  }
  add_action( 'wp_enqueue_scripts', 'es_x_theme_styles_footer', 999999 );

  function es_x_acf_styles_footer() {

    if ( is_admin() ) {

      wp_enqueue_style( 'es-x-acf-css', get_stylesheet_directory_uri() . '/framework/foundation/css/es-acf.css' , array(), null, 'screen'  );
    }

  }
  add_action( 'admin_enqueue_scripts', 'es_x_acf_styles_footer', 999999 );


  function es_x_theme_scripts() {
// es_x_theme_scripts
    if ( !is_admin() ) {
      wp_enqueue_script( 'es-x-theme-js', get_stylesheet_directory_uri() . '/framework/js/dist/site/es-x-theme.js', array( 'jquery' ), false, true );
    }

  }
  add_action( 'wp_enqueue_scripts', 'es_x_theme_scripts' );
// es_x_theme_scripts

//  es_x_theme_scripts_buzon_sugerencias
  function es_x_theme_scripts_buzon_sugerencias() {

    if ( !is_admin() && is_page('buzon-de-sugerencias')) {
      wp_enqueue_script( 'es-x-theme-js-buzon-sugerencias', get_stylesheet_directory_uri() . '/framework/js/dist/site/es-x-theme-buzon-sugerencias.js', array( 'jquery' ),  false, true );
    }

  }
  add_action('wp_enqueue_scripts', 'es_x_theme_scripts_buzon_sugerencias');
//  es_x_theme_scripts_buzon_sugerencias

  //  es_x_theme_scripts_denuncia_de_fraude
    function es_x_theme_scripts_denuncia_de_fraude() {

      if ( !is_admin() && is_page('denuncia-de-fraude')) {
        // wp_enqueue_script( 'es-x-theme-js-backbone.marionette', get_stylesheet_directory_uri() . '/framework/js/dist/marionette/backbone.marionette.min.js', array('backbone'), true );
        wp_enqueue_script( 'es-x-theme-js-denuncia-fraude', get_stylesheet_directory_uri() . '/framework/js/dist/site/es-x-theme-denuncia-fraude.js', array( 'jquery' ), false,  true );
      }

    }
    add_action( 'wp_enqueue_scripts', 'es_x_theme_scripts_denuncia_de_fraude', 999999 );
  //  es_x_theme_scripts_denuncia_de_fraude

  //  es_x_theme_scripts_nuevo_servicio
    function es_x_theme_scripts_nuevo_servicio() {

      if ( !is_admin() && is_page('nuevo-servicio')) {
        wp_enqueue_script( 'es-x-theme-js-nuevo-servicio', get_stylesheet_directory_uri() . '/framework/js/dist/site/es-x-theme-nuevo-servicio.js', array( 'jquery' ), false, true );
      }

    }
    add_action( 'wp_enqueue_scripts', 'es_x_theme_scripts_nuevo_servicio' );
  //  es_x_theme_scripts_nuevo_servicio

  // Function to add googleanalytics.js to the site
  function es_x_theme_add_google_analytics() {

    if ( !is_admin()) {
      // Register googleanalytics.js file
        wp_register_script( 'es-x-theme-ga', get_stylesheet_directory_uri() . '/framework/js/dist/site/es-x-theme-ga.js', false, '1.01', true );

        // Enqueue the registered script file
        wp_enqueue_script('es-x-theme-ga');
    }

  }

  add_action('wp_enqueue_scripts', 'es_x_theme_add_google_analytics');

  // es_foundation_scripts
  function es_foundation_scripts() {

    if ( !is_admin() ) {

      wp_enqueue_script( 'what-input-js', get_stylesheet_directory_uri() . '/framework/foundation/node_modules/what-input/dist/what-input.js', array( 'jquery' ), false, true );
      wp_enqueue_script( 'foundation-sites-js', get_stylesheet_directory_uri() . '/framework/foundation/node_modules/foundation-sites/dist/js/foundation.min.js', array( 'jquery' ), false, true );
       wp_enqueue_script( 'foundation-js', get_stylesheet_directory_uri() . '/framework/foundation/js/app.js', array( 'jquery' ), false, true );
       wp_enqueue_script( 'es-app-js', get_stylesheet_directory_uri() . '/framework/foundation/js/es-app.js', array( 'jquery' ), false, true );
    }

  }
  add_action( 'wp_enqueue_scripts', 'es_foundation_scripts'/*, 9999999*/);
  // es_foundation_scripts

  //  es_x_theme_scripts_puntos_de_pago
    function es_x_theme_scripts_puntos_de_pago() {

      if ( !is_admin() && ( is_page('puntos-de-pago') || is_page('oficinas-comerciales-puntos-expresos') ) ) {
        wp_enqueue_script( 'es-js-puntos-pago', get_stylesheet_directory_uri() . '/framework/foundation/js/es-puntos-pago.js', array( 'jquery' ), false, false );
      }

    }
    add_action( 'wp_enqueue_scripts', 'es_x_theme_scripts_puntos_de_pago' );
  //  es_x_theme_scripts_puntos_de_pago


  //  es_x_theme_scripts_forms_validate
    function es_x_theme_scripts_forms_validate() {

      if ( !is_admin() && ( get_post_type() === 'servicio' || is_page('contacto') ) ) {
        wp_enqueue_script( 'es-js-form-validate', get_stylesheet_directory_uri() . '/framework/foundation/js/es-forms-validate.js', array( 'jquery' ), false, false );
      }

    }
    add_action( 'wp_enqueue_scripts', 'es_x_theme_scripts_forms_validate' );
  //  es_x_theme_scripts_forms_validate

    //  Create shortcode to add copyright year to Pro theme footer

    function es_footer_pro_copyright_year() {
      $year = date('Y');
      return $year;
    }
    add_shortcode( 'es_copyright_year', 'es_footer_pro_copyright_year');

  // Set Titles of custom post with ACF fields
function es_set_acf_title( $post_id ) {

  // bail early if no ACF data
    if( empty($_POST['acf']) ) {

        // return;

    }

  $es_post = array();

  $es_post['ID'] = $post_id;

  switch (get_post_type()) {
    case 'proceso-de-compras':
       $es_post['post_title'] = get_field('es_compras_proceso_compra_title') ;
       $es_post['post_name'] = get_field('es_compras_proceso_compra_id') ;
      break;

      case 'denuncia-fraude':
       $es_post['post_title'] = get_field('es_form_denuncia_fraude_name_full') ;
       break;

       case 'servicio':
        $es_post['post_title'] = get_field('es_servicio_title') ;
        $es_post['post_excerpt'] = get_field('es_servicio_excerpt') ;
        $es_post['post_name'] = get_field('es_servicio_title') ;
        break;

       case 'mantenimiento':
       $es_mantenimiento_week = get_field('es_mantenimiento_week');

       $es_matenimiento_date_start = reset($es_mantenimiento_week)['es_mantenimiento_date'];
       $es_matenimiento_date_end = end($es_mantenimiento_week)['es_mantenimiento_date'];

       $es_matenimiento_date_start =  strtotime($es_matenimiento_date_start);
       $es_matenimiento_date_end =  strtotime($es_matenimiento_date_end);
       $es_matenimiento_date_year =  $es_matenimiento_date_end;
       $es_mantenimiento_date_format = 'j \d\e F';

         $es_matenimiento_date_start =  date_i18n($es_mantenimiento_date_format, $es_matenimiento_date_start );
         $es_matenimiento_date_end = date_i18n( $es_mantenimiento_date_format, $es_matenimiento_date_end);
         $es_matenimiento_date_year = date_i18n('Y', $es_matenimiento_date_year);

       $es_matenimiento_title = 'Mantenimientos Programados del ' . $es_matenimiento_date_start . ' al ' . $es_matenimiento_date_end .' '. $es_matenimiento_date_year;

        $es_post['post_title'] = $es_matenimiento_title;
        $es_post['post_name'] = $es_matenimiento_title;
        break;

        case 'pregunta-frecuente':
          $es_pregunta_frecuente_title = get_field('es_preguntas_frecuentes_category');
          $es_post['post_title'] = $es_pregunta_frecuente_title;
          $es_post['post_name'] = $es_pregunta_frecuente_title;
         break;

        case 'transparencia-doc':
          $es_transparencia_doc_type_field = get_field('es_transparencia_documento_type');
          $es_transparencia_doc_type_name = $es_transparencia_doc_type_field->name;
          $es_transparencia_doc_type_desc = $es_transparencia_doc_type_field->description;
          $es_transparencia_doc_type_slug = $es_transparencia_doc_type_field->slug;
          $es_transparencia_doc_type_tax = $es_transparencia_doc_type_field->taxonomy;

          $es_transparencia_doc_type_parent  = get_term( $es_transparencia_doc_type_field->parent, $es_transparencia_doc_type_tax);

          // $es_transparencia_doc_type_title = ( $es_transparencia_doc_type_slug == 'estado-resultados' ) ? $es_transparencia_doc_type_desc : $es_transparencia_doc_type_name;

          if ($es_transparencia_doc_type_slug == 'estado-resultados' || $es_transparencia_doc_type_slug == 'estado-cuentas-suplidores' || $es_transparencia_doc_type_slug == 'plan-trabajo') {
              $es_transparencia_doc_type_title = $es_transparencia_doc_type_desc;
          } else {
              $es_transparencia_doc_type_title = $es_transparencia_doc_type_name;
          }

          // Title

          $es_transparencia_doc_title = get_field('es_transparencia_documento_title');

          // Laws
          $es_transparencia_doc_law_code = get_field('es_transparencia_documento_law_code');

           // Sworn Affidavit
          $es_transparencia_doc_affidavit_name = get_field('es_transparencia_documento_affidavit_name');
          $es_transparencia_doc_affidavit_position = get_field('es_transparencia_documento_affidavit_position');

          // Frequency
          $es_transparencia_doc_frecuency_field = get_field('es_transparencia_documento_frecuency');
          $es_transparencia_doc_frecuency = $es_transparencia_doc_frecuency_field['value'];

          // Date fields
          $es_transparencia_doc_date_field = get_field('es_transparencia_documento_date');
          $es_transparencia_doc_date_start_field = get_field('es_transparencia_documento_date_period_start');
          $es_transparencia_doc_date_end_field = get_field('es_transparencia_documento_date_period_end');

          $es_transparencia_doc_date = strtotime( $es_transparencia_doc_date_field );
          $es_transparencia_doc_date_start = strtotime( $es_transparencia_doc_date_start_field );
          $es_transparencia_doc_date_end = strtotime( $es_transparencia_doc_date_end_field );

          // Months
          $es_transparencia_doc_date_month = date_i18n('F', $es_transparencia_doc_date);
          $es_transparencia_doc_date_start_month = date_i18n('F', $es_transparencia_doc_date_start);
          $es_transparencia_doc_date_end_month = date_i18n('F', $es_transparencia_doc_date_end);

          // Years
          $es_transparencia_doc_date_year = date_i18n('Y', $es_transparencia_doc_date);
          $es_transparencia_doc_date_start_year = date_i18n('Y', $es_transparencia_doc_date_start);
          $es_transparencia_doc_date_end_year = date_i18n('Y', $es_transparencia_doc_date_end);

          $es_transparencia_doc_year_constitution = get_field('es_transparencia_documento_year_constitution');

          // Title & URL slug

          if ( $es_transparencia_doc_frecuency == 'tri-semestral' && $es_transparencia_doc_type_slug == 'plan-estrategico-institucional' ) {

            $es_post['post_title'] = $es_transparencia_doc_type_title . ' '. $es_transparencia_doc_date_start_year . ' - ' . $es_transparencia_doc_date_end_year;
            $es_post['post_name'] = $es_transparencia_doc_type_slug . ' '. $es_transparencia_doc_date_start_year . '-' . $es_transparencia_doc_date_end_year;

          } elseif ( $es_transparencia_doc_type_parent->slug == 'oficina-acceso-informacion' &&
                    ( $es_transparencia_doc_type_slug == 'oai-estructura-organizacional' ||
                       $es_transparencia_doc_type_slug == 'oai-manual-organizacion' ||
                       $es_transparencia_doc_type_slug == 'oai-manual-procedimiento' ||
                       $es_transparencia_doc_type_slug == 'oai-manual-funcionamiento')
           ) {

            $es_post['post_title'] = $es_transparencia_doc_title;
            $es_post['post_name'] = $es_transparencia_doc_type_name;

          } elseif ( $es_transparencia_doc_type_slug == 'declaraciones-juradas' ) {

            $es_post['post_title'] = 'Declaración Jurada de Patrimonio - ' . $es_transparencia_doc_affidavit_name;
            $es_post['post_name'] = 'declaracion-jurada-' . $es_transparencia_doc_affidavit_name;

          } elseif ($es_transparencia_doc_type_slug == 'constitucion-dominicana' ) {

            $es_post['post_title'] = $es_transparencia_doc_type_title . ' - ' . $es_transparencia_doc_year_constitution;
            $es_post['post_name'] = $es_transparencia_doc_type_title . '-' . $es_transparencia_doc_year_constitution;

          } elseif (($es_transparencia_doc_type_slug == 'leyes-base-legal' || $es_transparencia_doc_type_slug == 'leyes-marco-legal') ) {

            $es_post['post_title'] = 'Ley No. ' . $es_transparencia_doc_law_code;
            $es_post['post_name'] = 'ley-' . $es_transparencia_doc_law_code;

          }  elseif ( $es_transparencia_doc_type_slug == 'resoluciones-base-legal' || $es_transparencia_doc_type_slug == 'resoluciones-marco-legal' ) {

            $es_post['post_title'] = 'Resolución No. ' . $es_transparencia_doc_law_code;
            $es_post['post_name'] = 'resolucion-' . $es_transparencia_doc_law_code;

          }  elseif ($es_transparencia_doc_type_slug == 'reglamentos' || $es_transparencia_doc_type_slug == 'reglamentos-marco-legal') {

            $es_post['post_title'] = 'Reglamento No. ' . $es_transparencia_doc_law_code;
            $es_post['post_name'] = 'reglamento-' . $es_transparencia_doc_law_code;

          } elseif ($es_transparencia_doc_type_slug == 'decretos' ) {

            $es_post['post_title'] = 'Decreto No. ' . $es_transparencia_doc_law_code;
            $es_post['post_name'] = 'decreto-' . $es_transparencia_doc_law_code;

          } elseif ($es_transparencia_doc_type_slug == 'decretos-base-legal' ) {

            $es_post['post_title'] = 'Decreto No. ' . $es_transparencia_doc_law_code;
            $es_post['post_name'] = 'decreto-base-legal-' . $es_transparencia_doc_law_code;

          } elseif ($es_transparencia_doc_type_slug == 'normativas' ) {

            $es_post['post_title'] = 'Normativa No. ' . $es_transparencia_doc_law_code;
            $es_post['post_name'] = 'normativa-' . $es_transparencia_doc_law_code;

          } elseif ($es_transparencia_doc_type_slug == 'organigrama' ) {

            $es_post['post_title'] = $es_transparencia_doc_title;
            $es_post['post_name'] = 'estructura-organizativa-' . $es_transparencia_doc_title;

          } elseif ( $es_transparencia_doc_frecuency == 'tri-semestral' ) {

            if ( $es_transparencia_doc_date_start_year == $es_transparencia_doc_date_end_year ) {
              $es_post['post_title'] = $es_transparencia_doc_type_title . ' '. $es_transparencia_doc_date_start_month . ' - ' . $es_transparencia_doc_date_end_month . ' ' . $es_transparencia_doc_date_end_year;
              $es_post['post_name'] = $es_transparencia_doc_type_slug . '-' . $es_transparencia_doc_date_start_month . '-' . $es_transparencia_doc_date_end_month .'-' . $es_transparencia_doc_date_end_year;
            } else {
              $es_post['post_title'] = $es_transparencia_doc_type_title . ' '. $es_transparencia_doc_date_start_month . ' - ' . $es_transparencia_doc_date_start_year . ' - ' . $es_transparencia_doc_date_end_month . ' ' . $es_transparencia_doc_date_end_year;
              $es_post['post_name'] = $es_transparencia_doc_type_slug . '-' . $es_transparencia_doc_date_start_month . '-' . $es_transparencia_doc_date_start_year . '-' . $es_transparencia_doc_date_end_month .'-' . $es_transparencia_doc_date_end_year;
            }

            // $es_post['post_title'] = $es_transparencia_doc_type_title . ' '. $es_transparencia_doc_date_start_month . ' - ' . $es_transparencia_doc_date_end_month . ' ' . $es_transparencia_doc_date_end_year;
            // $es_post['post_name'] = $es_transparencia_doc_type_slug . '-' . $es_transparencia_doc_date_start_month . '-' . $es_transparencia_doc_date_end_month .'-' . $es_transparencia_doc_date_end_year;

          } elseif ( $es_transparencia_doc_frecuency == 'anual' ) {
            $es_post['post_title'] = $es_transparencia_doc_type_title . ' - ' . $es_transparencia_doc_date_year;
            $es_post['post_name'] = $es_transparencia_doc_type_slug . '-' . $es_transparencia_doc_date_year;
          } elseif ( $es_transparencia_doc_frecuency == 'anual' && $es_transparencia_doc_type_slug == 'plan-operativo-anual' ) {
            $es_post['post_title'] = $es_transparencia_doc_type_title . ' - ' . $es_transparencia_doc_date_year;
            $es_post['post_name'] = $es_transparencia_doc_type_slug . '-' . $es_transparencia_doc_date_year;
          } else {
            if ( $es_transparencia_doc_type_slug == 'activos' || $es_transparencia_doc_type_slug == 'pasivo-capital' ) {
              $es_post['post_title'] = 'Balance General - ' .  $es_transparencia_doc_type_title . ' - ' . $es_transparencia_doc_date_month . ' ' . $es_transparencia_doc_date_year;
            } else {
              $es_post['post_title'] = $es_transparencia_doc_type_title . ' - ' . $es_transparencia_doc_date_month . ' ' . $es_transparencia_doc_date_year;
            }

            $es_post['post_name'] = $es_transparencia_doc_type_slug . '-' . $es_transparencia_doc_date_month . '-' . $es_transparencia_doc_date_year;

          }

          if ( $es_transparencia_doc_frecuency == 'tri-semestral' && $es_transparencia_doc_type_slug == 'plan-estrategico-institucional' ) {
            $es_transparencia_doc_date_meta = get_post_meta( $post_id,'es_transparencia_documento_date_period_start',true );
          } elseif ( $es_transparencia_doc_frecuency == 'tri-semestral' ) {
            $es_transparencia_doc_date_meta = get_post_meta( $post_id,'es_transparencia_documento_date_period_end',true );
          } else {
            $es_transparencia_doc_date_meta = get_post_meta( $post_id,'es_transparencia_documento_date',true );
          }

          $es_transparencia_doc_time_format = current_time( 'H:i:s', $gmt = 0 );
          $es_transparencia_doc_date_format = 'Y-m-d';
          $es_transparencia_doc_datetime_format = $es_transparencia_doc_date_format . ' ' . $es_transparencia_doc_time_format;
          $es_transparencia_doc_date = date( $es_transparencia_doc_datetime_format, strtotime($es_transparencia_doc_date_meta) );

          if ( isset($es_transparencia_doc_frecuency) && !($es_transparencia_doc_frecuency == 'ninguna') ) {
            $es_post['post_date'] = $es_transparencia_doc_date;
          }

         break;

      default:
      # code...
        break;
  }

  wp_update_post( $es_post );

}

add_action('acf/save_post', 'es_set_acf_title', 20);
// Set Titles of custom post with ACF fields

function save_mantenimiento_meta( $post_id, $post, $update ) {

    /*
     * In production code, $slug should be set only once in the plugin,
     * preferably as a class property, rather than in each function that needs it.
     */
    $post_type = get_post_type($post_id);

    // If this isn't a 'mantenimiento' post, don't update it.
    if ( "mantenimiento" != $post_type ) return;


}
// add_action( 'save_post', 'save_mantenimiento_meta', 10, 3 );

// Disable Denuncia de Fraude ID Field
function es_form_denuncia_fruade_disable_id( $field ) {

  if( $field['value'] ) {

    $field['disabled'] = true;

  }

    return $field;

}
// name
add_filter('acf/prepare_field/name=es_form_denuncia_fraude_id', 'es_form_denuncia_fruade_disable_id');
// Disable Denuncia de Fraude ID Field


// Disable Denuncia de Fraude ID Field
function es_form_denuncia_fruade_prepare_id( $field ) {

    global $post;

  $es_post = array();

    $es_fraude_date = get_the_date('dmY');

    $es_fraude_provincia_field = get_field('es_form_denuncia_fraude_address_province');
    $es_fraude_report_type_field = get_field('es_form_denuncia_fraude_report_type');
    $es_fraude_report_type_street_lighting_field = get_field('es_form_denuncia_fraude_type_street_lighting');
    $es_fraude_report_type_meter_field = get_field('es_form_denuncia_fraude_type_meter');
    $es_fraude_report_type_grid_field = get_field('es_form_denuncia_fraude_type_grid');
    $es_fraude_report_type_service_field = get_field('es_form_denuncia_fraude_type_service');

    if (!empty($es_fraude_provincia_field) && is_array($es_fraude_provincia_field)) {
      $es_fraude_provincia = $es_fraude_provincia_field['value'];
    } else {
      $es_fraude_provincia = $es_fraude_provincia_field;
    }

    if (!empty($es_fraude_report_type_field) && is_array($es_fraude_report_type_field)) {
      $es_fraude_report_type = $es_fraude_report_type_field['value'];
    } else {
      $es_fraude_report_type = $es_fraude_report_type_field;
    }

    if (!empty($es_fraude_report_type_street_lighting_field) && is_array($es_fraude_report_type_street_lighting_field)) {
      $es_fraude_report_type_street_lighting = $es_fraude_report_type_street_lighting_field['value'];
    } else {
      $es_fraude_report_type_street_lighting = $es_fraude_report_type_street_lighting_field;
    }

    if (!empty($es_fraude_report_type_meter_field) && is_array($es_fraude_report_type_meter_field)) {
      $es_fraude_report_type_meter = $es_fraude_report_type_meter_field['value'];
    } else {
      $es_fraude_report_type_meter = $es_fraude_report_type_meter_field;
    }

    if (!empty($es_fraude_report_type_grid_field) && is_array($es_fraude_report_type_grid_field)) {
      $es_fraude_report_type_grid = $es_fraude_report_type_grid_field['value'];
    } else {
      $es_fraude_report_type_grid = $es_fraude_report_type_grid_field;
    }

    if (!empty($es_fraude_report_type_service_field) && is_array($es_fraude_report_type_service_field)) {
      $es_fraude_report_type_service = $es_fraude_report_type_service_field['value'];
    } else {
      $es_fraude_report_type_service = $es_fraude_report_type_service_field;
    }


    if ( !empty($es_fraude_report_type_street_lighting) ) {
      $es_fraude_report_type_choice = $es_fraude_report_type_street_lighting;
    } elseif ( !empty($es_fraude_report_type_meter) ) {
      $es_fraude_report_type_choice = $es_fraude_report_type_meter;
    } elseif ( !empty($es_fraude_report_type_grid) ) {
      $es_fraude_report_type_choice = $es_fraude_report_type_grid;
    } elseif( !empty($es_fraude_report_type_service) ) {
      $es_fraude_report_type_choice = $es_fraude_report_type_service;
    }

    if (isset($es_fraude_report_type_choice)) {
      $es_form_denuncia_fraude_id = $es_fraude_date . '-' . $es_fraude_provincia . '-' . $es_fraude_report_type . '-' . $es_fraude_report_type_choice . '-' . $post->ID;
    }

    if (isset($post) && $post->post_type == 'denuncia-fraude') {
       $es_post['ID'] = $post->ID;

      //  if ( empty( get_post_meta($post->ID, 'es_form_denuncia_fraude_id', true) ) ) {

         update_post_meta($post->ID, 'es_form_denuncia_fraude_id', $es_form_denuncia_fraude_id );
      //  }
    }

    return $field;

}

function es_form_denuncia_fruade_set_id($field, $value, $post_id) {
  update_field($field, $value, $post_id);
}
// name
add_filter('acf/load_field/name=es_form_denuncia_fraude_id', 'es_form_denuncia_fruade_prepare_id');
// Disable Denuncia de Fraude ID Field


function my_acf_update_value( $value, $post_id, $field  ) {

  // override value
    $value = get_field('es_form_denuncia_fraude_report_type');;

    // do something else to the $post object via the $post_id

    $es_post = array();

    wp_update_post( $es_post );

  // return
    return $value;

}

/**
 * Example of current_screen usage
 * @param $current_screen
 */
function wporg_current_screen_example( $current_screen ) {
  if ( 'someposttype' == $current_screen->post_type && 'post' == $current_screen->base ) {
    // Do something in the edit screen of this post type
  }
}
// add_action( 'current_screen', 'es_set_es_form_denuncia_fraude_id' );

// function my_the_post_action( $post_object ) {
//   // modify post object here
// }
// add_action( 'the_post', 'es_set_es_form_denuncia_fraude_id' );

// add_action( 'load-post.php', 'es_set_es_form_denuncia_fraude_id' );
function es_set_es_form_denuncia_fraude_id($post_object) {

/*  var_dump($post_object);
*/
  if ($post_object->post_type == 'denuncia-fraude') {

    global $post;

    $es_fraude_date = get_the_date('Ymd');

    $es_fraude_provincia_field = get_field('es_form_denuncia_fraude_address_province');
    $es_fraude_report_type_field = get_field('es_form_denuncia_fraude_report_type');
    $es_fraude_report_type_street_lighting_field = get_field('es_form_denuncia_fraude_type_street_lighting');
    $es_fraude_report_type_meter_field = get_field('es_form_denuncia_fraude_type_meter');
    $es_fraude_report_type_grid_field = get_field('es_form_denuncia_fraude_type_grid');
    $es_fraude_report_type_service_field = get_field('es_form_denuncia_fraude_type_service');

    if (!empty($es_fraude_provincia_field) && is_array($es_fraude_provincia_field)) {
      $es_fraude_provincia = $es_fraude_provincia_field['value'];
    } else {
      $es_fraude_provincia = $es_fraude_provincia_field;
    }

    if (!empty($es_fraude_report_type_field) && is_array($es_fraude_report_type_field)) {
      $es_fraude_report_type = $es_fraude_report_type_field['value'];
    } else {
      $es_fraude_report_type = $es_fraude_report_type_field;
    }

    if (!empty($es_fraude_report_type_street_lighting_field) && is_array($es_fraude_report_type_street_lighting_field)) {
      $es_fraude_report_type_street_lighting = $es_fraude_report_type_street_lighting_field['value'];
    } else {
      $es_fraude_report_type_street_lighting = $es_fraude_report_type_street_lighting_field;
    }

    if (!empty($es_fraude_report_type_meter_field) && is_array($es_fraude_report_type_meter_field)) {
      $es_fraude_report_type_meter = $es_fraude_report_type_meter_field['value'];
    } else {
      $es_fraude_report_type_meter = $es_fraude_report_type_meter_field;
    }

    if (!empty($es_fraude_report_type_grid_field) && is_array($es_fraude_report_type_grid_field)) {
      $es_fraude_report_type_grid = $es_fraude_report_type_grid_field['value'];
    } else {
      $es_fraude_report_type_grid = $es_fraude_report_type_grid_field;
    }

    if (!empty($es_fraude_report_type_service_field) && is_array($es_fraude_report_type_service_field)) {
      $es_fraude_report_type_service = $es_fraude_report_type_service_field['value'];
    } else {
      $es_fraude_report_type_service = $es_fraude_report_type_service_field;
    }


    if ( !empty($es_fraude_report_type_street_lighting) ) {
      $es_fraude_report_type_choice = $es_fraude_report_type_street_lighting;
    } elseif ( !empty($es_fraude_report_type_meter) ) {
      $es_fraude_report_type_choice = $es_fraude_report_type_meter;
    } elseif ( !empty($es_fraude_report_type_grid) ) {
      $es_fraude_report_type_choice = $es_fraude_report_type_grid;
    } elseif( !empty($es_fraude_report_type_service) ) {
      $es_fraude_report_type_choice = $es_fraude_report_type_service;
    }


    echo '<pre>' . $es_fraude_date . '-' . $es_fraude_provincia . '-' . $es_fraude_report_type . '-' . $es_fraude_report_type_choice . '-' . $post->ID . '</pre>';

    echo '<pre>Field: ' . $post->ID . '</pre>';

    echo '<pre>Type: ' . $es_fraude_report_type . '</pre>';

    echo '<pre>Lighting: ' . $es_fraude_report_type_street_lighting . '</pre>';

    echo '<pre>Meter: ' . $es_fraude_report_type_meter . '</pre>';

    echo '<pre>Grid: ' . $es_fraude_report_type_grid . '</pre>';

    echo '<pre>Service: ' . $es_fraude_report_type_service . '</pre>';

    echo '<pre>Service: ' . is_singular() . '</pre>';
  }

}

// acf/update_value/type={$field_type} - filter for a specific field based on it's type
// add_filter('acf/update_value/name=es_form_denuncia_fraude_report_type', 'my_acf_update_value', 10, 3);


// add_filter( 'ninja_forms_submit_data', 'my_ninja_forms_submit_data' );
function my_ninja_forms_submit_data( $form_data ) {

  foreach( $form_data[ 'fields' ] as $field ) { // Field settigns, including the field key and value.

   /*if( 'my_key' != $field[ 'key' ] ) continue;*/ // Check the field key to see if this is the field that I need to update.

    /*$field[ 'value' ] = 'foo';*/ // Update the submitted field value.
  }

  /*$form_settings = $form_data[ 'settings' ];*/ // Form settings.

  /*$extra_data = $form_data[ 'extra' ];*/ // Extra data included with the submission.

   // echo '<pre>' . var_dump($form_data) . '</pre>';
  return $form_data;
}

// add_action( 'ninja_forms_after_submission', 'my_ninja_forms_after_submission' );
function my_ninja_forms_after_submission( $form_data ){
  // echo '<pre>' . var_dump($form_data) . '</pre>';

  $es_form_denuncia_fraude_id = $form_data['fields'];


}

// Breadcrumbs
// =============================================================================



// Entry Meta
// =============================================================================

if ( ! function_exists( 'es_x_integrity_entry_meta' ) ) :
  function es_x_integrity_entry_meta() {

    //
    // Author.
    //

    // $author = sprintf( '<span><i class="x-icon-pencil" data-x-icon="&#xf040;"></i> %s</span>',
    //   get_the_author()
    // );

    $author = NULL;



    //
    // Date.
    //

    $date = sprintf( '<span><time class="entry-date" datetime="%1$s"><i class="x-icon-calendar" data-x-icon="&#xf073;"></i> %2$s</time></span>',
      esc_attr( get_the_date( 'c' ) ),
      esc_html( get_the_date() )
    );


    //
    // Categories.
    //

    if ( get_post_type() == 'x-portfolio' ) {
      if ( has_term( '', 'portfolio-category', NULL ) ) {
        $categories        = get_the_terms( get_the_ID(), 'portfolio-category' );
        $separator         = ', ';
        $categories_output = '';
        foreach ( $categories as $category ) {
          $categories_output .= '<a href="'
                              . get_term_link( $category->slug, 'portfolio-category' )
                              . '" title="'
                              . esc_attr( sprintf( __( "View all posts in: &ldquo;%s&rdquo;", '__x__' ), $category->name ) )
                              . '"><i class="x-icon-bookmark" data-x-icon="&#xf02e;"></i> '
                              . $category->name
                              . '</a>'
                              . $separator;
        }

        $categories_list = sprintf( '<span>%s</span>',
          trim( $categories_output, $separator )
        );
      } else {
        $categories_list = '';
      }
    } elseif ( get_post_type() == 'gestion-social' ) {
      if ( has_term( '', 'comunitaria', NULL ) ) {
        $categories        = get_the_terms( get_the_ID(), 'comunitaria' );
        $separator         = ', ';
        $categories_output = '';
        foreach ( $categories as $category ) {
          $categories_output .= '<a href="'
                              . get_term_link( $category->slug, 'comunitaria' )
                              . '" title="'
                              . esc_attr( sprintf( __( "Ver todas las publicaciones en: &ldquo;%s&rdquo;", '__x__' ), $category->name ) )
                              . '"><i class="x-icon-bookmark" data-x-icon="&#xf02e;"></i> '
                              . $category->name
                              . '</a>'
                              . $separator;
        }

        $categories_list = sprintf( '<span>%s</span>',
          trim( $categories_output, $separator )
        );
      } else {
        $categories_list = '';
      }
    } else {
      $categories        = get_the_category();
      $separator         = ', ';
      $categories_output = '';
      foreach ( $categories as $category ) {
        $categories_output .= '<a href="'
                            . get_category_link( $category->term_id )
                            . '" title="'
                            . esc_attr( sprintf( __( "View all posts in: &ldquo;%s&rdquo;", '__x__' ), $category->name ) )
                            . '"><i class="x-icon-bookmark" data-x-icon="&#xf02e;"></i> '
                            . $category->name
                            . '</a>'
                            . $separator;
      }

      $categories_list = sprintf( '<span>%s</span>',
        trim( $categories_output, $separator )
      );
    }


    //
    // Comments link.
    //

    if ( comments_open() ) {

      $title  = apply_filters( 'x_entry_meta_comments_title', get_the_title() );
      $link   = apply_filters( 'x_entry_meta_comments_link', get_comments_link() );
      $number = apply_filters( 'x_entry_meta_comments_number', get_comments_number() );

    $text = ( 0 === $number ) ? 'Leave a Comment' : sprintf( _n( '%s Comment', '%s Comments', $number, '__x__' ), $number );

$comments = sprintf( '<span><a href="%1$s" title="%2$s" class="meta-comments"><i class="x-icon-comments" data-x-icon="&#xf086;"></i> %3$s</a></span>',
        esc_url( $link ),
        esc_attr( sprintf( __( 'Leave a comment on: &ldquo;%s&rdquo;', '__x__' ), $title ) ),
        $text
      );

    } else {

      $comments = '';

    }


    //
    // Output.
    //

    if ( x_does_not_need_entry_meta() ) {
      return;
    } else {
      printf( '<p class="p-meta">%1$s%2$s%3$s%4$s</p>',
        $author,
        $date,
        $categories_list,
        $comments
      );
    }

  }
endif;

function save_gestion_social_meta( $post_id, $post, $update ) {

    $slug = 'gestion-social'; //Slug of CPT

    // If this isn't a 'gestion-social' post, don't update it.
    if ( $slug != $post->post_type ) {
        return;
    }

    wp_set_object_terms( get_the_ID(), $term_id, $taxonomy );
}

// add_action( 'save_post', 'save_gestion_social_meta', 10, 3 );

/**
* Add an automatic default custom taxonomy for custom post type.
* If no story (taxonomy) is set, the comic post will be sorted as “draft” and won’t return an offset error.
*
*/
    function set_default_object_terms( $post_id, $post ) {


        if ( 'publish' === $post->post_status &&
            ( $post->post_type === 'gestion-social' || $post->post_type === 'instructivo' ) ) {

            $post_term = ($post->post_type === 'gestion-social') ? 'gestion-social' : 'comunicacion-digital' ;
            $taxonomies = get_object_taxonomies( $post->post_type );
            foreach ( (array) $taxonomies as $taxonomy ) {
                $terms = wp_get_post_terms( $post_id, $taxonomy );
                if ( empty( $terms ) ) {
                    wp_set_object_terms( $post_id, $post_term, $taxonomy );
                }
            }
        }

        // switch ($post->post_type) {
        //   case 'gestion-social':
        //     $query_posts_type =
        //     break;
        //   case 'instructivo':
        //     # code...
        //     break;
        //   default:
        //     # code...
        //     break;
        // }
    }
    add_action( 'save_post', 'set_default_object_terms', 0, 2 );

    add_action('admin_head','es_admin_notice_hide'); //using action hook to modify css
    function es_admin_notice_hide()
    {
    if(!current_user_can('administrator')) //check the current user role which is not admin
    {
        echo '<style>';
            echo '.notice{display:none}';
            echo '</style>';
        }
    }

    // Set uploaded documents MIME type

    function es_get_file_attrs( $es_doc_file_mime_type) {

      switch ($es_doc_file_mime_type) {
        case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
          $es_doc_file_class = 'es-transparencia-doc-file';
          $es_doc_file_alt = 'MicroSoft Word';
          $es_doc_file_icon = 'far fa-file-word';
          break;
        case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
          $es_doc_file_class = 'es-transparencia-doc-file';
          $es_doc_file_alt = 'MicroSoft Excel';
          $es_doc_file_icon = 'far fa-file-excel';
          break;

        case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
          $es_doc_file_class = 'es-transparencia-doc-file';
          $es_doc_file_alt = 'Office Open XML';
          $es_doc_file_icon = 'fas fa-file-excel';
          $es_doc_file_desc = 'Hoja de cálculo de Office Open XML';
          break;

        case 'application/vnd.ms-excel':
          $es_doc_file_class = 'es-transparencia-doc-file';
          $es_doc_file_alt = 'MicroSoft Excel';
          $es_doc_file_icon = 'far fa-file-excel';
          break;
        case 'application/vnd.ms-excel.sheet.macroEnabled.12':
          $es_doc_file_class = 'es-transparencia-doc-file';
          $es_doc_file_alt = 'MicroSoft Excel';
          $es_doc_file_icon = 'far fa-file-excel';
          break;

        case 'text/csv':
          $es_doc_file_class = 'es-transparencia-doc-file';
          $es_doc_file_alt = 'Comma-separated values';
          $es_doc_file_icon = 'fas fa-file-csv';
          $es_doc_file_desc = 'Valores separados por comas';
          break;

        case 'application/vnd.oasis.opendocument.spreadsheet':
          $es_doc_file_class = 'es-transparencia-doc-file';
          $es_doc_file_alt = 'OpenDocument Spreadsheet';
          $es_doc_file_icon = 'fas fa-file-alt';
          $es_doc_file_desc = 'Hoja de cálculo de documento abierto';
          break;

        case 'application/pdf':
          $es_doc_file_class = 'es-transparencia-doc-file';
          $es_doc_file_alt = 'Adobe PDF';
          $es_doc_file_icon = 'far fa-file-pdf';
          break;

        default:
          $es_doc_file_alt = 'Archivo desconocido';
          $es_doc_file_class = 'es-transparencia-doc-file-none';
          $es_doc_file_icon = 'far fa-file';
          break;
      }

      $es_doc_file_attrs = array(
        'class' => $es_doc_file_class,
        'alt' => $es_doc_file_alt,
        'icon' => $es_doc_file_icon
      );

      return $es_doc_file_attrs;
    }

    // Allow YouTube embeds in WPForo
    // add_filter('wpforo_content_after', 'wpforo_custom_video_embed', 10);
    function wpforo_custom_video_embed( $content ){
        $paterns = array();
        $paterns[] = "/<a[^><]+>\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i";
        $paterns[] = "/<a[^><]+>\s*[a-zA-Z\/\/:\.]*youtu.be\/([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i";
        $content = preg_replace($paterns, "<iframe width=\"420\" height=\"280\" src=\"//www.youtube.com/embed/$1\" frameborder=\"0\" allowfullscreen></iframe>", $content);
        return $content;
    }
 ?>