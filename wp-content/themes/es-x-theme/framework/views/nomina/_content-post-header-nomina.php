<?php

// =============================================================================
// VIEWS/INTEGRITY/_CONTENT-POST-HEADER.PHP
// -----------------------------------------------------------------------------
// Standard <header> output for various posts.
// =============================================================================

?>

<header class="entry-header">

  <h2 class="entry-title">Nómina de Transparencia</h2>

  <?php /*es_x_integrity_entry_meta();*/ ?>
</header>