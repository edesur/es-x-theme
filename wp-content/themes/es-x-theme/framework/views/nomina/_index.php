<?php

// =============================================================================
// VIEWS/NOMINA/_INDEX.PHP
// -----------------------------------------------------------------------------
// Includes the index output.
// =============================================================================
$stack = x_get_stack();

if ( is_home() ) :
  $style     = x_get_option( 'x_blog_style' );
  $cols      = x_get_option( 'x_blog_masonry_columns' );
  $condition = is_home() && $style == 'masonry';
elseif ( is_archive() ) :
  $style     = x_get_option( 'x_archive_style' );
  $cols      = x_get_option( 'x_archive_masonry_columns' );
  $condition = is_archive() && $style == 'masonry';
elseif ( is_search() ) :
  $condition = false;
endif;
?>



<?php if ( $condition) : ?>

  <?php /*x_get_view( 'global', '_script', 'isotope-index' );*/ ?>

  <!-- <div id="x-iso-container" class="x-iso-container x-iso-container-posts"> -->

    <div class="es-site-transparencia-nomina entry-wrap">

      <?php x_get_view( 'nomina', '_content', 'post-header-nomina' ); ?>

      <div class="grid-x grid-margin-x" style="padding: 45px 0px;">
        <div class="cell small-12 large-3">
          <?php dynamic_sidebar('ups-sidebar-edesur-sidebar-transparencia'); ?>
        </div>
        <div class="cell small-12 large-9">

          <?php if ( have_posts() ) : ?>
            <ul class="x-feature-list">
              <?php while ( have_posts() ) : the_post(); ?>

                <?php
                  $es_gestion_humana_nomina_file = get_field('es_gestion_humana_nomina_file');
                  $es_gestion_humana_nomina_file_url = $es_gestion_humana_nomina_file['url'];
                  $es_gestion_humana_nomina_file_dir = get_attached_file($es_gestion_humana_nomina_file['id']);
                  $es_gestion_humana_nomina_file_size = filesize($es_gestion_humana_nomina_file_dir);
                  $es_gestion_humana_nomina_file_size_format = size_format($es_gestion_humana_nomina_file_size, 2);
                  $es_gestion_humana_nomina_file_mime_type = $es_gestion_humana_nomina_file['mime_type'];
                  $es_gestion_humana_nomina_date_field = get_field('es_gestion_humana_nomina_date');
                  $es_gestion_humana_nomina_date = strtotime($es_gestion_humana_nomina_date_field);
                  $es_gestion_humana_nomina_date_month = date_i18n('F', $es_gestion_humana_nomina_date);
                  $es_gestion_humana_nomina_date_year = date_i18n('Y', $es_gestion_humana_nomina_date);

                  switch ($es_gestion_humana_nomina_file_mime_type) {
                    case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
                      $es_gestion_humana_nomina_file_icon = 'x-icon-file-excel-o';
                      $es_gestion_humana_nomina_file_icon_data = '';
                      break;
                    case 'application/vnd.ms-excel':
                      $es_transparencia_documento_file_icon = 'x-icon-file-excel-o';
                      $es_transparencia_documento_file_icon_data = '';
                      break;
                    case 'application/pdf':
                      $es_gestion_humana_nomina_file_icon = 'x-icon-file-pdf-o';
                      $es_gestion_humana_nomina_file_icon_data = '';
                      break;

                    default:
                      $es_gestion_humana_nomina_file_icon = 'x-icon-file-o';
                      $es_gestion_humana_nomina_file_icon_data = '';
                      break;
                  }
                ?>

                <li class="x-feature-box left-text middle-text cf" style="max-width: none;">

                  <div class="x-feature-box-graphic circle x-feature-box-align-v-middle">
                    <div class="x-feature-box-graphic-outer circle" style=" margin-right: 16px;">
                      <div class="x-feature-box-graphic-inner circle" style="font-size: 48px;">
                        <i class="<?php echo $es_gestion_humana_nomina_file_icon; ?> circle" data-x-icon="<?php echo $es_gestion_humana_nomina_file_icon_data; ?>" style="margin: 0 auto; color: #ffffff; background-color: hsl(24, 100%, 52%);"></i>
                      </div>
                    </div>
                  </div>

                  <div class="x-feature-box-content x-feature-box-align-v-middle">
                    <h4 class="x-feature-box-title"><?php the_title(); ?></h4>
                      <p class="x-feature-box-text">Nómina de empleados del mes de <?php echo $es_gestion_humana_nomina_date_month . ' ' . $es_gestion_humana_nomina_date_year; ?>  <a href="<?php echo $es_gestion_humana_nomina_file_url; ?>" title="Descargar: <?php the_title(); ?>">Descargar</a> (<?php echo  $es_gestion_humana_nomina_file_size_format; ?>)</p>
                  </div>

              </li>

              <?php endwhile; ?>
            </ul>
          <?php else : ?>
            <?php x_get_view( 'global', '_content-none' ); ?>
          <?php endif; ?>

        </div>
      </div>
    </div>

  <!-- </div> -->

<?php endif; ?>

<?php pagenavi(); ?>