<?php

// =============================================================================
// VIEWS/INTEGRITY/CONTENT.PHP
// -----------------------------------------------------------------------------
// Standard post output for Integrity.
// =============================================================================

?>

<?php
  $es_class_accordion = ($wp_query->current_post === 0) ? 'accordion-item is-active' : 'accordion-item';

  switch ( $wp_query->post->post_name ) {
    case 'preguntas-generales':
      $es_class_fa_icon = 'fa fa-question-circle';
      break;
    case 'telemedida':
      $es_class_fa_icon = 'fa fa-tachometer';
      break;
    case 'glosario-telemedida':
      $es_class_fa_icon = 'fa fa-book';
      break;
    case 'servicio-prepago':
      $es_class_fa_icon = 'fa fa-money';
      break;
    case 'medicion-neta':
      $es_class_fa_icon = 'fa fa-exchange';
      break;
    case 'pagos-recurrentes':
      $es_class_fa_icon = 'fa fa-credit-card-alt';
      break;

    default:
      $es_class_fa_icon = 'fa fa-question-circle-o';
      break;
  }
?>

<li class="<?php echo $es_class_accordion; ?>" data-accordion-item>

  <a class="accordion-title" href="#">
    <i class="<?php echo $es_class_fa_icon; ?>" style="font-size: 1rem;"> </i>
    <?php the_title() ?>
  </a>

  <div class="accordion-content" data-tab-content>

    <?php if (get_field('es_preguntas_frecuentes_description')): ?>
      <div class="callout">
        <?php echo the_field('es_preguntas_frecuentes_description'); ?>
      </div>
    <?php endif ?>

    <?php if (have_rows('es_preguntas_frecuentes_questions')): ?>
      <?php $es_preguntas_frecuentes = get_field('es_preguntas_frecuentes_questions'); ?>
      <ul class="accordion" data-accordion data-allow-all-closed="true">

        <?php foreach ( $es_preguntas_frecuentes as $question => $es_pregunta_frecuente ): ?>

          <li class="accordion-item" data-accordion-item>
            <a href="#" class="accordion-title">
             <?php echo $es_pregunta_frecuente['es_preguntas_frecuentes_question'] ?>
            </a>

            <div class="accordion-content" data-tab-content>
              <?php echo $es_pregunta_frecuente['es_preguntas_frecuentes_answer'] ?>
            </div>
          </li>

        <?php endforeach ?>

      </ul>
    <?php endif ?>
  </div>
</li>