<?php

// =============================================================================
// VIEWS/GLOBAL/_INDEX.PHP
// -----------------------------------------------------------------------------
// Includes the index output.
// =============================================================================
$stack = x_get_stack();

if ( is_home() ) :
  $style     = x_get_option( 'x_blog_style' );
  $cols      = x_get_option( 'x_blog_masonry_columns' );
  $condition = is_home() && $style == 'masonry';
elseif ( is_archive() ) :
  $style     = x_get_option( 'x_archive_style' );
  $cols      = x_get_option( 'x_archive_masonry_columns' );
  $condition = is_archive() && $style == 'masonry';
elseif ( is_search() ) :
  $condition = false;
endif;

?>

    <?php if ( have_posts() ) : ?>

      <div class="entry-wrap">

        <?php x_get_view( 'pregunta-frecuente', '_content', 'post-header-pregunta-frecuente' ); ?>

        <ul class="es-site-preguntas-frecuentes accordion" id="es_site_preguntas_frecuentes" data-responsive-accordion-tabs="accordion large-tabs" data-allow-all-closed="true">
            <?php while ( have_posts() ) : the_post(); ?>

                <?php x_get_view( 'pregunta-frecuente', 'content', get_post_format() ); ?>

            <?php endwhile; ?>
        </ul>

      </div>

    <?php endif; ?>
