<?php

// =============================================================================
// VIEWS/INTEGRITY/_CONTENT-POST-HEADER.PHP
// -----------------------------------------------------------------------------
// Standard <header> output for various posts.
// =============================================================================

?>

<header class="entry-header">

  <h1 class="h2"><?php  post_type_archive_title(''); ?></h1>

  <?php /*es_x_integrity_entry_meta();*/ ?>
</header>