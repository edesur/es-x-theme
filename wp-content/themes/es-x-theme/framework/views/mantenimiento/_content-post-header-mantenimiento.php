<?php

// =============================================================================
// VIEWS/INTEGRITY/_CONTENT-POST-HEADER.PHP
// -----------------------------------------------------------------------------
// Standard <header> output for various posts.
// =============================================================================

?>

<header class="entry-header">

  <h3 class=""><?php the_title(); ?></h3>

  <?php /*es_x_integrity_entry_meta();*/ ?>
</header>