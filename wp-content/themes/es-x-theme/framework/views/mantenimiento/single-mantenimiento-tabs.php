<?php if ( have_rows('es_mantenimiento_week') ): ?>

  <?php $es_mantenimiento_week = get_field('es_mantenimiento_week'); ?>

  <ul class="expanded tabs" data-responsive-accordion-tabs="tabs medium-accordion large-tabs" id="es_site_mantenimientos" data-multi-expand="true">

    <?php $date_today = date('m/d/Y'); ?>

    <?php foreach ($es_mantenimiento_week as $es_mantenimiento => $es_mantenimiento_day): ?>

      <?php
        $es_mantenimiento_date =  $es_mantenimiento_day['es_mantenimiento_date'];
        $es_mantenimiento_tab_date =  strtotime($es_mantenimiento_day['es_mantenimiento_date']);
        $es_mantenimiento_tab_date_day =  $es_mantenimiento_tab_date;
        $es_mantenimiento_tab_date_format_day = 'l';
        $es_mantenimiento_tab_date_format = 'j \d\e F, Y';

        $es_tab_class = ($date_today == $es_mantenimiento_date) ? 'tabs-title is-active' : 'tabs-title' ;
      ?>

      <?php if ( $date_today == $es_mantenimiento_date ): ?>

      <?php endif ?>

        <li class="<?php echo $es_tab_class; ?> text-center" ><a href="#panel_<?php echo $es_mantenimiento + 1; ?>" style="padding: 1.25rem;"><?php /*echo date_i18n( $es_mantenimiento_tab_date_format_day, $es_mantenimiento_tab_date_day);*/ ?>
          <?php echo date_i18n( $es_mantenimiento_tab_date_format, $es_mantenimiento_tab_date); ?></a></li>

    <?php endforeach ?>

  </ul>

  <div class="tabs-content" data-tabs-content="es_site_mantenimientos">

    <?php foreach ($es_mantenimiento_week as $es_mantenimiento => $es_mantenimiento_data): ?>

      <?php
        $es_mantenimiento_data_date  = $es_mantenimiento_data['es_mantenimiento_date'];
        $es_tab_panel_class = ($date_today == $es_mantenimiento_data_date) ? 'tabs-panel is-active' : 'tabs-panel' ;
      ?>

        <div class="<?php echo $es_tab_panel_class; ?>" id="panel_<?php echo $es_mantenimiento + 1; ?>">

          <?php $es_mantenimiento_provinces = $es_mantenimiento_data['es_mantenimiento_provinces_affected']; ?>

          <ul class="accordion" data-accordion data-allow-all-closed="true" data-multi-expand="true">


            <?php foreach ($es_mantenimiento_provinces as $es_mantenimiento_province): ?>

              <?php if ($es_mantenimiento_province['es_mantenimiento_province']['value'] == 'no-provincias-afectadas'): ?>

                <p>No trabajos de mantenimiento programados este <?php echo date_i18n( 'l j \d\e F, Y', strtotime($es_mantenimiento_data_date)); ?>:</p>

              <?php else: ?>

                <li class="accordion-item" data-accordion-item>
                  <!-- Accordion tab title -->

                  <?php
                    $es_mantenimiento_count =  count($es_mantenimiento_province['es_mantenimiento_schedule_sector']);
                    $es_mantenimiento_label = ($es_mantenimiento_count > 1) ? 'Mantenimientos Programandos' : 'Mantenimiento Programando' ;
                  ?>


                  <a href="#" class="accordion-title">
                    <span class="es-mantenimiento-province-label h4">
                      <?php echo $es_mantenimiento_province['es_mantenimiento_province']['label'] .' '; ?>
                    </span><br class="show-for-small-only">
                    <span class="label secondary" style="border-radius: 50%;">
                      <?php echo $es_mantenimiento_count; ?>
                    </span>
                    <span class="h5" style="font-size: 0.75rem;">
                      <?php echo ' ' . $es_mantenimiento_label; ?>
                    </span>

                  </a> <!-- <span class="badge warning"></span> -->

                  <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
                  <div class="accordion-content" data-tab-content>
                    <?php $es_mantenimiento_sectors = $es_mantenimiento_province['es_mantenimiento_schedule_sector']; ?>

                    <?php foreach ($es_mantenimiento_sectors as $es_mantenimiento_sector): ?>

                    <h5>Sectores afectados entre <?php echo $es_mantenimiento_sector['es_mantenimiento_time_start'] . ' a ' . $es_mantenimiento_sector['es_mantenimiento_time_end']?>:</h5>

                    <p><?php echo $es_mantenimiento_sector['es_mantenimiento_affected_area'] ?></p>

                <?php endforeach ?>
                  </div>
                </li>

              <?php endif ?>

            <?php endforeach ?>
          </ul>

        </div>

    <?php endforeach ?>

  </div>
<?php endif ?>