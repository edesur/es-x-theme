<?php

// =============================================================================
// VIEWS/GLOBAL/_CONTENT-NONE.PHP
// -----------------------------------------------------------------------------
// Output when no posts or pages are available.
// =============================================================================

   $current_month = date('F');
   $current_month_digit = date('m');
   $current_year = date('Y');

   $display_month = Date('F', strtotime($current_month . " last month"));
   $display_year = ($current_month_digit == 01) ? Date('Y', strtotime($current_year . " last year")) : strftime('%Y');
?>

<div class="x-container">
  <div class="x-column x-sm cs-ta-center x-1-1">
      <h3 class="h-custom-headline mtn h4">Durante el mes de <?php echo __($display_month) . ' del ' . $display_year; ?> no se realizó ningún Proceso de Compras bajo esta modalidad.</h3>
      <a class="x-btn x-btn-global" href="http://edesur2.edesur.com.do/procesos-de-compras/" title="Procesos de Compras" target="_blank" >Procesos de Compras</a>
      <p><a href="http://edesur2.edesur.com.do/procesos-de-compras/" title="Todos los Procesos de Compras" target="blank">Todos los Procesos de Compras</a></p>
  </div>
</div>