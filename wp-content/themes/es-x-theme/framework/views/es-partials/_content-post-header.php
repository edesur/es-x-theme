<?php

// =============================================================================
// VIEWS/INTEGRITY/_CONTENT-POST-HEADER.PHP
// -----------------------------------------------------------------------------
// Standard <header> output for various posts.
// =============================================================================

?>

<header class="entry-header">

  <?php if ( get_post_type() == 'mantenimiento' ): ?>

    <h3 class=""><?php the_title(); ?></h3>

  <?php else: ?>

    <?php if (get_post_type() == 'servicio'): ?>

      <?php
        $es_service_type = get_field('es_servicio_informacion_chioce');
        $es_service_value = $es_service_type['value'];
        $es_service_label = $es_service_type['label'];

        switch ($es_service_value) {
          case 'herramienta':
            $es_label_class = 'secondary';
            break;
          case 'informacion':
            $es_label_class = 'success';
            break;

          default:
            $es_label_class = 'primary';
            break;
        }
      ?>

      <div class="es-cell-item text-right">
        <span class="label <?php echo $es_label_class ?>" style="color: #fff;"><?php echo $es_service_label; ?></span>
      </div>
    <?php endif ?>

    <?php if ( is_single() ) : ?>
    <h1 class="entry-title"><?php the_title(); ?></h1>
    <?php else : ?>
    <h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to: "%s"', '__x__' ), the_title_attribute( 'echo=0' ) ) ); ?>"><?php x_the_alternate_title(); ?></a>
    </h2>

    <?php endif; ?>

  <?php endif ?>

  <?php /*es_x_integrity_entry_meta();*/ ?>
</header>