<?php

// =============================================================================
// VIEWS/INTEGRITY/_CONTENT-KK-STAR-RATINGS.PHP
// -----------------------------------------------------------------------------
// =============================================================================

?>

<?php if (function_exists("kk_star_ratings")): ?>

  <div class="es-cell-item-mantenimientos-ratings">
    <?php echo do_shortcode( '[ratemypost]' ); ?>
  </div>

<?php endif ?>