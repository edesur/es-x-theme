<?php

// =============================================================================
// VIEWS/GLOBAL/_CONTENT-404.PHP
// -----------------------------------------------------------------------------
// Output for the 404 page.
// =============================================================================

?>

<div class="es-site-page-404 es-cell-item text-center">

  <h1>Mantenimiento</h1>

  <p>
    <?php _e( 'Esta página está en mantenimiento. Disculpe el inconveniente', '__x__' ); ?><br>
    <?php _e( 'Puede intentar buscar en el siguiente formulario.', '__x__' ); ?>
  </p>

  <?php
   $es_site_page_search_404 = get_search_form(false);
   ?>

   <p><?php echo $es_site_page_search_404; ?></p>

   <p>Regresar a la <a href="<?php echo home_url(); ?>" title="Inicio | <?php echo get_bloginfo( 'name' ); ?>">página de inicio</a></p>

</div>