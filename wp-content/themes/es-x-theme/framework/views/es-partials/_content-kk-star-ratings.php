<?php

// =============================================================================
// VIEWS/INTEGRITY/_CONTENT-KK-STAR-RATINGS.PHP
// -----------------------------------------------------------------------------
// =============================================================================

?>

<?php if (function_exists("kk_star_ratings")): ?>

  <div class="es-cell-item-mantenimientos-ratings">
    <h5>¿Cómo calificaría este servicio?</h5>
    <?php /*echo gdrts_posts_render_rating( );*/ ?>
    <?php echo kk_star_ratings(); ?>
  </div>

<?php endif ?>