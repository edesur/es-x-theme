<?php

// =============================================================================
// VIEWS/GLOBAL/_CONTENT-NONE.PHP
// -----------------------------------------------------------------------------
// Output when no posts or pages are available.
// =============================================================================

?>

<?php
   require_once(locate_template( '/framework/views/es-transparencia/_es-content-variables.php'));
  ?>

    <div class="cell small-12">

      <?php
      $es_img_meta_feat =  es_get_file_meta('/2018/07/es-transparencia-comision-etica-juramentacion-colectiva.jpg');
      $es_img_url_feat = $es_img_meta_feat['url'];
      ?>

    <img src="<?php echo $es_img_url_feat; ?>" alt="Comisión de Ética Pública Juramentación Colectiva">

    <h1 class="text-center mts">Comisión de Ética Pública</h1>

    <p>Las Comisiones de Ética Pública (CEP) son organismos de carácter cívico, integrados por servidores públicos que promueven valores, principios y normas de conducta a lo interno de las instituciones públicas donde operan. Las CEP se constituyen en medios de fortalecimiento institucional, en tanto que desarrollan acciones que procuran la correcta y honesta prestación de servicios públicos.</p>
    <p>Las CEP reciben el acompañamiento, monitoreo y asesoría de la DIGEIG para el diseño de sus Planes Operativos y para garantizar el alcance de sus objetivos. De igual manera, los integrantes de las CEP son capacitados y entrenados mediante Diplomados y otros cursos, para un mejor desempeño de sus funciones.</p>

    <h3>Objetivo General</h3>
    <p>Coordinar el desarrollo de acciones de fortalecimiento de la ética y la integridad en la administración pública a través de las comisiones de Ética Pública.</p>

    <?php
    $es_img_meta_content =  es_get_file_meta('/2018/07/es-transparencia-comision-etica-firma-promocion-codigo.jpg');
    $es_img_url_content = $es_img_meta_content['url'];
    ?>

    <img src="<?php echo $es_img_url_content; ?>" alt="Comisión de Ética Pública | Gestión de firma y promoción del Código de Pautas Éticas">

    <h3 class="mts">Funciones Principales</h3>

    <ol>
      <li>Coordinar y fomentar la conformación de Comisiones de Ética Pública (CEP) en las instituciones públicas del Estado dominicano, así como monitorear, acompañar y dar seguimiento a su funcionamiento.</li>
      <li>Diseñar y establecer herramientas y/o mecanismos para el seguimiento y funcionamiento de las Comisiones de Ética Pública (CEP).</li>
      <li>Acompañar a las Comisiones de Ética Pública (CEP) en la elaboración de planes anuales de ética e integridad, así como dar seguimiento y evaluar la ejecución de los mismos.</li>
      <li>Mantener coordinación con las CEP, para orientarles sobre el proceso de reporte a la DIGEIG de informaciones relativas a denuncias sobre actos de corrupción administrativa y presuntas violaciones al Código de Pautas Ética.</li>
      <li>Coordinar adiestramiento sobre el funcionamiento de las Comisiones de Ética Pública dirigidas a sus miembros.</li>
      <li>Realizar cualquier función afín y complementaria que le sea asignada por su superior inmediato.</li>
    </ol>

  </div>

  <?php if (is_archive()): ?>
    <h3>Documentos</h3>
  <?php else: ?>

    <div class="es-site-transparencia-docs cell small-12">
      <h3>Documentos</h3>
      <ul class="accordion" data-accordion>

        <li class="es-site-transparencia-docs-date accordion-item is-active" data-accordion-item>

          <!-- Accordion tab title -->
          <a href="#" class="accordion-title">
            <h5 class="es-transparencia-docs-date-title"><i class="fas fa-folder"></i>Miembros de la Comisión de Ética Pública</h5>
          </a>

          <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
          <div id="es_cep_miembros_contacto" class="accordion-content" data-tab-content>

            <?php
              $es_doc_file_meta_miembros =  es_get_file_meta('/2018/11/es-transparencia-comision-etica-listado-miembros.xlsx');
               $es_transparencia_doc_file_id = $es_doc_file_meta_miembros['type'] . '-' . $es_doc_file_meta_miembros['id'];
               $es_transparencia_doc_file_class = $es_doc_file_meta_miembros['class'];
               $es_transparencia_doc_file_alt = $es_doc_file_meta_miembros['alt'];
               $es_transparencia_doc_file_mime_type = $es_doc_file_meta_miembros['mime'];
               $es_transparencia_doc_file_url = $es_doc_file_meta_miembros['url'];
               $es_transparencia_doc_file_title = $es_doc_file_meta_miembros['title'];
               $es_transparencia_doc_file_dir = $es_doc_file_meta_miembros['dir'];
               $es_transparencia_doc_file_icon = $es_doc_file_meta_miembros['icon'];
               $es_transparencia_doc_file_size = filesize($es_doc_file_meta_miembros['dir']);
               $es_transparencia_doc_file_size_format = $es_doc_file_meta_miembros['format'];
                $es_transparencia_updated_date = $es_doc_file_meta_miembros['date'];
            ?>

            <div class="es-transparencia-doc-content grid-container">

            <div class="es-transparencia-doc-single grid-x align-middle">

                <div class="es-transparencia-doc-copy">
                  <h4 class="es-transparencia-doc-copy-title h5">Listado de miembros de la Comisión de Ética Pública</h4>
                  <p class="es-transparencia-doc-copy-desc">Listado de miembros  y medios de contactos (teléfonos y correos)</p>
                </div>

                <div class="es-transparencia-doc-download <?php echo $es_transparencia_doc_file_class; ?>">

                    <div class="es-transparencia-file-tools es-cell-item">
                      <div class="button-group expanded small">
                        <a href="<?php echo urlencode($es_transparencia_doc_file_url); ?>" class="es-transparencia-doc-tools-btn button" data-toggle="<?php echo $es_transparencia_doc_file_id ?>" type="<?php echo $es_transparencia_doc_file_mime_type; ?>"><i class="far fa-eye fa-lg"></i> <span>Vista previa</span></a>

                        <a class="es-doc-tools-btn-down es-transparencia-doc-tools-btn button" href="<?php echo $es_transparencia_doc_file_url; ?>" title="Descargar: <?php if (empty($es_transparencia_doc_file_title)) {
                          the_title();
                        } else {
                          echo $es_transparencia_doc_file_title;
                        } ?>" target="_blank" download><i class="fas fa-file-download fa-lg"></i> <span>Descargar</span></a>
                      </div>
                    </div>

                    <div class="es-transparencia-doc-file-info es-cell-item">

                      <div class="es-transparencia-doc-file-meta es-cell-item">

                        <div class="es-transparencia-doc-file-icon es-cell-item">
                          <i class="<?php echo $es_transparencia_doc_file_icon; ?>"  title="<?php echo $es_transparencia_doc_file_alt; ?>"></i>
                        </div>

                        <div class="es-transparencia-doc-file-meta-copy es-cell-item">
                          <small class="es-transparencia-doc-file-meta-type"><?php echo $es_transparencia_doc_file_alt; ?></small>
                          <small class="es-transparencia-doc-file-meta-size">(<?php echo  $es_transparencia_doc_file_size_format; ?>)</small><br>
                        </div>

                      </div>

                      <div class="es-transparencia-doc-file-date-modified es-cell-item">
                        <small ">Publicado: <?php echo $es_transparencia_updated_date; ?></small>
                      </div>

                    </div>

                    </div>

            </div>

            </div>

          </div>
        </li>

        <li class="es-site-transparencia-docs-date accordion-item" data-accordion-item>
          <!-- Accordion tab title -->
          <a href="#" class="accordion-title">
            <h5 class="es-transparencia-docs-date-title"><i class="fas fa-folder"></i>Pautas Éticas</h5>
          </a>

          <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
          <div class="accordion-content" data-tab-content>

            <div class="es-transparencia-doc-content grid-container">

              <?php
                $es_doc_file_meta_pautas =  es_get_file_meta('/2018/07/es-transparencia-comision-etica-codigo-pautas-eticas.pdf');
                 $es_transparencia_doc_file_id = $es_doc_file_meta_pautas['type'] . '-' . $es_doc_file_meta_pautas['id'];
                 $es_transparencia_doc_file_class = $es_doc_file_meta_pautas['class'];
                 $es_transparencia_doc_file_alt = $es_doc_file_meta_pautas['alt'];
                 $es_transparencia_doc_file_mime_type = $es_doc_file_meta_pautas['mime'];
                 $es_transparencia_doc_file_url = $es_doc_file_meta_pautas['url'];
                 $es_transparencia_doc_file_title = $es_doc_file_meta_pautas['title'];
                 $es_transparencia_doc_file_dir = $es_doc_file_meta_pautas['dir'];
                 $es_transparencia_doc_file_icon = $es_doc_file_meta_pautas['icon'];
                 $es_transparencia_doc_file_size = filesize($es_doc_file_meta_pautas['dir']);
                 $es_transparencia_doc_file_size_format = $es_doc_file_meta_pautas['format'];
                  $es_transparencia_updated_date = $es_doc_file_meta_pautas['date'];
              ?>

              <div class="es-transparencia-doc-single grid-x align-middle">

                <div class="es-transparencia-doc-copy">
                  <h4 class="es-transparencia-doc-copy-title h5">Código de Pautas Éticas</h4>
                  <p class="es-transparencia-doc-copy-desc">Código de Pautas Éticas</p>
                </div>

                <div class="es-transparencia-doc-download <?php echo $es_transparencia_doc_file_class; ?>">

                    <div class="es-transparencia-file-tools es-cell-item">
                      <div class="button-group expanded small">
                        <a href="<?php echo urlencode($es_transparencia_doc_file_url); ?>" class="es-transparencia-doc-tools-btn button" data-toggle="<?php echo $es_transparencia_doc_file_id ?>" type="<?php echo $es_transparencia_doc_file_mime_type; ?>"><i class="far fa-eye fa-lg"></i> <span>Vista previa</span></a>

                        <a class="es-doc-tools-btn-down es-transparencia-doc-tools-btn button" href="<?php echo $es_transparencia_doc_file_url; ?>" title="Descargar: <?php if (empty($es_transparencia_doc_file_title)) {
                          the_title();
                        } else {
                          echo $es_transparencia_doc_file_title;
                        } ?>" target="_blank" download><i class="fas fa-file-download fa-lg"></i> <span>Descargar</span></a>
                      </div>
                    </div>

                    <div class="es-transparencia-doc-file-info es-cell-item">

                      <div class="es-transparencia-doc-file-meta es-cell-item">

                        <div class="es-transparencia-doc-file-icon es-cell-item">
                          <i class="<?php echo $es_transparencia_doc_file_icon; ?>"  title="<?php echo $es_transparencia_doc_file_alt; ?>"></i>
                        </div>

                        <div class="es-transparencia-doc-file-meta-copy es-cell-item">
                          <small class="es-transparencia-doc-file-meta-type"><?php echo $es_transparencia_doc_file_alt; ?></small>
                          <small class="es-transparencia-doc-file-meta-size">(<?php echo  $es_transparencia_doc_file_size_format; ?>)</small><br>
                        </div>

                      </div>

                      <div class="es-transparencia-doc-file-date-modified es-cell-item">
                        <small ">Publicado: <?php echo $es_transparencia_updated_date; ?></small>
                      </div>

                    </div>

                    </div>

            </div>

            </div>

          </div>
        </li>
      </ul>
    </div>

     <?php  include(locate_template( '/framework/views/transparencia-doc/transparencia-doc-reveal.php')); ?>

  <?php endif ?>