<?php

// =============================================================================
// VIEWS/GLOBAL/_CONTENT-NONE.PHP
// -----------------------------------------------------------------------------
// Output when no posts or pages are available.
// =============================================================================

?>

<div class="entry-content">
  <div class="x-container">
    <div class="x-column x-sm x-1-4">
      <?php dynamic_sidebar('ups-sidebar-edesur-sidebar-transparencia'); ?>
    </div>



    <div class="x-column x-sm x-3-4">
      <div class="x-column x-sm cs-ta-center x-1-1">

      <?php

      if ( $post->post_name == 'presupuesto' ||
            $post->post_name == 'balance-general' ||
            $post->post_name == 'plan-anual-compras' ||
            $post->post_name == 'estado-cuentas-suplidores' ||
            $post->post_name == 'manual-organizacion-oai' ||
            $post->post_name == 'manual-procedimiento-oai' ||
            $post->post_name == 'indice-documentos' ||
            $post->post_name == 'inventario-almacen'  )
      {
        $es_pub_verb = 'publicó el ';
      }
      elseif ( $post->post_name == 'ejecucion-presupuesto' ||
                    $post->post_name == 'estructura-organizacional-oai' ||
                    $post->post_name == 'informacion-clasificada' )
      {
        $es_pub_verb = 'publicó la ';
      } else
      {
        $es_pub_verb = 'publicaron ';
      }

      $current_month = date('F');
      $current_month_digit = date('m');
      $current_year = date('Y');

      $display_month = Date('F', strtotime($current_month . " last month"));
      $display_year = ($current_month_digit == 01) ? Date('Y', strtotime($current_year . " last year")) : strftime('%Y');
       ?>


        <?php if ( $post->post_name == 'jubilaciones-pensiones-retiros' ): ?>
          <h3 class="h-custom-headline h4">No Aplica. Esta empresa no posee fondos aprobados para estos fines.</h3>
        <?php elseif ($post->post_name == 'beneficiarios'): ?>
            <h3 class="h-custom-headline h4">No Aplica. Esta empresa no posee fondos complementarios para estos fines.</h3>
        <?php else: ?>
          <h3 class="h-custom-headline h5">Durante el mes de <?php echo __($display_month) . ' del ' . $display_year;  ?> no se  <?php echo $es_pub_verb; the_title(); ?>.</h3>
        <?php endif ?>

      </div>
    </div>
  </div>
</div>