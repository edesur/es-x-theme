<?php

function es_get_file_meta($es_doc_file_path) {

  $es_url_uploads = wp_upload_dir()['baseurl'];
  $es_dir_uploads = wp_upload_dir()['basedir'];

  $es_doc_file_url = $es_url_uploads . $es_doc_file_path;
  $es_doc_file_dir = $es_dir_uploads . $es_doc_file_path;
  $es_doc_file_id = es_get_attachment_id($es_doc_file_url);
  $es_doc_file_mime_type = get_post_mime_type( $es_doc_file_id );
  $es_doc_file_title = get_the_title($es_doc_file_id);
  $es_doc_file_type = get_post_type($es_doc_file_id);
  $es_doc_file_size = filesize($es_doc_file_dir);
  $es_doc_file_size_format = size_format($es_doc_file_size, 2);
   $es_doc_file_updated_date = get_the_date('j \d\e F, Y',$es_doc_file_id);

  switch ($es_doc_file_mime_type) {
    case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
      $es_doc_file_class = 'es-transparencia-doc-file';
      $es_doc_file_alt = 'MicroSoft Word';
      $es_doc_file_icon = 'far fa-file-word';
      break;
    case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
      $es_doc_file_class = 'es-transparencia-doc-file';
      $es_doc_file_alt = 'MicroSoft Excel';
      $es_doc_file_icon = 'far fa-file-excel';
      break;

    case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
      $es_doc_file_class = 'es-transparencia-doc-file';
      $es_doc_file_alt = 'Office Open XML';
      $es_doc_file_icon = 'fas fa-file-excel';
      $es_doc_file_desc = 'Hoja de cálculo de Office Open XML';
      break;

    case 'application/vnd.ms-excel':
      $es_doc_file_class = 'es-transparencia-doc-file';
      $es_doc_file_alt = 'MicroSoft Excel';
      $es_doc_file_icon = 'far fa-file-excel';
      break;
    case 'application/vnd.ms-excel.sheet.macroEnabled.12':
      $es_doc_file_class = 'es-transparencia-doc-file';
      $es_doc_file_alt = 'MicroSoft Excel';
      $es_doc_file_icon = 'far fa-file-excel';
      break;

    case 'text/csv':
      $es_doc_file_class = 'es-transparencia-doc-file';
      $es_doc_file_alt = 'Comma-separated values';
      $es_doc_file_icon = 'fas fa-file-csv';
      $es_doc_file_desc = 'Valores separados por comas';
      break;

    case 'application/vnd.oasis.opendocument.spreadsheet':
      $es_doc_file_class = 'es-transparencia-doc-file';
      $es_doc_file_alt = 'OpenDocument Spreadsheet';
      $es_doc_file_icon = 'fas fa-file-alt';
      $es_doc_file_desc = 'Hoja de cálculo de documento abierto';
      break;

    case 'application/pdf':
      $es_doc_file_class = 'es-transparencia-doc-file';
      $es_doc_file_alt = 'Adobe PDF';
      $es_doc_file_icon = 'far fa-file-pdf';
      break;

    default:
      $es_doc_file_alt = 'Archivo desconocido';
      $es_doc_file_class = 'es-transparencia-doc-file-none';
      $es_doc_file_icon = 'far fa-file';
      break;
  }

  $es_file_meta = array(
    'url' => $es_doc_file_url,
    'dir' => $es_doc_file_dir,
    'id' => $es_doc_file_id,
    'title' => $es_doc_file_title,
    'type' =>$es_doc_file_type,
    'mime' => $es_doc_file_mime_type,
    'size' => $es_doc_file_size,
    'format' => $es_doc_file_size_format,
    'date' => $es_doc_file_updated_date,
    'alt' => $es_doc_file_alt,
    'class' => $es_doc_file_class,
    'icon' => $es_doc_file_icon
  );

  return $es_file_meta;
}

function es_get_attachment_id($image_url) {
  global $wpdb;
  $attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $image_url ));
        return $attachment[0];
}

?>