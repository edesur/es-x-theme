<?php

// =============================================================================
// VIEWS/GLOBAL/_CONTENT-NONE.PHP
// -----------------------------------------------------------------------------
// Output when no posts or pages are available.
// =============================================================================

?>

<?php
  $es_url_uploads = wp_upload_dir()['baseurl'];
  $es_dir_uploads = wp_upload_dir()['basedir'];
  ?>

  <div class="cell small-12 large-9">
        <p>En el portal <a href="https://map.gob.do/Concursa/Default.aspx" title="MAP Concursa" target="_blank">MAP Concursa</a>, administrado por el <a href="https://map.gob.do/" title="Ministerio Administracion Publica" target="_blank">Ministerio Administracion Publica</a>, los ciudadanos que no pertenecen a la administración pública y los servidores públicos podrán saber cuáles son las vacantes que actualmente existen en las distintas instituciones públicas bajo el ámbito de la Ley 41-08, cuál es el perfil de esos cargos, aplicar a esos concursos si cumplen con los requisitos del perfil, y dar seguimiento a los resultados de las diferentes fases de evaluación, también ver las noticias relacionadas con el reclutamiento y selección de personas, y las informaciones estadísticas de las instituciones que mayores concursos públicos realizan y sobre los cargos más concursados, entre otros datos.</p>

    <p>Para más información y aplicar por vacantes visite la sección <a href="https://map.gob.do/Concursa/plazasvacantes.aspx" title="Plazas Vacantes - MAP Concursa" target="_blank">Plazas Vacantes</a></p>
  </div>