<?php

// =============================================================================
// VIEWS/INTEGRITY/CONTENT.PHP
// -----------------------------------------------------------------------------
// Standard post output for Integrity.
// =============================================================================

?>

  <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

        <div class="entry-wrap">

           <div class="es-section-grid-servicios-info">
             <div class="es-cell-item-content-servicios-info">

                 <div class="es-cell-item-entry-featured ">
                   <?php x_featured_image(); ?>
                 </div>

                 <header class="es-cell-item-entry-header">
                   <h1 class="entry-title"><?php the_title(); ?></h1>
                 </header>

                 <div class="es-cell-item-the-content">
                   <?php echo get_the_content(); ?>
                 </div>

                <?php
                  include(locate_template('/framework/views/informacion/_content-excerpt-download.php'));
                ?>

                <?php if ($post->post_name == 'tarifa-electrica'): ?>

                  <h5 class="es-doc-copy-title-servicios-info">Publicaciones de la <?php the_title(); ?></h5>
                  <?php  include(locate_template( '/framework/views/transparencia-doc/_index.php')); ?>

                <?php endif ?>

                <?php echo do_shortcode('[addthis tool="addthis_inline_share_toolbox_hfga"]');  ?>

             </div>

             <div class="es-cell-item-sidebar-servicios-info">
                <?php echo do_shortcode( '[the_grid name="Edesur Información de Servicios The Grid"]' );  ?>
             </div>

           </div>

        </div>

    <?php x_get_view('integrity', '_content', 'post-footer'); ?>
  </article>