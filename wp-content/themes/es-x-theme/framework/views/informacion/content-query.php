<?php

// =============================================================================
// VIEWS/INFORMACION/CONTENT-QUERY.PHP
// -----------------------------------------------------------------------------
// Standard post output for Integrity.
// =============================================================================

?>

<?php
  $es_index_info_query_args = array(
    'post_type' => 'informacion',
    'posts_per_archive_page'  => -1,
  );

  $es_index_info_query = new WP_Query( $es_index_info_query_args );
?>

 <?php if ( $es_index_info_query->have_posts() ) : ?>

  <!-- start of the loop -->
  <?php while ( $es_index_info_query->have_posts() ) : $es_index_info_query->the_post(); ?>
    <?php x_get_view( $stack, 'content', get_post_format() ); ?>
  <?php endwhile; ?><!-- end of the loop -->

  <!-- put pagination functions here -->
  <?php wp_reset_postdata(); ?>

<?php else:  ?>

  <?php x_get_view( 'global', '_content-none' ); ?>

<?php endif; ?>