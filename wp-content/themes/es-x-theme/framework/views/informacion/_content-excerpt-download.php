<?php if ( have_rows('es_servicio_documentos') ): ?>

  <?php while( have_rows('es_servicio_documentos') ): the_row();

    $es_doc_file = get_sub_field('es_servicio_documento_individual');
    $es_transparencia_doc_file_id = get_post_type() . '_' . $es_doc_file['id'];
    $es_doc_file_url = $es_doc_file['url'];
    $es_doc_file_title = $es_doc_file['title'];
    $es_doc_file_attrs = es_get_file_attrs($es_doc_file['mime_type']);
    $es_doc_file_dir = get_attached_file( $es_doc_file['id'] );
    $es_doc_file_size = filesize( $es_doc_file_dir );
    $es_doc_file_size_format = size_format($es_doc_file_size, 2);
    $es_doc_file_date = date( 'j \d\e F, Y', strtotime($es_doc_file['modified']) );
    ?>

    <?php if (is_archive() ): ?>
      <div class="es-section-grid-servicios-info">
        <div class="es-cell-item-doc-servicios-info">
          <div class="es-cell-item-doc-download-servicios-info <?php echo $es_doc_file_attrs['class']; ?>">

              <div class="es-cell-item-doc-download-file-servicios-info <?php echo $es_doc_file_attrs['class']; ?>">
               <div class="es-cell-item-doc-download-file-button">
                 <a href="<?php echo $es_doc_file_url; ?>" class="button tiny expanded" title="Descargar: <?php the_title(); ?>">Descargar</a>
               </div>
               <div class="es-cell-item-doc-download-file-size">
                 <small>(<?php echo  $es_doc_file_size_format; ?>)</small>
               </div>
              </div>

             <div class="es-cell-item-doc-download-file <?php echo $es_doc_file_attrs['class']; ?>">
               <i class="<?php echo $es_doc_file_attrs['icon']; ?>"  title="Descargar: <?php the_title(); ?>"></i>
             </div>
           </div>
        </div>

      </div>

      <?php echo do_shortcode('[addthis tool="addthis_inline_share_toolbox_hfga"]');  ?>
    <?php endif ?>

      <?php if ( is_singular() ): ?>
        <div class="es-cell-item-doc-servicios-info">
          <div class="es-site-transparencia-docs">
            <div class="es-transparencia-doc-content grid-container">

            <div class="es-transparencia-doc-single grid-x align-middle">

                <div class="es-transparencia-doc-copy">
                  <h4 class="es-transparencia-doc-copy-title h5">Folleto de<?php the_title(); ?></h4>
                  <p class="es-transparencia-doc-copy-desc">Folleto acerca de <?php the_title(); ?> disponible para descargar</p>
                </div>

                <div class="es-transparencia-doc-download <?php echo $es_doc_file_attrs;['class'] ?>">

                    <div class="es-transparencia-file-tools es-cell-item">
                      <div class="button-group expanded small">
                        <a href="<?php echo urlencode($es_doc_file_url); ?>" class="es-transparencia-doc-tools-btn button" data-toggle="<?php echo $es_transparencia_doc_file_id ?>" type="<?php echo $es_doc_file_attrs['mime_type']; ?>"><i class="far fa-eye fa-lg"></i> <span>Vista previa</span></a>

                        <a class="es-doc-tools-btn-down es-transparencia-doc-tools-btn button" href="<?php echo $es_doc_file_url; ?>" title="Descargar: <?php if (empty($es_doc_file_title)) {
                          the_title();
                        } else {
                          echo $es_doc_file_title;
                        } ?>" target="_blank" download><i class="fas fa-file-download fa-lg"></i> <span>Descargar</span></a>
                      </div>
                    </div>

                    <div class="es-transparencia-doc-file-info es-cell-item">

                      <div class="es-transparencia-doc-file-meta es-cell-item">

                        <div class="es-transparencia-doc-file-icon es-cell-item">
                          <i class="<?php echo $es_doc_file_attrs['icon']; ?>"  title="<?php echo $es_doc_file_attrs['alt']; ?>"></i>
                        </div>

                        <div class="es-transparencia-doc-file-meta-copy es-cell-item">
                          <small class="es-transparencia-doc-file-meta-type"><?php echo $es_doc_file_attrs['alt']; ?></small>
                          <small class="es-transparencia-doc-file-meta-size">(<?php echo  $es_doc_file_size_format; ?>)</small><br>
                        </div>

                      </div>

                      <div class="es-transparencia-doc-file-date-modified es-cell-item">
                        <small ">Publicado: <?php echo $es_doc_file_date; ?></small>
                      </div>

                    </div>

                    </div>

            </div>

            </div>
          </div>
         </div>
      <?php endif ?>




    <?php if (is_archive() ): ?>

    <?php endif ?>

  <?php endwhile; ?>

  <?php if ( is_singular() ): ?>

    <div class="es-cell-item">
      <small>Estas extensiones son recomendadas para visualizar los documentos en el explorador:</small>

      <div class="es-transparencia-download-acrobat media-object align-middle" style="margin-bottom: 0.125rem">
        <div class="es-transparencia-doc-file-pdf media-object-section">
          <i class="far fa-file-pdf"></i>
        </div>
        <div class="media-object-section">
          <small><a href="https://get.adobe.com/reader/?loc=es" title="Descargar Adobe Acrobat Reader DC" target="_blank" style="color: hsl(0, 100%, 50%);">Abobe Reader DC</a> para documentos PDF</small>
        </div>
      </div>

      <div class="es-transparencia-download-acrobat media-object align-middle">
        <div class="es-transparencia-doc-file-pdf media-object-section">
          <i class="fab fa-windows" style="color: #d83b01;"></i>
        </div>
        <div class="media-object-section">
          <small><a href="https://support.office.com/es-es/article/acceso-rápido-a-los-archivos-de-office-en-el-explorador-dc1024b4-92be-46eb-81a7-aea85368baa0" target="_blank" title="Acceso rápido a los archivos de Office en el explorador
          " style="color: #d83b01;">Office Online</a> para documentos Word y Excel  para <a href="https://chrome.google.com/webstore/detail/office-online/ndjpnladcallmjemlbaebfadecfhkepb?hl=es-419" title="Descargar: Office Online | Chrome Web Store" target="_blank" style="color: #d83b01;">Google Chrome</a> y <a href="https://www.microsoft.com/es-us/p/office-online/9nblggh4v88g?activetab=pivot%3aoverviewtab" title="Descargar: Office Online | Microsoft Store Online" style="color: #d83b01;">Microsoft Edge</a></small>
        </div>
      </div>
    </div>

  <?php endif ?>

<?php endif ?>