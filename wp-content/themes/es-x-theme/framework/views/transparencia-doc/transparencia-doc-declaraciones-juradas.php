<div class="es-transparencia-doc-content grid-container mbl">

<div class="callout warning">

  <div class="es-transparencia-doc-single grid-x align-middle">

      <div class="es-transparencia-doc-copy">
        <p class="es-transparencia-doc-copy-desc">Los datos personales han sido protegidos de conformidad con el art. 18 de la Ley 200-04 de Libre Acceso Información Pública, de fecha 28 de julio de 2004 y el Articulo 28 de su Reglamento de Aplicación, aprobado mediante Decreto 130-05, de fecha 25 de febrero de 2005</p>
      </div>

  </div>

</div>

</div>