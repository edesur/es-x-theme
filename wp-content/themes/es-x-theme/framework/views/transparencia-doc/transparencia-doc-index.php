<?php

// =============================================================================
// VIEWS/TRANSPARENCIA DOCS/INDEX-MONTH.PHP
// -----------------------------------------------------------------------------
// Includes the index output.
// =============================================================================

?>

<?php
  if (/*$es_transparencia_docs_terms &&*/
                    ( $es_transparencia_docs_object->slug == 'plan-estrategico-institucional' ||
                     $es_transparencia_docs_object->slug == 'plan-operativo-anual' ||
                     $es_transparencia_docs_object->slug == 'informes-poa' ||
                     $es_transparencia_docs_object->slug == 'oai-estadisticas' ||
                     $es_transparencia_docs_object->slug == 'comision-etica-publica' ||
                     $es_transparencia_docs_object->slug == 'plan-trabajo' ||
                     $es_transparencia_docs_object->slug == 'informes' ||
                    $es_transparencia_docs_object->slug == 'memorias-plan-estrategico' ||
                     $es_transparencia_docs_object->slug == '311-estadisticas' ||
                      $es_transparencia_docs_object->slug == 'presupuesto-aprobado' ||
                      $es_transparencia_docs_object->slug == 'plan-anual-compras' )) {
    $es_transparencia_docs_query_index = $es_transparencia_docs_query_year;
  } /*elseif (is_page()) {

  }*/ else {
    $es_transparencia_docs_query_index = $es_transparencia_docs_query_month;
  }
?>

  <?php if ( $es_transparencia_docs_query_index->have_posts() ): ?>

    <div class="es-transparencia-doc-content grid-container">
      <?php while ( $es_transparencia_docs_query_index->have_posts() ) : $es_transparencia_docs_query_index->the_post(); ?>

        <?php
          $es_transparencia_doc_type_field = get_field('es_transparencia_documento_type');
          $es_transparencia_doc_type = $es_transparencia_doc_type_field->slug;
          $es_transparencia_doc_type_name = $es_transparencia_doc_type_field->name;
          $es_transparencia_doc_type_desc = $es_transparencia_doc_type_field->description;

          // Frequency
          $es_transparencia_doc_frecuency_field = get_field('es_transparencia_documento_frecuency');
          $es_transparencia_doc_frecuency = $es_transparencia_doc_frecuency_field['value'];

          // File
          $es_transparencia_doc_file = get_field('es_transparencia_documento_file');
          $es_transparencia_doc_file_url = $es_transparencia_doc_file['url'];
          $es_transparencia_doc_file_title = $es_transparencia_doc_file['title'];
          $es_transparencia_doc_file_dir = get_attached_file($es_transparencia_doc_file['id']);
          $es_transparencia_doc_file_size = filesize($es_transparencia_doc_file_dir);
          $es_transparencia_doc_file_size_format = size_format($es_transparencia_doc_file_size, 2);
          $es_transparencia_doc_file_mime_type = $es_transparencia_doc_file['mime_type'];
          $es_transparencia_doc_file_id = $es_transparencia_doc_type . '-' . $es_transparencia_doc_file['id'];

          // Date
          $es_transparencia_doc_date_field = get_field('es_transparencia_documento_date');
          $es_transparencia_doc_date_start_field = get_field('es_transparencia_documento_date_period_start');
          $es_transparencia_doc_date_end_field = get_field('es_transparencia_documento_date_period_end');

          $es_transparencia_doc_date = strtotime($es_transparencia_doc_date_field);
          $es_transparencia_doc_date_start = strtotime( $es_transparencia_doc_date_start_field );
          $es_transparencia_doc_date_end = strtotime( $es_transparencia_doc_date_end_field );

          // Months
          $es_transparencia_doc_date_month = date_i18n('F', $es_transparencia_doc_date);
          $es_transparencia_doc_date_start_month = date_i18n('F', $es_transparencia_doc_date_start);
          $es_transparencia_doc_date_end_month = date_i18n('F', $es_transparencia_doc_date_end);

          // Years
          $es_transparencia_doc_date_year = date_i18n('Y', $es_transparencia_doc_date);
          $es_transparencia_doc_date_start_year = date_i18n('Y', $es_transparencia_doc_date_start);
          $es_transparencia_doc_date_end_year = date_i18n('Y', $es_transparencia_doc_date_end);

          //  Published and modified date
          $es_transparencia_time = get_the_time('U');
          $es_transparencia_modified_time = get_the_modified_time('U');
          if ($es_transparencia_modified_time >= $es_transparencia_time + 86400) {
          $es_transparencia_updated_date = get_the_modified_time('j \d\e F, Y');
          $es_transparencia_updated_time = get_the_modified_time('h:i a');
          } else {
             $es_transparencia_updated_date = get_the_date();
           }

          switch ($es_transparencia_doc_file_mime_type) {
            case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
              $es_transparencia_doc_file_class = 'es-transparencia-doc-file';
              $es_transparencia_doc_file_alt = 'MicroSoft Word';
              $es_transparencia_doc_file_icon = 'far fa-file-word';
              break;
            case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
              $es_transparencia_doc_file_class = 'es-transparencia-doc-file';
              $es_transparencia_doc_file_alt = 'MicroSoft Excel';
              $es_transparencia_doc_file_icon = 'far fa-file-excel';
              break;

            case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
              $es_transparencia_doc_file_class = 'es-transparencia-doc-file';
              $es_transparencia_doc_file_alt = 'Office Open XML';
              $es_transparencia_doc_file_icon = 'fas fa-file-excel';
              $es_transparencia_doc_file_desc = 'Hoja de cálculo de Office Open XML';
              break;

            case 'application/vnd.ms-excel':
              $es_transparencia_doc_file_class = 'es-transparencia-doc-file';
              $es_transparencia_doc_file_alt = 'MicroSoft Excel';
              $es_transparencia_doc_file_icon = 'far fa-file-excel';
              break;
            case 'application/vnd.ms-excel.sheet.macroEnabled.12':
              $es_transparencia_doc_file_class = 'es-transparencia-doc-file';
              $es_transparencia_doc_file_alt = 'MicroSoft Excel';
              $es_transparencia_doc_file_icon = 'far fa-file-excel';
              break;

            case 'text/csv':
              $es_transparencia_doc_file_class = 'es-transparencia-doc-file';
              $es_transparencia_doc_file_alt = 'Comma-separated values';
              $es_transparencia_doc_file_icon = 'fas fa-file-csv';
              $es_transparencia_doc_file_desc = 'Valores separados por comas';
              break;

            case 'application/vnd.oasis.opendocument.spreadsheet':
              $es_transparencia_doc_file_class = 'es-transparencia-doc-file';
              $es_transparencia_doc_file_alt = 'OpenDocument Spreadsheet';
              $es_transparencia_doc_file_icon = 'fas fa-file-alt';
              $es_transparencia_doc_file_desc = 'Hoja de cálculo de documento abierto';
              break;

            case 'application/pdf':
              $es_transparencia_doc_file_class = 'es-transparencia-doc-file';
              $es_transparencia_doc_file_alt = 'Adobe PDF';
              $es_transparencia_doc_file_icon = 'far fa-file-pdf';
              break;

            default:
              $es_transparencia_doc_file_class = 'es-transparencia-doc-file-none';
              $es_transparencia_doc_file_icon = 'far fa-file';
              break;
          }

        if ( $es_transparencia_doc_frecuency == 'tri-semestral' && $es_transparencia_doc_type == 'plan-estrategico-institucional' ) {

          $es_transparencia_doc_date_period = $es_transparencia_doc_date_start_year . ' - ' . $es_transparencia_doc_date_end_year;

        } elseif ( $es_transparencia_doc_frecuency == 'tri-semestral') {

          $es_transparencia_doc_date_period = $es_transparencia_doc_date_start_month . ' - ' . $es_transparencia_doc_date_end_month .' ' . $es_transparencia_doc_date_end_year;

        } elseif ( $es_transparencia_doc_frecuency == 'anual' ) {

          $es_transparencia_doc_date_period = $es_transparencia_doc_date_year;

        } else {
          $es_transparencia_doc_date_period = $es_transparencia_doc_date_month . ' ' . $es_transparencia_doc_date_year;
        }

        switch ($es_transparencia_doc_type) {

          case 'activos':
            $es_transparencia_doc_desc = $es_transparencia_doc_type_desc;
            break;

           case 'activos-fijos':
            $es_transparencia_doc_desc = 'Propiedad, Planta y Equipo';
            break;

          case 'beneficiarios':
           $es_transparencia_doc_desc = $es_transparencia_doc_type_desc;
           break;

          case 'egresos':
              $es_transparencia_doc_desc = 'Estado de Gastos Operacionales y Financieros';
              break;

          case 'ejecucion-presupuesto':
            $es_transparencia_doc_desc = 'Informe de Ejecución Presupuestaria';
            break;

          case 'estadisticas-institucionales':
            $es_transparencia_doc_desc = 'Evolutivo Indicadores de Desempeño';
            break;

            case 'estado-resultados':
              $es_transparencia_doc_desc = 'Estado de Resultados';
              break;

            case 'estado-cuentas-suplidores':
              $es_transparencia_doc_desc = $es_transparencia_doc_type_desc;
              break;

            case '311-estadisticas':
              $es_transparencia_doc_desc = $es_transparencia_doc_type_desc;
              break;

            case 'ingresos':
              $es_transparencia_doc_desc = 'Estado de Ingresos Operacionales y Financieros';
              break;

            case 'inventario-almacen':
              $es_transparencia_doc_desc = 'Inventario en Almacén';
              break;

            case 'memorias-plan-estrategico':
              $es_transparencia_doc_desc = $es_transparencia_doc_type_desc;
              break;

            case 'nomina-transparencia':
              $es_transparencia_doc_desc = 'Nómina de Transparencia';
              break;

            case 'oai-estadisticas':
              $es_transparencia_doc_desc = 'Estadísticas de la Oficina de Acceso a la Información (OAI)';
              break;

            case 'oai-indice-documentos-disponibles':
              $es_transparencia_doc_desc = 'Índice de documentos disponibles para entrega por la Oficina de Libre Acceso a la Información (OAI)';
              break;

            case 'oai-informacion-clasificada':
              $es_transparencia_doc_desc = $es_transparencia_doc_type_desc;
              break;

            case 'pasivo-capital':
              $es_transparencia_doc_desc = $es_transparencia_doc_type_desc;
              break;

            case 'presupuesto-aprobado':
              $es_transparencia_doc_desc = 'Presupuesto Aprobado para el año';
              break;

            case 'plan-estrategico-institucional':
              $es_transparencia_doc_desc = $es_transparencia_doc_type_name;
              break;

            case 'plan-trabajo':
              $es_transparencia_doc_desc = $es_transparencia_doc_type_desc;
              break;

            case 'informes':
              $es_transparencia_doc_desc = $es_transparencia_doc_type_desc;
              break;

              case 'informes-poa':
              $es_transparencia_doc_desc = $es_transparencia_doc_type_desc;
              break;

          default:
            $es_transparencia_doc_desc = $es_transparencia_doc_type_name;
            break;
        }

        ?>

        <?php

         ?>

      <div class="es-transparencia-doc-single grid-x align-middle">

          <div class="es-transparencia-doc-copy">
            <h4 class="es-transparencia-doc-copy-title h5"><?php the_title(); ?></h4>
            <p class="es-transparencia-doc-copy-desc"><?php echo $es_transparencia_doc_desc . ' - ' . $es_transparencia_doc_date_period; ?></p>
          </div>

          <div class="es-transparencia-doc-download <?php echo $es_transparencia_doc_file_class; ?>">

              <div class="es-transparencia-file-tools es-cell-item">
                <div class="button-group expanded small">
                  <a href="<?php echo urlencode($es_transparencia_doc_file_url); ?>" class="es-transparencia-doc-tools-btn button" data-toggle="<?php echo $es_transparencia_doc_file_id ?>" type="<?php echo $es_transparencia_doc_file_mime_type; ?>"><i class="far fa-eye fa-lg"></i> <span>Vista previa</span></a>

                  <a class="es-doc-tools-btn-down es-transparencia-doc-tools-btn button" href="<?php echo $es_transparencia_doc_file_url; ?>" title="Descargar: <?php if (empty($es_transparencia_doc_file_title)) {
                    the_title();
                  } else {
                    echo $es_transparencia_doc_file_title;
                  } ?>" target="_blank" download><i class="fas fa-file-download fa-lg"></i> <span>Descargar</span></a>
                </div>
              </div>

              <div class="es-transparencia-doc-file-info es-cell-item">

                <div class="es-transparencia-doc-file-meta es-cell-item">

                  <div class="es-transparencia-doc-file-icon es-cell-item">
                    <i class="<?php echo $es_transparencia_doc_file_icon; ?>"  title="<?php echo $es_transparencia_doc_file_alt; ?>"></i>
                  </div>

                  <div class="es-transparencia-doc-file-meta-copy es-cell-item">
                    <small class="es-transparencia-doc-file-meta-type"><?php echo $es_transparencia_doc_file_alt; ?></small>
                    <small class="es-transparencia-doc-file-meta-size">(<?php echo  $es_transparencia_doc_file_size_format; ?>)</small><br>
                  </div>

                </div>

                <div class="es-transparencia-doc-file-date-modified es-cell-item">
                  <small ">Publicado: <?php echo $es_transparencia_updated_date; ?></small>
                </div>

              </div>

              </div>

      </div>

      <?php endwhile; ?>

    </div>

  <?php else: ?>

    <?php

      if ( $es_transparencia_docs_object->slug == 'plan-estrategico-institucional' ||
                        $es_transparencia_docs_object->slug == 'plan-operativo-anual' ||
                        $es_transparencia_docs_object->slug == 'informes-poa' ||
                        $es_transparencia_docs_object->slug == 'presupuesto-aprobado' ||
                        $es_transparencia_docs_object->slug == 'comision-etica-publica' ||
                        $es_transparencia_docs_object->slug == 'plan-trabajo' ||
                        $es_transparencia_docs_object->slug == 'informes' ||
                        $es_transparencia_docs_object->slug == 'memorias-plan-estrategico' ||
                        $es_transparencia_docs_object->slug == '311-estadisticas' ||
                        $es_transparencia_docs_object->slug == 'oai-estadisticas' ||
                        $es_transparencia_docs_object->slug == 'plan-anual-compras') {

        $es_transparencia_doc_date_period = $es_transparencia_year;

      } else {
        $es_transparencia_doc_date_period = $es_transparencia_doc_month_i18n . ' ' . $es_transparencia_year;
      }

      if (!empty($es_transparencia_docs_term) ) {
        $es_transparencia_docs_term_slug = $es_transparencia_docs_term->slug;
        $es_transparencia_docs_term_name = $es_transparencia_docs_term->name;
        $es_transparencia_docs_term_description = $es_transparencia_docs_term->description;
      } elseif (is_page() || get_post_type() == 'informacion') {
        $es_transparencia_docs_term_slug = $es_transparencia_docs_object->post_name;
        $es_transparencia_docs_term_name = $es_transparencia_docs_object->post_title ;
        $es_transparencia_docs_term_description = $es_transparencia_docs_object->description;
      } else {
        $es_transparencia_docs_term_slug = $es_transparencia_docs_object->slug;
        $es_transparencia_docs_term_name = $es_transparencia_docs_object->name ;
        $es_transparencia_docs_term_description = $es_transparencia_docs_object->description;
      }

      // $es_transparencia_docs_term_slug = ( $es_transparencia_docs_terms ) ? $es_transparencia_docs_term->slug : $es_transparencia_docs_object->slug ;
      // $es_transparencia_docs_term_name = ( $es_transparencia_docs_terms ) ? $es_transparencia_docs_term->name : $es_transparencia_docs_object->name ;
      // $es_transparencia_docs_term_description = ( $es_transparencia_docs_terms ) ? $es_transparencia_docs_term->description : $es_transparencia_docs_object->description;

      // $es_transparencia_doc_month_i18n
      switch ($es_transparencia_docs_term_slug) {

        case 'balance-general':
        $es_transparencia_docs_term_title = $es_transparencia_docs_term_name;
          $es_transparencia_doc_desc_none = 'No existen ' . $es_transparencia_docs_term_name . ' disponible en el período ' . $es_transparencia_doc_date_period;
          break;

        case 'activos-fijos':
        $es_transparencia_docs_term_title = $es_transparencia_docs_term_name;
          $es_transparencia_doc_desc_none = 'No existen ' . $es_transparencia_docs_term_name . ' disponibles para el período ' . $es_transparencia_doc_date_period;
          break;

        case 'beneficiarios':
        $es_transparencia_docs_term_title = $es_transparencia_docs_term_name;
          $es_transparencia_doc_desc_none = 'La institución no posee fondos complementarios para programas asistenciales' . ' en el período ' . $es_transparencia_doc_date_period;
          // $es_transparencia_doc_desc_none = 'En el mes de ' . $es_transparencia_doc_date_period . ' no tenemos ' . $es_transparencia_docs_term_name;
          // $es_transparencia_doc_desc_none = 'No existen ' . $es_transparencia_docs_term_description . ' en el período ' . $es_transparencia_doc_date_period;
          break;

        case 'jubilaciones-pensiones-retiros':
        $es_transparencia_docs_term_title = $es_transparencia_docs_term_name;
          $es_transparencia_doc_desc_none = 'La institución no posee fondos complementarios para ' . $es_transparencia_docs_term_name . ' en el período ' . $es_transparencia_doc_date_period;
          break;

          case 'oai-informacion-clasificada':
          $es_transparencia_docs_term_title = $es_transparencia_docs_term_name;
          $es_transparencia_doc_desc_none = 'No existen ' . $es_transparencia_docs_term_name . ' en el período ' . $es_transparencia_doc_date_period;
          break;

          case 'estado-cuentas-suplidores':
          $es_transparencia_docs_term_title = $es_transparencia_docs_term_description;
          $es_transparencia_doc_desc_none = 'No existen ' . $es_transparencia_docs_term_description . ' en el período ' . $es_transparencia_doc_date_period;
          break;

          case 'publicaciones':
          $es_transparencia_docs_term_title = $es_transparencia_docs_term_description;
          $es_transparencia_doc_desc_none = 'La institución no posee ' . $es_transparencia_docs_term_description . ' en el período ' . $es_transparencia_doc_date_period;
          break;

          case 'publicaciones-oficiales':
          $es_transparencia_docs_term_title = $es_transparencia_docs_term_name;
          $es_transparencia_doc_desc_none = 'La institución no posee ' . $es_transparencia_docs_term_title . ' en el período ' . $es_transparencia_doc_date_period;
          break;

        case 'vacantes':
        $es_transparencia_docs_term_title = $es_transparencia_docs_term_name;
          $es_transparencia_doc_desc_none = 'No existen ' . $es_transparencia_docs_term_name . ' disponibles para el período ' . $es_transparencia_doc_date_period;
          break;

          case 'informes-auditorias':
          $es_transparencia_docs_term_title = $es_transparencia_docs_term_name;
            $es_transparencia_doc_desc_none = 'No existen ' . $es_transparencia_docs_term_name . ' disponibles para el período ' . $es_transparencia_doc_date_period;
            break;

          case 'informes-poa':
          $es_transparencia_docs_term_title = $es_transparencia_docs_term_description;
            $es_transparencia_doc_desc_none = 'No existen ' .  $es_transparencia_docs_term_description . ' disponibles para el período ' . $es_transparencia_doc_date_period;
            break;

        default:
        $es_transparencia_docs_term_title = $es_transparencia_docs_term_name;
        $es_transparencia_doc_desc_none = 'No existen ' . $es_transparencia_docs_term_name . ' en el período ' . $es_transparencia_doc_date_period;
          break;
      }
    ?>
    <div class="es-transparencia-doc-copy">
      <h4 class="es-transparencia-doc-copy-title h5"><?php echo $es_transparencia_docs_term_title . ' - ' . $es_transparencia_doc_date_period; ?></h4>
      <p><?php echo $es_transparencia_doc_desc_none; ?></p>
    </div>

  <?php endif ?>