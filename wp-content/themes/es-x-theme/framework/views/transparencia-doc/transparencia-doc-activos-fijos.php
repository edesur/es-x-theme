<?php
  require_once(locate_template( '/framework/views/es-transparencia/_es-content-variables.php'));

    $es_doc_file_meta_miembros =  es_get_file_meta('/2018/04/edesur-transparencia-finanzas-inventario-almacen-carta-bienes-nacionales.pdf');
     $es_transparencia_doc_file_id = $es_doc_file_meta_miembros['type'] . '-' . $es_doc_file_meta_miembros['id'];
     $es_transparencia_doc_file_class = $es_doc_file_meta_miembros['class'];
     $es_transparencia_doc_file_alt = $es_doc_file_meta_miembros['alt'];
     $es_transparencia_doc_file_mime_type = $es_doc_file_meta_miembros['mime'];
     $es_transparencia_doc_file_url = $es_doc_file_meta_miembros['url'];
     $es_transparencia_doc_file_title = $es_doc_file_meta_miembros['title'];
     $es_transparencia_doc_file_dir = $es_doc_file_meta_miembros['dir'];
     $es_transparencia_doc_file_icon = $es_doc_file_meta_miembros['icon'];
     $es_transparencia_doc_file_size = filesize($es_doc_file_meta_miembros['dir']);
     $es_transparencia_doc_file_size_format = $es_doc_file_meta_miembros['format'];
      $es_transparencia_updated_date = $es_doc_file_meta_miembros['date'];
?>

<div class="es-transparencia-doc-content grid-container">

<div class="callout warning">

  <div class="es-transparencia-doc-single grid-x align-middle">

      <div class="es-transparencia-doc-copy">
        <h4 class="es-transparencia-doc-copy-title h5">Dirección Nacional de Bienes Nacionales</h4>
        <p class="es-transparencia-doc-copy-desc">Edesur Dominicana no cae en la esfera de las atribuciones de la Dirección Nacional de Bienes Nacionales</p>
      </div>

      <div class="es-transparencia-doc-download <?php echo $es_transparencia_doc_file_class; ?>">

          <div class="es-transparencia-file-tools es-cell-item">
            <div class="button-group expanded small">
              <a href="<?php echo urlencode($es_transparencia_doc_file_url); ?>" class="es-transparencia-doc-tools-btn button" data-toggle="<?php echo $es_transparencia_doc_file_id ?>" type="<?php echo $es_transparencia_doc_file_mime_type; ?>"><i class="far fa-eye fa-lg"></i> <span>Vista previa</span></a>

              <a class="es-doc-tools-btn-down es-transparencia-doc-tools-btn button" href="<?php echo $es_transparencia_doc_file_url; ?>" title="Descargar: <?php if (empty($es_transparencia_doc_file_title)) {
                the_title();
              } else {
                echo $es_transparencia_doc_file_title;
              } ?>" target="_blank" download><i class="fas fa-file-download fa-lg"></i> <span>Descargar</span></a>
            </div>
          </div>

          <div class="es-transparencia-doc-file-info es-cell-item">

            <div class="es-transparencia-doc-file-meta es-cell-item">

              <div class="es-transparencia-doc-file-icon es-cell-item">
                <i class="<?php echo $es_transparencia_doc_file_icon; ?>"  title="<?php echo $es_transparencia_doc_file_alt; ?>"></i>
              </div>

              <div class="es-transparencia-doc-file-meta-copy es-cell-item">
                <small class="es-transparencia-doc-file-meta-type"><?php echo $es_transparencia_doc_file_alt; ?></small>
                <small class="es-transparencia-doc-file-meta-size">(<?php echo  $es_transparencia_doc_file_size_format; ?>)</small><br>
              </div>

            </div>

            <div class="es-transparencia-doc-file-date-modified es-cell-item">
              <small ">Publicado: <?php echo $es_transparencia_updated_date; ?></small>
            </div>

          </div>

          </div>

  </div>

</div>

</div>