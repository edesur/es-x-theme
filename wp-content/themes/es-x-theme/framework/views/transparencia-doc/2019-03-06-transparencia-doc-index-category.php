<ul class="accordion" data-accordion data-allow-all-closed="true">

  <pre>index-category.php - Taxonomy: <?php var_dump(is_tax()); ?></pre>
  <pre>index-category.php - Archive: <?php var_dump(is_post_type_archive()); ?></pre>
  <pre>index-category.php - Terms: <?php var_dump($es_transparencia_docs_terms); ?></pre>

    <?php if ( $es_transparencia_docs_terms ): ?>

      <?php if ( $es_transparencia_docs_object->slug =='comision-etica-publica' ) {
        $es_transparencia_docs_terms = array_reverse($es_transparencia_docs_terms);
      } ?>

          <?php foreach ( $es_transparencia_docs_terms as $es_transparencia_docs_term ): ?>

            <?php
              $es_class_accordion = ( $es_transparencia_docs_term === reset($es_transparencia_docs_terms) ) ? 'accordion-item is-active' : 'accordion-item';

              if ($es_transparencia_docs_term->count > 1) {
                $es_transparencia_docs_count_label = (is_tax( $es_transparencia_docs_object->taxonomy, 'datos-abiertos' )) ? $es_transparencia_docs_term->count . ' conjuntos' : $es_transparencia_docs_term->count . ' documentos';
              } elseif ($es_transparencia_docs_term->count == 1) {
                $es_transparencia_docs_count_label = (is_tax( $es_transparencia_docs_object->taxonomy, 'datos-abiertos' )) ? $es_transparencia_docs_term->count . ' conjunto' : $es_transparencia_docs_term->count . ' documento';
              } elseif ($es_transparencia_docs_term->count < 1) {

              $es_transparencia_term_children = get_term_children( $es_transparencia_docs_term->term_id, $es_transparencia_docs_term->taxonomy );

              if ( count( $es_transparencia_term_children ) ) {

                $es_transparencia_child_args = array( 'child_of' => $es_transparencia_docs_term->term_id );

                $es_transparencia_child_terms = get_terms($es_transparencia_docs_term->taxonomy, $es_transparencia_child_args);

                foreach ($es_transparencia_child_terms as $es_transparencia_child_term) {

                  $es_transparencia_term_count  = $es_transparencia_child_term->count;
                }

              } else {
                $es_transparencia_term_count = $es_transparencia_docs_term->count;
              }

              $es_class_accordion = ( $es_transparencia_docs_term === reset($es_transparencia_docs_terms) ) ? 'accordion-item is-active' : 'accordion-item';

              if ($es_transparencia_term_count > 1 || count( get_term_children( $es_transparencia_docs_term->term_id, $es_transparencia_docs_term->taxonomy ) ) == 0 ) {
                $es_transparencia_docs_count_label = (is_tax( $es_transparencia_docs_object->taxonomy, 'datos-abiertos' )) ? $es_transparencia_term_count . ' conjuntos' : $es_transparencia_term_count . ' documentos';
              } elseif ($es_transparencia_term_count == 1 || count( get_term_children( $es_transparencia_docs_term->term_id, $es_transparencia_docs_term->taxonomy ) ) == 0 ) {
                $es_transparencia_docs_count_label = (is_tax( $es_transparencia_docs_object->taxonomy, 'datos-abiertos' )) ? $es_transparencia_term_count . ' conjunto' : $es_transparencia_term_count . ' documento';
              }elseif ($es_transparencia_term_count < 1) {
                $es_transparencia_docs_count_label = (is_tax( $es_transparencia_docs_object->taxonomy, 'datos-abiertos' )) ? ' No existen conjuntos' : ' No existen documentos';
              }

            }

              $es_id_accordion = $es_transparencia_docs_term->slug . '-'. $es_transparencia_docs_term->term_id;
            ?>

            <?php $es_transparencia_docs_term_link = get_term_link($es_transparencia_docs_term, $es_transparencia_docs_term->taxonomy); ?>

             <li class="accordion-item" data-accordion-item>
               <a href="" class="accordion-title">
                 <h4 class="es-transparencia-docs-cat-title h5"></i><?php echo $es_transparencia_docs_term->name; ?><small style="font-size: 0.875rem;">
                 </small> <span class="es-transparencia-docs-count h5" style="font-size: 0.875rem;"><?php echo $es_transparencia_docs_count_label; ?></span></h4>
               </a>

               <div class="accordion-content" data-tab-content>

                  <?php if ( is_tax( $es_transparencia_docs_object->taxonomy, 'base-legal'  ) ||
                  is_tax( $es_transparencia_docs_object->taxonomy, 'marco-legal'  ) ||
                  is_tax( $es_transparencia_docs_object->taxonomy, 'organigrama'  ) ||
                  is_tax( $es_transparencia_docs_object->taxonomy, 'declaraciones-juradas'  ) || (isset($es_transparencia_docs_term_parent) && $es_transparencia_docs_term_parent->slug == 'marco-legal' ) ) : ?>
                    <?php include(locate_template( '/framework/views/transparencia-doc/transparencia-doc-legal.php')); ?>
                  <?php else: ?>
                    <?php include(locate_template( '/framework/views/transparencia-doc/transparencia-doc-index-year.php')); ?>
                  <?php endif ?>

               </div>

             </li>

          <?php endforeach ?>

    <?php else: ?>

      <?php if ( is_tax( $es_transparencia_docs_object->taxonomy, 'organigrama'  ) ||
                is_tax( $es_transparencia_docs_object->taxonomy, 'declaraciones-juradas'  ) ||
                (isset($es_transparencia_docs_term_parent) && ($es_transparencia_docs_term_parent->slug == 'oficina-acceso-informacion' && (
                  is_tax( $es_transparencia_docs_object->taxonomy, 'oai-estructura-organizacional' ) ||
                  is_tax( $es_transparencia_docs_object->taxonomy, 'oai-manual-organizacion' ) ||
                  is_tax( $es_transparencia_docs_object->taxonomy, 'oai-manual-procedimiento' ) ||
                  is_tax( $es_transparencia_docs_object->taxonomy, 'oai-manual-funcionamiento' )
                ) )) ||
      (is_tax( $es_transparencia_docs_object->taxonomy, 'base-legal' ) || (isset($es_transparencia_docs_term_parent) &&
                      $es_transparencia_docs_term_parent->slug == 'base-legal' ) ) ||
       ( is_tax( $es_transparencia_docs_object->taxonomy, 'marco-legal' ) || (isset($es_transparencia_docs_term_parent) &&
                        ($es_transparencia_docs_term_parent->slug == 'marco-legal') ||
                         $es_transparencia_docs_term_parent->slug == 'reglamentos-resoluciones' )  ) ) : ?>
                          <?php include(locate_template( '/framework/views/transparencia-doc/transparencia-doc-legal.php')); ?>
                        <?php else: ?>

                          <?php if ( is_tax( $es_transparencia_docs_object->taxonomy, 'datos-abiertos'  ) || $es_transparencia_docs_term_parent->slug == 'datos-abiertos'): ?>

                            <?php include(locate_template( '/framework/views/transparencia-doc/transparencia-datos-index.php')); ?>

                          <?php else: ?>

                            <?php include(locate_template( '/framework/views/transparencia-doc/transparencia-doc-index-year.php')); ?>

                          <?php endif ?>

                        <?php endif ?>


    <?php endif ?>

  </ul>

</ul>