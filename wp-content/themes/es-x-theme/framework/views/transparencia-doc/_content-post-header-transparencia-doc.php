<?php

// =============================================================================
// VIEWS/INTEGRITY/_CONTENT-POST-HEADER.PHP
// -----------------------------------------------------------------------------
// Standard <header> output for various posts.
// =============================================================================

?>

<header class="entry-header">



  <?php

    if ($es_transparencia_docs_object->parent) {
      $es_transparencia_docs_term_parent  = get_term( $es_transparencia_docs_object->parent, $es_transparencia_docs_tax );
    }

    if ( is_post_type_archive() ) {

       $es_transparencia_docs_title = post_type_archive_title('', false);

     }elseif ( (isset($es_transparencia_docs_term_parent) && $es_transparencia_docs_term_parent->slug == 'base-legal' ) ||
              (isset($es_transparencia_docs_term_parent) && $es_transparencia_docs_term_parent->slug == 'oficina-acceso-informacion' ) ||
              (isset($es_transparencia_docs_term_parent) &&
               ($es_transparencia_docs_term_parent->slug == 'marco-legal' || $es_transparencia_docs_term_parent->slug == 'reglamentos-resoluciones') )  ) {

      $es_transparencia_docs_title = single_tag_title('', false);

     } elseif ( is_tax()) {

      if ($es_transparencia_docs_object->slug == 'datos-abiertos') {
        $es_transparencia_docs_title = single_tag_title('', false);
      }  else {
        $es_transparencia_docs_title = tag_description( '', false );
      }
     }  else {
       $es_transparencia_docs_title = $es_transparencia_docs_object->post_title;
     }
   ?>

   <h2 class="entry-title"><?php echo $es_transparencia_docs_title; ?></h2>

   <?php if ( is_tax( $es_transparencia_docs_object->taxonomy, 'comision-etica-publica' ) ): ?>

    <?php else: ?>
   <?php endif ?>

</header>