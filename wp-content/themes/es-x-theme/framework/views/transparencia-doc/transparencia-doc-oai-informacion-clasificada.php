<div class="es-transparencia-doc-content grid-container">

  <?php $es_transparencia_doc_inventario_file_size = filesize(wp_upload_dir('2018/04')['path'] . '/edesur-transparencia-finanzas-inventario-almacen-carta-bienes-nacionales.pdf'); ?>

<div class="callout warning">
  <div class="es-transparencia-doc-single grid-x align-middle">

      <div class="es-transparencia-doc-copy">
        <h3 class="es-transparencia-doc-copy-title h5">Dirección Nacional de Bienes Nacionales</h3>
        <p style="font-size: 0.875rem">Edesur Dominicana no cae en la esfera de las atribuciones de la Dirección Nacional de Bienes Nacionales</p>
      </div>

      <div class="es-transparencia-doc-download es-transparencia-doc-file">

        <div class="media-object align-middle align-right">

            <div class="es-transparencia-doc-download-file es-transparencia-doc-file media-object-section cell auto">

              <ul class="no-bullet text-center">
                <li><a class="button small expanded" href="<?php echo wp_upload_dir('2018/04')['url']; ?>/edesur-transparencia-finanzas-inventario-almacen-carta-bienes-nacionales.pdf" title="Descargar: Carta Bienes Nacionales">Descargar</a></li>
                <li><small><?php echo size_format($es_transparencia_doc_inventario_file_size, 2); ?></small></li>
              </ul>


            </div>

            <div class="es-transparencia-doc-file media-object-section">
              <i class="far fa-file-pdf fa-3x" title="Adobe Acrobat"></i>
            </div>

        </div>

      </div>

  </div>
</div>

</div>