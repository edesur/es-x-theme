<?php

// =============================================================================
// VIEWS/TRANSPARENCIA DOCS/INDEX-YEAR.PHP
// -----------------------------------------------------------------------------
// Includes the index output.
// =============================================================================

?>

<ul class="accordion" data-accordion data-allow-all-closed="true">

  <?php foreach ($es_transparencia_years as $es_transparencia_year): ?>

    <?php
      if ( is_post_type_archive() ) {
       $es_transparencia_docs_query_args = array(
          'year' => $es_transparencia_year,
         'post_type' => 'transparencia-doc',
         'posts_per_archive_page'  => -1,
       );
      } elseif ( is_tax() ) {

        $es_transparencia_docs_query_date_array = array(
          'taxonomy' => $es_transparencia_docs_object->taxonomy,
          'terms' => $es_transparencia_docs_object->term_id
        );

        if ($es_transparencia_docs_terms) {
          $es_transparencia_docs_query_tax_array = array(
            'field'    => 'id',
            'taxonomy' => $es_transparencia_docs_object->taxonomy,
            'terms' => $es_transparencia_docs_term->term_id
          );
        } else {
          $es_transparencia_docs_query_tax_array = array(
            'taxonomy' => $es_transparencia_docs_object->taxonomy,
            'terms' => $es_transparencia_docs_object->term_id
          );
        }

       $es_transparencia_docs_query_args = array(
         'year' => $es_transparencia_year,
         'post_type' => 'transparencia-doc',
         'posts_per_archive_page'  => -1,
         'tax_query' => array($es_transparencia_docs_query_tax_array)
       );
      }

      $es_transparencia_docs_query_year = new WP_Query( $es_transparencia_docs_query_args );
    ?>

    <?php
      $es_class_accordion = ( $es_transparencia_year === reset($es_transparencia_years) ) ? 'accordion-item is-active' : 'accordion-item';

      /*if ($es_transparencia_docs_query_year->post_count > 1) {
        $es_transparencia_docs_count_label = $es_transparencia_docs_query_year->post_count . ' documentos';
      } elseif ($es_transparencia_docs_query_year->post_count == 1) {
        $es_transparencia_docs_count_label = $es_transparencia_docs_query_year->post_count . ' documento';
      }elseif ($es_transparencia_docs_query_year->post_count < 1) {
        $es_transparencia_docs_count_label = ' No existen documentos';
      }*/

      if ($es_transparencia_docs_query_year->post_count > 1) {
        $es_transparencia_docs_count_label = (is_tax( $es_transparencia_docs_object->taxonomy, 'datos-abiertos' )) ? $es_transparencia_docs_query_year->post_count . ' conjuntos' : $es_transparencia_docs_query_year->post_count . ' documentos';
      } elseif ($es_transparencia_docs_query_year->post_count == 1) {
        $es_transparencia_docs_count_label = (is_tax( $es_transparencia_docs_object->taxonomy, 'datos-abiertos' )) ? $es_transparencia_docs_query_year->post_count . ' conjunto' : $es_transparencia_docs_query_year->post_count . ' documento';
      }elseif ($es_transparencia_docs_query_year->post_count < 1) {
        $es_transparencia_docs_count_label = (is_tax( $es_transparencia_docs_object->taxonomy, 'datos-abiertos' )) ? ' No existen conjuntos' : ' No existen documentos';
      }

    ?>

    <li class="es-site-transparencia-docs-date <?php echo $es_class_accordion; ?>" data-accordion-item>

      <a href="#" class="accordion-title">
        <h4 class="es-transparencia-docs-date-title h5"><i class="fas fa-archive"></i><?php echo $es_transparencia_year; ?> <small style="font-size: 0.875rem;">
          <?php echo $es_transparencia_docs_count_label; ?>
        </small></h4>

      </a>

      <div class="accordion-content" data-tab-content>

        <?php

          if ($es_transparencia_docs_query_year->have_posts() &&
              ($es_transparencia_docs_object->slug == 'oai-estadisticas' ||
              $es_transparencia_docs_object->slug == '311-estadisticas' ||
              $es_transparencia_docs_object->slug == 'memorias-plan-estrategico' ||
              $es_transparencia_docs_object->slug == 'plan-estrategico-institucional' ||
              $es_transparencia_docs_object->slug == 'plan-operativo-anual' ||
              $es_transparencia_docs_object->slug == 'presupuesto-aprobado' ||
              $es_transparencia_docs_object->slug == 'comision-etica-publica' ||
              $es_transparencia_docs_object->slug == 'plan-trabajo' ||
              $es_transparencia_docs_object->slug == 'informes' ||
              $es_transparencia_docs_object->slug == 'plan-anual-compras' /*||
              ($es_transparencia_docs_object->slug == 'informes-auditorias')*/ )
          ) {
            // Months array
            $es_transparencia_months = array();

            foreach ($es_transparencia_docs_query_year->posts as $es_transparencia_post) {

              $es_transparencia_months[] = strtotime($es_transparencia_post->post_date);

            }

          $es_transparencia_months = array_unique($es_transparencia_months);

          // include(locate_template( '/framework/views/transparencia-doc/transparencia-doc-index.php') );
          include(locate_template( '/framework/views/transparencia-doc/transparencia-doc-index-month.php'));

          } else {
            $current_month = date('F');
            $current_month_digit = date('m');
            $current_year = date('Y');

            $display_month = Date('m', strtotime($current_month . " last month"));
            $current_month_format = Date('m', strtotime($current_month ));

            // $es_loop_counter = ($es_transparencia_year == $current_year ) ?  $display_month :  12;
            $es_loop_counter = ($es_transparencia_year == $current_year ) ?  $current_month_format :  12;

            $es_transparencia_months = array();

            for ( $m=1; $m<=$es_loop_counter; ++$m ) {
              $es_transparencia_months[] = mktime(0, 0, 0, $m, 1);
            }

            $es_transparencia_months = array_reverse($es_transparencia_months);

            include(locate_template( '/framework/views/transparencia-doc/transparencia-doc-index-month.php'));
          }

        ?>

      </div>
    </li>

    <?php wp_reset_postdata(); // Reset Post Data ?>

  <?php endforeach ?>

</ul>