<div class="es-transparencia-doc-content grid-container">

  <?php $es_transparencia_doc_inventario_file_size = filesize(wp_upload_dir('2018/04')['path'] . '/edesur-transparencia-finanzas-inventario-almacen-carta-bienes-nacionales.pdf'); ?>

<div class="callout warning">
  <div class="es-transparencia-doc-single grid-x align-middle">

      <div class="es-cell-item text-center">
        <h3 class="es-transparencia-doc-copy-title h5">Edesur Dominicana en Datos.gob.do</h3>
        <p class="mbs"><small><b>Datos.gob.do</b> es el portal oficial de datos abiertos del gobierno de la República Dominicana y el repositorio unificado de publicaciones institucionales. Su objetivo principal es que los datos sean más fáciles de encontrar y reutilizar.</small></p>
        <p class="text-center"><a href="http://datos.gob.do/organization/empresa-distribuidora-de-electricidad-del-sur" class="button" target="_blank" title="Publicaciones de Edesur Dominicana en Datos.gop.do">Publicaciones de Edesur Dominicana en Datos.gob.do</a></p>
      </div>

  </div>
</div>

</div>