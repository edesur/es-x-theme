<?php
  $es_url_uploads = wp_upload_dir()['baseurl'];
  $es_dir_uploads = wp_upload_dir()['basedir'];
  ?>

<div id="es_rehabilitacion_redes_masthead" class="es-rehabilitacion-redes-section-masthead">
  <div class="es-rehabilitacion-redes-cell-title-about">
    <h1><?php the_title() ?></h1>
  </div>
  <div class="es-rehabilitacion-redes-cell-content-about">

      <div class="es-rehabilitacion-redes-about-desc">
        <h2 class="subheader h3">Edesur Dominicana está ejecutando diversos proyectos de Rehabilitación de Redes en toda su área de concesión, estos proyectos buscan mejorar la calidad de vida de los clientes y se enmarcan dentro del Plan Operativo que Edesur se ha trazado para la mejora del servicio eléctrico.</h2>
      </div>

      <div class="es-rehabilitacion-redes-about-goal">

        <h4 class="subheader">Para el Gran Santo Domingo y San Cristóbal, se ejecutan actualmente 11 proyectos de Rehabilitación de Redes y 4 de Telemedición, con fondos propios y de organismos multilaterales.</h4>
      </div>

      <div class="es-rehabilitacion-redes-about-goal">

        <h4 class="subheader">Estos 11 circuitos a rehabilitar y los 4 proyectos de telemedición mejorarán la calidad de vida de más de 570,000 dominicanos.</h4>
      </div>

  </div>
</div>

<div id="es_rehabilitacion_redes_sectores" class="es-rehabilitacion-redes-section-sectores">
  <div class="es-rehabilitacion-redes-cell-title-sectores">
    <h1 class="h2">Sectores y Comunidades en Rehabilitación</h1>
    <p>Los proyectos de Rehabilitación de Redes llevarán energía 24 horas y calidad de vida a las comunidades impactadas.</p>
  </div>
  <div class="es-rehabilitacion-redes-cell-content-sectores">
       <div class="responsive-embed widescreen">
       <iframe width="1680" height="945" src="https://www.youtube.com/embed/96DlGAnbctY?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
    </div>
  </div>
</div>

<div id="es_rehabilitacion_redes_proyectos" class="es-rehabilitacion-redes-section-proyectos">
  <div class="es-rehabilitacion-redes-cell-title-proyectos">
    <h1 class="h2">Proyectos de Rehabilitación de Redes</h1>
    <p>Los proyectos contemplan la rehabilitación de más de 300 km de redes y más de 8,500 nuevas luminarias.</p>
  </div>
  <div class="es-rehabilitacion-redes-cell-content-proyectos">
    <div class="responsive-embed widescreen">
       <iframe width="1680" height="945" src="https://www.youtube.com/embed/cZyQmoZMrsY?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
    </div>
  </div>
</div>

<div id="es_rehabilitacion_redes_avances" class="es-rehabilitacion-redes-section-avances">
  <div class="es-rehabilitacion-redes-cell-title-avances">
    <h1 class="h2">Avance de los proyectos</h1>
  </div>
  <div class="es-rehabilitacion-redes-cell-content-avances">

    <p>Entérate de todo lo relacionado con los Proyectos de Rehabilitación de Redes en tu comunidad, visitando la sección de <a href="http://www.edesur.com.do/transparencia/documentos/proyectos/" target="_blank" title="Programas y Proyectos | Transparencia | Edesur Dominicana">Programas y Proyectos</a> en nuestro sub-portal de <a href="http://www.edesur.com.do/transparencia/" target="_blank" title="Transparencia | Edesur Dominicana">Transparencia</a></p>

    <div class="es-rehabilitacion-redes-button-group-avances">
      <a href="<?php echo get_term_link('descripcion', 'categoria-transparencia'); ?>" title="Proyectos y Programas – Descripción" target="_blank" class="es-rehabilitacion-redes-button-avances button">Descripción</a>
      <a href="<?php echo get_term_link('calendario-ejecucion', 'categoria-transparencia'); ?>" title="Proyectos y Programas – Calendario de Ejecución" target="_blank" class="es-rehabilitacion-redes-button-avances button">Calendario</a>
      <a href="<?php echo get_term_link('informes-presupuesto', 'categoria-transparencia'); ?>" title="Proyectos y Programas – Informes de Presupuesto" target="_blank" class="es-rehabilitacion-redes-button-avances button">Presupuesto</a>
      <a href="<?php echo get_term_link('informes-seguimiento', 'categoria-transparencia'); ?>" title="Proyectos y Programas – Informes de Seguimiento" target="_blank" class="es-rehabilitacion-redes-button-avances button">Seguimiento</a>
    </div>

  </div>
</div>

<div  id="es_rehabilitacion_redes_components" class="es-rehabilitacion-redes-section-components">

  <div class="es-rehabilitacion-redes-cell-title-components">
    <h1 class="h2">Componentes de los proyectos</h1>
  </div>

  <div class="es-rehabilitacion-redes-cell-content-components">

    <p>La ejecución los proyectos de rehabilitación implican la intervención de 4 componentes</p>

  </div>

  <div class="es-rehabilitacion-redes-content-component">
    <div class="es-rehabilitacion-redes-icon-components">
        <img src= "<?php echo $es_url_uploads; ?>/2018/05/es-rehabilitacion-redes-icon-gestion-social.png" alt="Gestión Social | Rehabilitación de Redes Eléctricas">
    </div>
    <div class="es-rehabilitacion-redes-copy-components">
      <h4>Gestión Social</h4>
      <p>La primera etapa de cada proyecto inicia con el componente de Gestión Social, identificando los líderes de cada comunidad y dándoles a conocer todas las actividades que desarrollaremos en sus sectores.</p>
    </div>
  </div>

  <div class="es-rehabilitacion-redes-content-component">
    <div class="es-rehabilitacion-redes-icon-components">
      <img src= "<?php echo $es_url_uploads; ?>/2018/05/es-rehabilitacion-redes-icon-gestion-ambiental.png" alt="Gestión Ambiental | Rehabilitación de Redes Eléctricas">
    </div>
    <div class="es-rehabilitacion-redes-copy-components">
      <h4>Gestión Ambiental</h4>
      <p>Luego, un equipo de Medio Ambiente y Seguridad, asegura que la intervención sea amigable con el medioambiente y se garantice la integridad de las personas y bienes, además de identificar las necesidades de poda en cada zona intervenida, todo esto, haciendo cumplir las normas nacionales e internacionales requeridas.</p>
    </div>
  </div>

  <div class="es-rehabilitacion-redes-content-component">
    <div class="es-rehabilitacion-redes-icon-components">
      <img src= "<?php echo $es_url_uploads; ?>/2018/05/es-rehabilitacion-redes-icon-ingenieria-obras.png" alt="Ingeniería y Obras | Rehabilitación de Redes Eléctricas">
    </div>
    <div class="es-rehabilitacion-redes-copy-components">
      <h4>Ingeniería y Obras</h4>
      <p>Para iniciar los trabajos, nuestro equipo de Ingeniería y Obras, valida la calidad de los materiales a utilizar y nuestros técnicos comienzan con los trabajos de excavación, izaje de postes, armado de estructura, instalación de tendido eléctrico, transformadores y luminarias.</p>
    </div>
  </div>

  <div class="es-rehabilitacion-redes-content-component">
    <div class="es-rehabilitacion-redes-icon-components">
      <img src= "<?php echo $es_url_uploads; ?>/2018/05/es-rehabilitacion-redes-icon-gestion-comercial.png" alt="Gestión Comercial | Rehabilitación de Redes Eléctricas">
    </div>
    <div class="es-rehabilitacion-redes-copy-components">
      <h4>Gestión Comercial</h4>
      <p>En la última etapa entra el Componente Comercial, con la instalación de las acometidas y los medidores así como la integración de los nuevos clientes a los procesos de lectura, facturación y cobros.</p>
    </div>
  </div>

</div>

<div id="es_rehabilitacion_redes_documentos" class="es-rehabilitacion-redes-section-documentos">
  <div class="es-rehabilitacion-redes-cell-title-documentos">
    <h1 class="h2">Análisis y Estudios</h1>
    <p>Análisis y Estudios ambientales y sociales del Programa de Expansión de Redes y Reducción de Pérdidas Eléctricas</p>
  </div>
  <div class="es-rehabilitacion-redes-cell-content-documentos es-site-transparencia-docs">
       <ul class="accordion" data-accordion>
             <li class="accordion-item is-active" data-accordion-item>
               <!-- Accordion tab title -->
               <a href="#" class="accordion-title"><h5>Análisis</h5></a>

               <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
               <div class="accordion-content" data-tab-content>

                 <div class="es-transparencia-doc-content grid-container">
                   <div class="es-transparencia-doc-single grid-x align-middle">
                     <div class="es-transparencia-doc-copy">
                       <h5 class="es-transparencia-doc-copy-title">Análisis Ambiental y Social - junio, 2018</h5>
                       <p>Análisis Ambiental y Social Programa Expansión de Redes y Reducción de Pérdidas Eléctrica en Distribución - junio, 2018</p>
                     </div>
                     <div class="es-transparencia-doc-download">
                       <div class="media-object align-middle align-right">
                         <div class="es-transparencia-doc-download-file media-object-section cell auto">
                           <ul class="no-bullet text-center">
                             <li><a href="<?php echo $es_url_uploads; ?>/2018/07/es-rehabilitacion-redes-analisis-ambiental-social-2018-06.pdf" class="button small expanded" title="Análisis Ambiental y Social - junio, 2018">Descargar</a></li>
                             <li><small>(<?php echo size_format( filesize ( $es_dir_uploads . '/2018/07/es-rehabilitacion-redes-analisis-ambiental-social-2018-06.pdf' ), 2 ); ?></small>)</li>
                           </ul>
                         </div>

                         <div class="es-transparencia-doc-file media-object-section">
                           <i class="far fa-file-pdf fa-3x" title="Análisis Ambiental y Social - junio, 2018"></i>
                         </div>
                       </div>
                     </div>
                   </div>
                 </div>

               </div>
             </li>

             <li class="accordion-item" data-accordion-item>
               <!-- Accordion tab title -->
               <a href="#" class="accordion-title"><h5>Estudios</h5></a>

               <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
               <div class="accordion-content" data-tab-content>

                 <div class="es-transparencia-doc-content grid-container">
                   <div class="es-transparencia-doc-single grid-x align-middle">
                     <div class="es-transparencia-doc-copy">
                       <h5 class="es-transparencia-doc-copy-title">Estudio Ambiental y Social Subestación Engombe-Manoguayabo - junio 2018</h5>
                       <p>Estudio Ambiental y Social para construcción Subestación eléctrica en Engombe-Manoguayabo – junio 2018</p>
                     </div>
                     <div class="es-transparencia-doc-download">
                       <div class="media-object align-middle align-right">
                         <div class="es-transparencia-doc-download-file media-object-section cell auto">
                           <ul class="no-bullet text-center">
                             <li><a href="<?php echo $es_url_uploads; ?>/2018/07/es-rehabilitacion-redes-estudio-ambiental-social-subestacion-engombe-manoguayabo-2018-06.pdf" class="button small expanded" title="Estudio Ambiental y Social Subestación Engombe-Manoguayabo - junio 2018">Descargar</a></li>
                             <li><small>(<?php echo size_format( filesize ( $es_dir_uploads . '/2018/07/es-rehabilitacion-redes-estudio-ambiental-social-subestacion-engombe-manoguayabo-2018-06.pdf' ), 2 ); ?></small>)</li>
                           </ul>
                         </div>

                         <div class="es-transparencia-doc-file media-object-section">
                           <i class="far fa-file-pdf fa-3x" title="Estudio Ambiental y Social Subestación Los Alcarrizos - junio 2018"></i>
                         </div>
                       </div>
                     </div>
                   </div>

                   <div class="es-transparencia-doc-single grid-x align-middle">
                     <div class="es-transparencia-doc-copy">
                       <h5 class="es-transparencia-doc-copy-title">Estudio Ambiental y Social Subestación Los Alcarrizos - junio 2018</h5>
                       <p>Estudio Ambiental y Social para construcción Subestación eléctrica en Los Alcarrizos – junio 2018</p>
                     </div>
                     <div class="es-transparencia-doc-download">
                       <div class="media-object align-middle align-right">
                         <div class="es-transparencia-doc-download-file media-object-section cell auto">
                           <ul class="no-bullet text-center">
                             <li><a href="<?php echo $es_url_uploads; ?>/2018/07/es-rehabilitacion-redes-estudio-ambiental-social-subestacion-los-alcarrizos-2018-06.pdf" class="button small expanded" title="Estudio Ambiental y Social Subestación Los Alcarrizos - junio 2018">Descargar</a></li>
                             <li><small>(<?php echo size_format( filesize ( $es_dir_uploads . '/2018/07/es-rehabilitacion-redes-estudio-ambiental-social-subestacion-los-alcarrizos-2018-06.pdf' ), 2 ); ?></small>)</li>
                           </ul>
                         </div>

                         <div class="es-transparencia-doc-file media-object-section">
                           <i class="far fa-file-pdf fa-3x" title="Estudio Ambiental y Social Subestación Los Alcarrizos - junio 2018"></i>
                         </div>
                       </div>
                     </div>
                   </div>

                   <div class="es-transparencia-doc-single grid-x align-middle">
                     <div class="es-transparencia-doc-copy">
                       <h5 class="es-transparencia-doc-copy-title">Estudio Ambiental y Social Subestación Piedra Blanca- junio 2018</h5>
                       <p>Estudio Ambiental y Social para construcción Subestación eléctrica en Piedra Blanca– junio 2018</p>
                     </div>
                     <div class="es-transparencia-doc-download">
                       <div class="media-object align-middle align-right">
                         <div class="es-transparencia-doc-download-file media-object-section cell auto">
                           <ul class="no-bullet text-center">
                             <li><a href="<?php echo $es_url_uploads; ?>/2018/07/es-rehabilitacion-redes-estudio-ambiental-social-subestacion-piedra-blanca-2018-06.pdf" class="button small expanded" title="Estudio Ambiental y Social Subestación Piedra Blanca- junio 2018">Descargar</a></li>
                             <li><small>(<?php echo size_format( filesize ( $es_dir_uploads . '/2018/07/es-rehabilitacion-redes-estudio-ambiental-social-subestacion-piedra-blanca-2018-06.pdf' ), 2 ); ?></small>)</li>
                           </ul>
                         </div>

                         <div class="es-transparencia-doc-file media-object-section">
                           <i class="far fa-file-pdf fa-3x" title="Estudio Ambiental y Social Subestación Piedra Blanca- junio 2018"></i>
                         </div>
                       </div>
                     </div>
                   </div>
                 </div>

               </div>
             </li>
           </ul>
  </div>
</div>

<div id="es_rehabilitacion_redes_prensa" class="es-rehabilitacion-redes-section-prensa">
  <div class="es-rehabilitacion-redes-cell-title-prensa">
    <h1 class="h2">Prensa</h1>
  </div>
  <div class="es-rehabilitacion-redes-cell-content-prensa">

    <?php echo do_shortcode( '[the_grid name="es-the-grid-rehabilitacion-redes"]' ); ?>

    <div class="es-rehabilitacion-redes-archive-prensa">
      <a href="<?php echo get_term_link('rehabilitacion-redes', 'category'); ?>" title="Rehabilitación de Redes Eléctricas" class="es-rehabilitacion-redes-button-avances button">Todas las publicaciones</a>
    </div>

  </div>

  <div class="es-rehabilitacion-redes-addthis">
    <?php echo do_shortcode( '[addthis tool="addthis_inline_share_toolbox_hfga"]' ); ?>
  </div>

</div>



