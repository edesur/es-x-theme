<?php

      if ($es_empresa_docs_terms) {
        $es_empresa_docs_query_legal_tax_array = array(
          'field'    => 'id',
          'taxonomy' => $es_empresa_docs_term->taxonomy,
          'terms' => $es_empresa_docs_term->term_id
        );
      } else {
        $es_empresa_docs_query_legal_tax_array = array(
          'taxonomy' => $es_empresa_docs_term->taxonomy,
          'terms' => $es_empresa_docs_term->term_id
        );
      }


     $es_empresa_docs_query_legal_args = array(
       'post_type' => 'transparencia-doc',
       'posts_per_archive_page'  => -1,
       'tax_query' => array($es_empresa_docs_query_legal_tax_array)
     );

    $es_empresa_docs_query_legal = new WP_Query( $es_empresa_docs_query_legal_args );
  ?>

  <?php if ( $es_empresa_docs_query_legal->have_posts() ): ?>

       <?php while ( $es_empresa_docs_query_legal->have_posts() ) : $es_empresa_docs_query_legal->the_post(); ?>

           <?php
             $es_empresa_doc_type_field = get_field('es_transparencia_documento_type');
             $es_empresa_doc_type = $es_empresa_doc_type_field->slug;
             $es_empresa_doc_type_name = $es_empresa_doc_type_field->name;
             $es_empresa_doc_type_desc = $es_empresa_doc_type_field->description;

               // File
               $es_empresa_doc_file = get_field('es_transparencia_documento_file');
               $es_empresa_doc_file_url = $es_empresa_doc_file['url'];
               $es_empresa_doc_file_title = $es_empresa_doc_file['title'];
               $es_empresa_doc_file_dir = get_attached_file($es_empresa_doc_file['id']);
               $es_empresa_doc_file_size = filesize($es_empresa_doc_file_dir);
               $es_empresa_doc_file_size_format = size_format($es_empresa_doc_file_size, 2);
               $es_empresa_doc_file_mime_type = $es_empresa_doc_file['mime_type'];
               $es_transparencia_doc_file_id = $es_empresa_doc_type . '-' . $es_empresa_doc_file['id'];

              // Laws
             $es_empresa_doc_law_code = get_field('es_transparencia_documento_law_code');
             $es_empresa_doc_legal_desc = get_field('es_transparencia_documento_legal_description');

             // Frequency
             $es_empresa_doc_frecuency_field = get_field('es_transparencia_documento_frecuency');
             $es_empresa_doc_frecuency = $es_empresa_doc_frecuency_field['value'];


             // Date

             $es_empresa_doc_year_constitution = get_field('es_transparencia_documento_year_constitution');

             //  Published and modified date

             $es_empresa_time = get_the_time('U');
             $es_empresa_modified_time = get_the_modified_time('U');
             if ($es_empresa_modified_time >= $es_empresa_time + 86400) {
             $es_empresa_updated_date = get_the_modified_time('j \d\e F, Y');
             $es_empresa_updated_time = get_the_modified_time('h:i a');
             } else {
               $es_empresa_updated_date = get_the_date();
             }

             switch ($es_empresa_doc_file_mime_type) {
               case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                 $es_transparencia_doc_file_class = 'es-transparencia-doc-file';
                 $es_transparencia_doc_file_alt = 'MicroSoft Word';
                 $es_empresa_doc_file_icon = 'far fa-file-word';
                 break;
               case 'application/msword':
                 $es_transparencia_doc_file_class = 'es-transparencia-doc-file';
                 $es_transparencia_doc_file_alt = 'MicroSoft Word';
                 $es_empresa_doc_file_icon = 'far fa-file-word';
                 break;
               case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
                 $es_transparencia_doc_file_class = 'es-transparencia-doc-file';
                 $es_transparencia_doc_file_alt = 'MicroSoft Excel';
                 $es_empresa_doc_file_icon = 'far fa-file-excel';
                 break;
               case 'application/vnd.ms-excel':
                 $es_transparencia_doc_file_class = 'es-transparencia-doc-file';
                 $es_transparencia_doc_file_alt = 'MicroSoft Excel';
                 $es_empresa_doc_file_icon = 'far fa-file-excel';
                 break;
               case 'application/vnd.ms-excel.sheet.macroEnabled.12':
                 $es_transparencia_doc_file_class = 'es-transparencia-doc-file';
                 $es_transparencia_doc_file_alt = 'MicroSoft Excel';
                 $es_empresa_doc_file_icon = 'far fa-file-excel';
                 break;
               case 'application/pdf':
                 $es_transparencia_doc_file_class = 'es-transparencia-doc-file';
                 $es_transparencia_doc_file_alt = 'Adobe PDF';
                 $es_empresa_doc_file_icon = 'far fa-file-pdf';
                 break;

               default:
                 $es_transparencia_doc_file_class = 'es-transparencia-doc-file-none';
                 $es_empresa_doc_file_icon = 'far fa-file fa-2x';
                 break;
             }

           switch ($es_empresa_doc_type) {

             case 'constitucion-dominicana':
               $es_empresa_doc_desc = 'Constitución de la República Dominicana ' . $es_empresa_doc_year_constitution;
               break;

              case 'leyes-base-legal':
               $es_empresa_doc_desc = $es_empresa_doc_legal_desc;
               break;

               case 'leyes-marco-legal':
               $es_empresa_doc_desc = $es_empresa_doc_legal_desc;
               break;

               case 'resoluciones-base-legal':
                $es_empresa_doc_desc = $es_empresa_doc_legal_desc;
                break;

                case 'resoluciones-marco-legal':
                $es_empresa_doc_desc = $es_empresa_doc_legal_desc;
                break;

             default:
               $es_empresa_doc_desc = $es_empresa_doc_legal_desc;
               break;
           }

           ?>


          <?php /*if (($es_empresa_doc_type == 'organigrama' && is_page('organigrama') && $post_parent->post_name == 'empresa') ||
          ($es_empresa_doc_type_parent_slug == 'base-legal' &&
                    (is_page('marco-legal') && $post_parent->post_name == 'empresa')) ):*/ ?>

            <div class="es-transparencia-doc-single grid-x align-middle">

                <div class="es-transparencia-doc-copy">
                  <h4 class="es-transparencia-doc-copy-title h5"><?php the_title(); ?></h4>
                  <p class="es-transparencia-doc-copy-desc"><?php echo $es_empresa_doc_desc ; ?></p>
                </div>

                <div class="es-transparencia-doc-download <?php echo $es_transparencia_doc_file_class; ?>">

                    <div class="es-transparencia-file-tools es-cell-item">
                      <div class="button-group expanded small">
                        <a href="<?php echo urlencode($es_empresa_doc_file_url); ?>" class="es-transparencia-doc-tools-btn button" data-toggle="<?php echo $es_transparencia_doc_file_id; ?>" type="<?php echo $es_empresa_doc_file_mime_type; ?>"><i class="far fa-eye fa-lg"></i> <span>Vista previa</span></a>

                        <a class="es-doc-tools-btn-down es-transparencia-doc-tools-btn button" href="<?php echo $es_empresa_doc_file_url; ?>" title="Descargar: <?php if (empty($es_empresa_doc_file_title)) {
                          the_title();
                        } else {
                          echo $es_empresa_doc_file_title;
                        } ?>" target="_blank" download><i class="fas fa-file-download fa-lg"></i> <span>Descargar</span></a>
                      </div>
                    </div>

                    <div class="es-transparencia-doc-file-info es-cell-item">

                      <div class="es-transparencia-doc-file-meta es-cell-item">

                        <div class="es-transparencia-doc-file-icon es-cell-item">
                          <i class="<?php echo $es_empresa_doc_file_icon; ?>"  title="<?php echo $es_transparencia_doc_file_alt; ?>"></i>
                        </div>

                          <div class="es-transparencia-doc-file-meta-copy es-cell-item">
                            <small class="es-transparencia-doc-file-meta-type"><?php echo $es_transparencia_doc_file_alt; ?></small>
                            <small class="es-transparencia-doc-file-meta-size">(<?php echo  $es_empresa_doc_file_size_format; ?>)</small><br>
                        </div>
                      </div>

                      <div class="es-transparencia-doc-file-date-modified es-cell-item">
                        <small ">Publicado: <?php echo $es_empresa_updated_date; ?></small>
                      </div>

                    </div>

                </div>

            </div>
          <?php /*endif*/ ?>

     <?php endwhile; ?>

  <?php endif ?>

     <?php wp_reset_postdata(); // Reset Post Data ?>