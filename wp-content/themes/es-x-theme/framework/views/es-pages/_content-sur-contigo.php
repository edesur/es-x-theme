<?php $es_dir_uploads = wp_upload_dir()['baseurl']; ?>
<?php $es_dir_theme = get_stylesheet_directory_uri(); ?>

<div class="es-sur-contigo-section-masthead">
  <div class="es-sur-contigo-cell-title-masthead">
    <img src= "<?php echo $es_dir_theme; ?>/framework/foundation/css/img/es-site-page-sur-contigo-logo.png" alt="Sur Contigo">
    <h1 class="es-sur-contigo-title-masthead"><?php the_title(); ?></h1>
  </div>
</div>

<div id="es_sur_contigo" class="es-sur-contigo-section-about">
  <div class="es-sur-contigo-cell-title-about">
    <h1 class="es-sur-contigo-title-about"><span>¿</span>Qué es Sur Contigo<span>?</span></h1>
  </div>
  <div class="es-sur-contigo-cell-content-about">
    <p>Es nuestro proyecto de responsabilidad social corporativa donde ponemos en manifiesto el compromiso con el bienestar de nuestra gente y las generaciones futuras, somos parte integral de nuestra área de concesión y nos identificamos con cada pueblo y su cultura. ¡Somos Sur!</p>
  </div>
</div>

<div id="es_sur_contigo_ejes" class="es-sur-contigo-section-concept">
  <div class="es-sur-contigo-cell-title-concept">
    <h1 class="es-sur-contigo-title-concept">Ejes del Proyecto</h1>
  </div>
  <div class="es-sur-contigo-cell-content-concept">
    <div class="es-sur-contigo-desc-concept">
      <p>Este proyecto tiene tres ejes fundamentales:</p>
      <ul class="no-bullet">
        <li>Sur Siembra (Reforestación)</li>
        <li>Sur Limpia (Limpieza de playas)</li>
        <li>Sur Aprende (Realización de actividades educativas)</li>
      </ul>
    </div>
  </div>

  <div class="es-sur-contigo-accordion-tabs-concept">
    <ul id="es_sur_contigo_accordion_concept" class="accordion" data-responsive-accordion-tabs="accordion medium-tabs large-tabs">

      <li class="es-sur-contigo-accordion-item-siembra accordion-item is-active" data-accordion-item>
        <a href="#" class="es-sur-contigo-accordion-title accordion-title"><img src= "<?php echo $es_dir_theme; ?>/framework/foundation/css/img/es-site-page-sur-contigo-logo.png" alt="Sur Contigo">
          <h4 class="es-sur-contigo-title">Siembra</h4></a>
        <div id="es_sur_contigo_siembra" class="accordion-content" data-tab-content>
          <div class="es-sur-contigo-accordion-content-siembra">
            <div class="es-sur-contigo-content-desc-siembra">
              <p>Reforestamos zonas impactadas de nuestra área de concesión, que se encuentran en estado delicado para establecer una vegetación más vasta y así contribuir con el medio ambiente favoreciendo directamente a los habitantes de la zona.</p>
            </div>
            <div class="es-sur-contigo-content-img-siembra">
              <img src= "<?php echo $es_dir_uploads; ?>/2016/12/cdeee-edesur-reforestan-instalan-bombillas-cercado-vallejuelo.jpg" alt="Gestión Social | Rehabilitación de Redes Eléctricas">
            </div>
          </div>
        </div>
      </li>

      <li class="es-sur-contigo-accordion-item-limpia accordion-item" data-accordion-item>
        <a href="#" class="es-sur-contigo-accordion-title accordion-title">
          <img src= "<?php echo $es_dir_theme; ?>/framework/foundation/css/img/es-site-page-sur-contigo-logo.png" alt="Sur Contigo">
          <h4 class="es-sur-contigo-title">Limpia</h4>
        </a>
        <div id="es_sur_contigo_limpia" class="accordion-content" data-tab-content>
          <div class="es-sur-contigo-accordion-content-limpia">
            <div class="es-sur-contigo-content-desc-limpia">
              <p>Realizamos jornadas de limpieza de playas para proteger la vida marina apoyando los negocios del entorno como también espacios importantes para las comunidades cercanas.</p>
            </div>
            <div class="es-sur-contigo-content-img-limpia">
              <img src= "<?php echo $es_dir_uploads; ?>/2017/12/Edesur-Dominicana-Sur-Contigo-PT.jpg" alt="Gestión Social | Rehabilitación de Redes Eléctricas">
            </div>
          </div>
        </div>
      </li>

      <li class="es-sur-contigo-accordion-item-aprende accordion-item" data-accordion-item>
        <a href="#" class="es-sur-contigo-accordion-title accordion-title">
          <img src= "<?php echo $es_dir_theme; ?>/framework/foundation/css/img/es-site-page-sur-contigo-logo.png" alt="Sur Contigo">
          <h4 class="es-sur-contigo-title">Aprende</h4>
        </a>
        <div id="es_sur_contigo_aprende" class="accordion-content" data-tab-content>
          <div class="es-sur-contigo-accordion-content-limpia">
            <div class="es-sur-contigo-content-desc-limpia">
              <p>Realizamos actividades educativas, dinámicas y recreativas preparando a nuestros futuros clientes para una mejor relación con la energía, de la mano de Gestión Social abarcamos temas de uso eficiente de energía, reciclaje, cuidado del medio ambiente y seguridad.</p>
            </div>
            <div class="es-sur-contigo-content-img-limpia">
              <img src= "<?php echo $es_dir_uploads; ?>/2018/04/portada_1920-1080.jpg" alt="EDESUR presente en la Feria Internacional del Libro 2018">
            </div>
          </div>
        </div>
      </li>

    </ul>
  </div>

</div>

<div id="es_sur_contigo_comunidad" class="es-sur-contigo-section-comunidad">
  <div class="es-sur-contigo-cell-title-comunidad">
    <h1 class="es-sur-contigo-title-comunidad">Comunidad Sur Contigo</h1>
  </div>
  <div class="es-sur-contigo-cell-content-comunidad">
    <p>Partiendo de la iniciativa de colaboradores siempre dispuestos a aportar al bienestar del país y ser voluntarios del crecimiento de su pueblo, nace la Comunidad Sur Contigo, los miembros de esta comunidad se caracterizan por ser entusiastas, colaboradores y socialmente responsables.</p>
    <p>Los integrantes de la comunidad se capacitan mediante un entrenamiento previo a las actividades y son portavoz del proyecto.</p>
  </div>
</div>

<div id="es_sur_contigo_actividades" class="es-sur-contigo-section-actividades">
  <div class="es-sur-contigo-cell-title-actividades">
    <h1 class="es-sur-contigo-title-actividades">Actividades</h1>
  </div>
  <div class="es-sur-contigo-cell-content-actividades">
    <?php echo do_shortcode( '[the_grid name="es-the-grid-responsabilidad-social"]', true ); ?>

    <div class="es-sur-contigo-archive-actividades">
      <a href="<?php echo get_term_link('responsabilidad-social', 'category'); ?>" title="Actividades Sur Contigo" class="es-sur-contigo-button-actividades button">Todas las actividades</a>
    </div>
  </div>

  <div class="es-sur-contigo-addthis">
    <?php echo do_shortcode( '[addthis tool="addthis_inline_share_toolbox_hfga"]' ); ?>
  </div>
</div>