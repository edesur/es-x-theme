<li class="accordion-item" data-accordion-item>
  <a href="" class="accordion-title">
    <h4 class="es-transparencia-docs-cat-title h5"></i><?php echo $es_empresa_docs_term->name; ?><small style="font-size: 0.875rem;">
    </small> <span class="es-transparencia-docs-count h5" style="font-size: 0.875rem;"><?php echo $es_empresa_docs_count_label; ?></span></h4>
  </a>

  <div class="accordion-content" data-tab-content>

      <?php include(locate_template( '/framework/views/es-pages/_content-empresa-doc-legal.php')); ?>

  </div>

</li>
