<?php

// =============================================================================
// VIEWS/TRANSPARENCIA DOCS/INDEX-MONTH.PHP
// -----------------------------------------------------------------------------
// Includes the index output.
// =============================================================================

?>

<?php
  // $es_empresa_doc_month = date( 'm', $es_empresa_month );
  // $es_empresa_doc_month_i18n = date_i18n( 'F', $es_empresa_month );

  $es_empresa_docs_query_args = array(
    'post_type' => 'transparencia-doc',
    'posts_per_archive_page'  => -1,
    'taxonomy' => 'categoria-transparencia',
    'hide_empty' => true
  );

  $es_empresa_docs_query = new WP_Query( $es_empresa_docs_query_args );
?>

  <?php if ( $es_empresa_docs_query->have_posts() ): ?>

    <div class="es-site-transparencia-docs entry-wrap">

      <?php if ( is_singular() ) : ?>
        <?php if ( $disable_page_title != 'on' ) : ?>
        <header class="entry-header">
          <h1 class="entry-title"><?php the_title(); ?></h1>
        </header>
        <?php endif; ?>
      <?php else : ?>
      <header class="entry-header">
        <h2 class="entry-title">
          <a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to: "%s"', '__x__' ), the_title_attribute( 'echo=0' ) ) ); ?>"><?php x_the_alternate_title(); ?></a>
        </h2>
      </header>
      <?php endif; ?>

      <div class="es-transparencia-doc-content grid-container">

        <?php if (is_page('marco-legal') && $post_parent->post_name == 'empresa' ): ?>
          <p>En EDESUR estamos sujetos a la aplicación del marco regulativo vigente en la República Dominicana para el sector eléctrico el cual publicamos en esta sección para que sea de utilidad en sus consultas.</p>
        <?php elseif(is_page('organigrama') && $post_parent->post_name == 'empresa'): ?>
          <h3 class="mbs">Estructura organizativa de Edesur Dominicana</h3 >
        <?php endif ?>

        <small>Estas extensiones son recomendadas para visualizar los documentos en el explorador:</small>

            <div class="es-transparencia-download-acrobat media-object align-middle" style="margin-bottom: 0.125rem">
              <div class="es-transparencia-doc-file-pdf media-object-section">
                <i class="far fa-file-pdf"></i>
              </div>
              <div class="media-object-section">
                <small><a href="https://get.adobe.com/reader/?loc=es" title="Descargar Adobe Acrobat Reader DC" target="_blank" style="color: hsl(0, 100%, 50%);">Abobe Reader DC</a> para documentos PDF</small>
              </div>
            </div>

            <div class="es-transparencia-download-acrobat media-object align-middle">
              <div class="es-transparencia-doc-file-pdf media-object-section">
                <i class="fab fa-windows" style="color: #d83b01;"></i>
              </div>
              <div class="media-object-section">
                <small><a href="https://support.office.com/es-es/article/acceso-rápido-a-los-archivos-de-office-en-el-explorador-dc1024b4-92be-46eb-81a7-aea85368baa0" target="_blank" title="Acceso rápido a los archivos de Office en el explorador
                " style="color: #d83b01;">Office Online</a> para documentos Word y Excel  para <a href="https://chrome.google.com/webstore/detail/office-online/ndjpnladcallmjemlbaebfadecfhkepb?hl=es-419" title="Descargar: Office Online | Chrome Web Store" target="_blank" style="color: #d83b01;">Google Chrome</a> y <a href="https://www.microsoft.com/es-us/p/office-online/9nblggh4v88g?activetab=pivot%3aoverviewtab" title="Descargar: Office Online | Microsoft Store Online" style="color: #d83b01;">Microsoft Edge</a></small>
              </div>
            </div>

              <?php while ( $es_empresa_docs_query->have_posts() ) : $es_empresa_docs_query->the_post(); ?>
              <!-- do stuff ... -->
              <?php endwhile; ?>

      <?php

        if ( is_page('marco-legal') && $post_parent->post_name == 'empresa' ) {
          $es_empresa_docs_query_term = '';
        } elseif ( is_page('organigrama') && $post_parent->post_name == 'empresa' ) {
          $es_empresa_docs_query_term = 'organigrama';
        }

        $es_empresa_docs_terms_array = array(
         'taxonomy' => $es_empresa_docs_query->query_vars['taxonomy'],
         'hide_empty' => true);

      $es_empresa_docs_terms = get_terms( $es_empresa_docs_query->query_vars['taxonomy']); ?>

      <ul class="accordion" data-accordion data-allow-all-closed="true">

        <?php foreach ( $es_empresa_docs_terms as $es_empresa_docs_term ): ?>

          <?php
            $es_class_accordion = ( $es_empresa_docs_term === reset($es_empresa_docs_terms) ) ? 'accordion-item is-active' : 'accordion-item';

            if ($es_empresa_docs_term->count > 1) {
              $es_empresa_docs_count_label = $es_empresa_docs_term->count . ' documentos';
            } elseif ($es_empresa_docs_term->count == 1) {
              $es_empresa_docs_count_label = $es_empresa_docs_term->count . ' documento';
            }elseif ($es_empresa_docs_term->count < 1) {
              $es_empresa_docs_count_label = ' No existen documentos';
            }

            $es_id_accordion = $es_empresa_docs_term->slug . '-'. $es_empresa_docs_term->term_id;
          ?>

          <?php
          $es_empresa_docs_term_link = get_term_link($es_empresa_docs_term, $es_empresa_docs_term->taxonomy);

          if ($es_empresa_docs_term->parent ) {
             $es_empresa_doc_type_parent  = get_term( $es_empresa_docs_term->parent);
             $es_empresa_doc_type_parent_slug  = $es_empresa_doc_type_parent->slug;
           }
          ?>


          <?php
            if ( $es_empresa_doc_type_parent_slug == 'base-legal' &&
                (is_page('marco-legal') && $post_parent->post_name == 'empresa') && !($es_empresa_docs_term->slug == 'marco-legal')  ) {
              include(locate_template( '/framework/views/es-pages/_content-empresa-doc-index-category.php'));
            } elseif ( (is_page('organigrama') && $post_parent->post_name == 'empresa') && $es_empresa_docs_term->slug == 'organigrama') {
              include(locate_template( '/framework/views/es-pages/_content-empresa-doc-legal.php'));
            }
          ?>


        <?php endforeach ?>

      </ul>

      </div>
    </div>

    <?php  include(locate_template( '/framework/views/transparencia-doc/transparencia-doc-reveal.php')); ?>

  <?php else: ?>

  <?php endif ?>

  <?php wp_reset_postdata(); // Reset Post Data ?>