<?php

// =============================================================================
// VIEWS/TRANSPARENCIA DOCS/INDEX-YEAR.PHP
// -----------------------------------------------------------------------------
// Includes the index output.
// =============================================================================

?>

    <ul class="accordion" data-accordion data-allow-all-closed="true">

      <?php foreach ($es_proceso_de_compras_months as $es_proceso_de_compra_month): ?>

        <?php
          $es_proceso_de_compras_month = date( 'm', $es_proceso_de_compra_month );
          $es_proceso_de_compras_month_i18n = date_i18n( 'F', $es_proceso_de_compra_month );

          $es_proceso_de_compras_query_date_array = array(
            'year' => $es_proceso_de_compras_year,
            'month' => $es_proceso_de_compras_month
          );

          if ( is_post_type_archive() ) {
           $es_proceso_de_compras_query_args = array(
            'date_query' => $es_proceso_de_compras_query_date_array,
             'post_type' => 'proceso-de-compras',
             'posts_per_archive_page'  => -1
           );
          } elseif ( is_tax() ) {

            if ($es_proceso_de_compras_terms) {
              $es_proceso_de_compras_query_tax_array = array(
                'field'    => 'id',
                'taxonomy' => $es_proceso_de_compras_object->taxonomy,
                'terms' => $es_proceso_de_compras_term->term_id
              );
            } else {
              $es_proceso_de_compras_query_tax_array = array(
                'taxonomy' => $es_proceso_de_compras_object->taxonomy,
                'terms' => $es_proceso_de_compras_object->term_id
              );
            }

           $es_proceso_de_compras_query_args = array(
             'date_query' => $es_proceso_de_compras_query_date_array,
             'post_type' => 'proceso-de-compras',
             'posts_per_archive_page'  => -1,
             'tax_query' => array($es_proceso_de_compras_query_tax_array)
           );
          }

          $es_proceso_de_compras_query_month = new WP_Query( $es_proceso_de_compras_query_args );
        ?>

        <?php
          $es_class_accordion = ( $es_proceso_de_compra_month === reset($es_proceso_de_compras_months) ) ? 'accordion-item is-active' : 'accordion-item';

          if ($es_proceso_de_compras_query_month->post_count > 1) {
            $es_proceso_de_compras_count_label = $es_proceso_de_compras_query_month->post_count . ' procesos';
          } elseif ($es_proceso_de_compras_query_month->post_count == 1) {
            $es_proceso_de_compras_count_label = $es_proceso_de_compras_query_month->post_count . ' proceso';
          }elseif ($es_proceso_de_compras_query_month->post_count < 1) {
            $es_proceso_de_compras_count_label = ' No existen procesos';
          }

        ?>

          <li class="es-site-transparencia-docs-date <?php echo $es_class_accordion; ?>" data-accordion-item>

            <a href="#" class="accordion-title">
              <h5 class="es-transparencia-docs-date-title" >
                <i class="fas fa-folder"></i><?php echo $es_proceso_de_compras_month_i18n . ' ' . $es_proceso_de_compras_year; ?>
                <span class="es-transparencia-docs-count h5" style="font-size: 0.875rem;"><?php echo $es_proceso_de_compras_count_label; ?></span>
              </h5>

            </a>

            <div class="accordion-content" data-tab-content>

              <?php include(locate_template( '/framework/views/proceso-de-compras/proceso-de-compras-index.php') ); ?>

            </div>
          </li>

      <?php endforeach ?>

      <?php wp_reset_postdata(); // Reset Post Data ?>

    </ul>