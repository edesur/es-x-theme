<?php

// =============================================================================
// VIEWS/INTEGRITY/_CONTENT-POST-HEADER.PHP
// -----------------------------------------------------------------------------
// Standard <header> output for various posts.
// =============================================================================

?>

<header class="entry-header">

  <?php
    if ( is_post_type_archive() ) {
       $es_proceso_de_compras_title = post_type_archive_title('', false);
     } elseif ( is_tax() ) {
       $es_proceso_de_compras_title = tag_description( '', false );
     }
   ?>

  <h2 class="entry-title"><?php echo $es_proceso_de_compras_title; ?></h2>

</header>