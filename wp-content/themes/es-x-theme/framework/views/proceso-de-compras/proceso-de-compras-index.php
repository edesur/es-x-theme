<?php

// =============================================================================
// VIEWS/PROCESO-DE-COMPRAS/INDEX.PHP
// -----------------------------------------------------------------------------
// Includes the index output.
// =============================================================================

?>

  <?php if ( $es_proceso_de_compras_query_month->have_posts()  ): ?>

    <div class="es-transparencia-doc-content grid-container">
      <?php while ( $es_proceso_de_compras_query_month->have_posts() ) : $es_proceso_de_compras_query_month->the_post(); ?>

        <?php
          $proceso_de_compras_type_field = get_field('es_compras_proceso_compra_type_tax');
          $proceso_de_compras_type = $proceso_de_compras_type_field->slug;
          $proceso_de_compras_type_name = $proceso_de_compras_type_field->name;
          $proceso_de_compras_type_desc = $proceso_de_compras_type_field->description;

          // Status
          $proceso_de_compras_status_field = get_field('es_compras_proceso_compra_status_tax');
          $proceso_de_compras_status = $proceso_de_compras_status_field->slug;
          $proceso_de_compras_status_name = $proceso_de_compras_status_field->name;
          $proceso_de_compras_status_desc = $proceso_de_compras_status_field->description;

          // ID
          $es_proceso_compra_id = get_field('es_compras_proceso_compra_id');

          // Frequency
          $proceso_de_compras_frecuency_field = get_field('es_transparencia_documento_frecuency');
          $proceso_de_compras_frecuency = $proceso_de_compras_frecuency_field['value'];

          // File
          $proceso_de_compras_file = get_field('es_transparencia_documento_file');
          $proceso_de_compras_file_url = $proceso_de_compras_file['url'];
          $proceso_de_compras_file_title = $proceso_de_compras_file['title'];
          $proceso_de_compras_file_dir = get_attached_file($proceso_de_compras_file['id']);
          $proceso_de_compras_file_size = filesize($proceso_de_compras_file_dir);
          $proceso_de_compras_file_size_format = size_format($proceso_de_compras_file_size, 2);
          $proceso_de_compras_file_mime_type = $proceso_de_compras_file['mime_type'];

          // Date
          $proceso_de_compras_date_pub = get_field('es_compras_proceso_compra_date_published');
          $proceso_de_compras_date_start_field = get_field('es_transparencia_documento_date_period_start');
          $proceso_de_compras_date_end_field = get_field('es_transparencia_documento_date_period_end');

          $proceso_de_compras_date = strtotime($proceso_de_compras_date_pub);
          $proceso_de_compras_date_start = strtotime( $proceso_de_compras_date_start_field );
          $proceso_de_compras_date_end = strtotime( $proceso_de_compras_date_end_field );


          // Months
          $proceso_de_compras_date_month = date_i18n('F', $proceso_de_compras_date);
          $proceso_de_compras_date_start_month = date_i18n('F', $proceso_de_compras_date_start);
          $proceso_de_compras_date_end_month = date_i18n('F', $proceso_de_compras_date_end);

          // Years
          $proceso_de_compras_date_year = date_i18n('Y', $proceso_de_compras_date);
          $proceso_de_compras_date_start_year = date_i18n('Y', $proceso_de_compras_date_start);
          $proceso_de_compras_date_end_year = date_i18n('Y', $proceso_de_compras_date_end);

          switch ($proceso_de_compras_file_mime_type) {
            case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
              $proceso_de_compras_file_class = 'es-transparencia-doc-file';
              $proceso_de_compras_file_alt = 'MicroSoft Word';
              $proceso_de_compras_file_icon = 'far fa-file-word fa-3x';
              break;
            case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
              $proceso_de_compras_file_class = 'es-transparencia-doc-file';
              $proceso_de_compras_file_alt = 'MicroSoft Excel';
              $proceso_de_compras_file_icon = 'far fa-file-excel fa-3x';
              break;
            case 'application/vnd.ms-excel':
              $proceso_de_compras_file_class = 'es-transparencia-doc-file';
              $proceso_de_compras_file_alt = 'MicroSoft Excel';
              $proceso_de_compras_file_icon = 'far fa-file-excel fa-3x';
              break;
            case 'application/vnd.ms-excel.sheet.macroEnabled.12':
              $proceso_de_compras_file_class = 'es-transparencia-doc-file';
              $proceso_de_compras_file_alt = 'MicroSoft Excel';
              $proceso_de_compras_file_icon = 'far fa-file-excel fa-3x';
              break;
            case 'application/pdf':
              $proceso_de_compras_file_class = 'es-transparencia-doc-file';
              $proceso_de_compras_file_alt = 'Adobe PDF';
              $proceso_de_compras_file_icon = 'far fa-file-pdf fa-3x';
              break;

            default:
              $proceso_de_compras_file_class = 'es-transparencia-doc-file-none';
              $proceso_de_compras_file_icon = 'far fa-file fa-2x';
              break;
          }

          $proceso_de_compras_date_period = $proceso_de_compras_date_month . ' ' . $proceso_de_compras_date_year;

        // $proceso_de_compras_desc = $proceso_de_compras_type_name;

          switch ($proceso_de_compras_status) {

            case 'adjudicado':
              $proceso_de_compras_status_label = 'success';
              break;

            case 'desierto':
              $proceso_de_compras_status_label = 'alert';
              break;

            default:
              $proceso_de_compras_status_label = 'warning';
              break;
          }

        ?>

        <div class="es-transparencia-doc-single grid-x align-middle">

            <div class="es-transparencia-doc-copy">
              <h4 class="es-transparencia-doc-copy-title h6"><?php the_title(); ?></h4>
              <div class="es-transparencia-doc-copy-info">
                <div class="es-transparencia-doc-id"><?php echo $es_proceso_compra_id; ?></div>
                <div class="es-transparencia-doc-date">Publicado: <?php echo $proceso_de_compras_date_pub; ?></div>
              </div>
              <div class="es-proceso-compras-type-status">
                <div class="es-proceso-compras-type">
                  <span class="label secondary"><?php echo $proceso_de_compras_type_desc; ?></span>
                </div>
                <div class="es-proceso-compras-status">
                  <span class="label <?php echo $proceso_de_compras_status_label; ?>"><?php echo $proceso_de_compras_status_name; ?></span>
                </div>
                <!-- <li><pre><?php var_dump($proceso_de_compras_status); ?></pre></li> -->
              </div>
            </div>

            <?php

              /*
                <div class="es-transparencia-doc-download <?php echo $proceso_de_compras_file_class; ?>">

                  <div class="media-object align-middle align-right">

                      <div class="es-transparencia-doc-download-file <?php echo $proceso_de_compras_file_class; ?> media-object-section cell auto">

                        <ul class="no-bullet text-center">
                          <li><a class="button small expanded" href="<?php echo $proceso_de_compras_file_url; ?>" title="Descargar: <?php if (empty($proceso_de_compras_file_title)) {
                          the_title();
                        } else {
                          echo $proceso_de_compras_file_title;
                        } ?>">Descargar</a></li>
                          <li><small>(<?php echo  $proceso_de_compras_file_size_format; ?>)</small></li>
                        </ul>

                        <?php
                         ?>

                      </div>

                      <div class="<?php echo $proceso_de_compras_file_class; ?> media-object-section">
                        <i class="<?php echo $proceso_de_compras_file_icon; ?>"  title="<?php echo $proceso_de_compras_file_alt; ?>"></i>
                      </div>

                  </div>

                </div>
              */

             ?>

        </div>

      <?php endwhile; ?>
    </div>

  <?php else: ?>

    <?php

      if ( $es_proceso_de_compras_object->slug == 'plan-estrategico-institucional' ||
                        $es_proceso_de_compras_object->slug == 'presupuesto-aprobado' ||
                        $es_proceso_de_compras_object->slug == 'plan-anual-compras') {

        $proceso_de_compras_date_period = $es_transparencia_year;

      } else {
        $proceso_de_compras_date_period = $proceso_de_compras_month_i18n . ' ' . $es_transparencia_year;
      }

      $es_proceso_de_compras_term_slug = ( $es_proceso_de_compras_terms ) ? $es_proceso_de_compras_term->slug : $es_proceso_de_compras_object->slug ;
      $es_proceso_de_compras_term_name = ( $es_proceso_de_compras_terms ) ? $es_proceso_de_compras_term->name : $es_proceso_de_compras_object->name ;
      $es_proceso_de_compras_term_description = ( $es_proceso_de_compras_terms ) ? $es_proceso_de_compras_term->description : $es_proceso_de_compras_object->description ;

      // $proceso_de_compras_month_i18n
      switch ($es_proceso_de_compras_term_slug) {

        case 'balance-general':
          $proceso_de_compras_desc_none = 'No existe ' . $es_proceso_de_compras_term_name . ' disponible en el período ' . $proceso_de_compras_date_period;
          break;

        case 'activos-fijos':
          $proceso_de_compras_desc_none = 'No existen ' . $es_proceso_de_compras_term_name . ' disponibles para el período ' . $proceso_de_compras_date_period;
          break;

        case 'beneficiarios':
          $proceso_de_compras_desc_none = 'No Aplica. Esta empresa no posee fondos complementarios para programas asistenciales';
          $proceso_de_compras_desc_none = 'En el mes de ' . $proceso_de_compras_date_period . ' no tenemos ' . $es_proceso_de_compras_term_name;
          $proceso_de_compras_desc_none = 'No existen ' . $es_proceso_de_compras_term_description . ' en el período ' . $proceso_de_compras_date_period;
          break;
          break;

          case 'informes-auditorias':
          $proceso_de_compras_desc_none = 'No existen ' . $es_proceso_de_compras_term_name . ' en el período ' . $proceso_de_compras_date_period;
          break;

        case 'jubilaciones-pensiones-retiros':
          $proceso_de_compras_desc_none = 'No existen ' . $es_proceso_de_compras_term_name . ' disponibles para el período ' . $proceso_de_compras_date_period;
          break;

          case 'oai-informacion-clasificada':
          $proceso_de_compras_desc_none = 'No existe ' . $es_proceso_de_compras_term_name . ' en el período ' . $proceso_de_compras_date_period;
          break;

        case 'vacantes':
          $proceso_de_compras_desc_none = 'No existen ' . $es_proceso_de_compras_term_name . ' disponibles para el período ' . $proceso_de_compras_date_period;
          break;

        default:
        $proceso_de_compras_desc_none = 'No existe ' . $es_proceso_de_compras_term_name . ' en el período ' . $proceso_de_compras_date_period;
          break;
      }
    ?>
    <div class="es-transparencia-doc-copy">
      <h4 class="es-transparencia-doc-copy-title h5"><?php echo $es_proceso_de_compras_term_name . ' - ' . $proceso_de_compras_date_period; ?></h4>
      <p><?php echo $proceso_de_compras_desc_none; ?></p>
    </div>

  <?php endif ?>