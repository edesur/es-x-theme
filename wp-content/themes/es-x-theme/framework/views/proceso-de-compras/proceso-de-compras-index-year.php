<?php

// =============================================================================
// VIEWS/PROCESO-DE-COMPRAS/INDEX-YEAR.PHP
// -----------------------------------------------------------------------------
// Includes the index output.
// =============================================================================

?>

<ul class="accordion" data-accordion data-allow-all-closed="true">

  <?php foreach ($es_proceso_de_compras_years as $es_proceso_de_compras_year): ?>

    <?php
      if ( is_post_type_archive() ) {
       $es_proceso_de_compras_query_args = array(
          'year' => $es_proceso_de_compras_year,
         'post_type' => 'proceso-de-compras',
         'posts_per_archive_page'  => -1,
       );
      } elseif ( is_tax() ) {

        $es_proceso_de_compras_query_date_array = array(
          'taxonomy' => $es_proceso_de_compras_object->taxonomy,
          'terms' => $es_proceso_de_compras_object->term_id
        );

        if ($es_proceso_de_compras_terms) {
          $es_proceso_de_compras_query_tax_array = array(
            'field'    => 'id',
            'taxonomy' => $es_proceso_de_compras_object->taxonomy,
            'terms' => $es_proceso_de_compras_term->term_id
          );
        } else {
          $es_proceso_de_compras_query_tax_array = array(
            'taxonomy' => $es_proceso_de_compras_object->taxonomy,
            'terms' => $es_proceso_de_compras_object->term_id
          );
        }

       $es_proceso_de_compras_query_args = array(
         'year' => $es_proceso_de_compras_year,
         'post_type' => 'proceso-de-compras',
         'posts_per_archive_page'  => -1,
         'tax_query' => array($es_proceso_de_compras_query_tax_array)
       );
      }

      $es_proceso_de_compras_query_year = new WP_Query( $es_proceso_de_compras_query_args );
    ?>

    <?php
      $es_class_accordion = ( $es_proceso_de_compras_year === reset($es_proceso_de_compras_years) ) ? 'accordion-item is-active' : 'accordion-item';

      if ($es_proceso_de_compras_query_year->post_count > 1) {
        $es_proceso_de_compras_count_label = $es_proceso_de_compras_query_year->post_count . ' procesos';
      } elseif ($es_proceso_de_compras_query_year->post_count == 1) {
        $es_proceso_de_compras_count_label = $es_proceso_de_compras_query_year->post_count . ' proceso';
      }elseif ($es_proceso_de_compras_query_year->post_count < 1) {
        $es_proceso_de_compras_count_label = ' No existen procesos';
      }
    ?>

    <li class="es-site-transparencia-docs-date <?php echo $es_class_accordion; ?>" data-accordion-item>

      <a href="#" class="accordion-title">
        <h4 class="es-transparencia-docs-date-title h5"><i class="fas fa-archive"></i><?php echo $es_proceso_de_compras_year; ?> <small style="font-size: 0.875rem;">
          <?php echo $es_proceso_de_compras_count_label; ?>
        </small></h4>

      </a>

      <div class="accordion-content" data-tab-content>

        <?php

            $current_month = date('F');
            $current_month_digit = date('m');
            $current_year = date('Y');

            $display_month = Date('m', strtotime($current_month . " last month"));
            $current_month_format = Date('m', strtotime($current_month ));

            $es_loop_counter = ($es_proceso_de_compras_year == $current_year ) ?  $current_month_format :  12;

            $es_proceso_de_compras_months = array();

            for ( $m=1; $m<=$es_loop_counter; ++$m ) {
              $es_proceso_de_compras_months[] = mktime(0, 0, 0, $m, 1);
            }

            $es_proceso_de_compras_months = array_reverse($es_proceso_de_compras_months);

          include(locate_template( '/framework/views/proceso-de-compras/proceso-de-compras-index-month.php'));
        ?>

      </div>
    </li>

    <?php wp_reset_postdata(); // Reset Post Data ?>

  <?php endforeach ?>

</ul>