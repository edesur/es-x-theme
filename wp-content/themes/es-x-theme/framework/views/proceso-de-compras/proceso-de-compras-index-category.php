<?php

// =============================================================================
// VIEWS/PROCESO-DE-COMPRAS/CATEGORY-YEAR.PHP
// -----------------------------------------------------------------------------
// Includes the index output.
// =============================================================================

?>

<ul class="accordion" data-accordion data-allow-all-closed="true">

    <?php if ( $es_proceso_de_compras_terms ): ?>

          <?php foreach ( $es_proceso_de_compras_terms as $es_proceso_de_compras_term ): ?>

            <?php
              $es_class_accordion = ( $es_proceso_de_compras_term ===reset($es_proceso_de_compras_terms) ) ? 'accordion-item is-active' : 'accordion-item';

              $es_id_accordion = $es_proceso_de_compras_term->slug . '-'. $es_proceso_de_compras_term->term_id;
            ?>

            <?php $es_proceso_de_compras_term_link = get_term_link($es_proceso_de_compras_term, $es_proceso_de_compras_term->taxonomy); ?>

            <li class="accordion-item" data-accordion-item>
              <a href="" class="accordion-title">
                <h4 class="es-transparencia-docs-cat-title h5"></i><?php echo $es_proceso_de_compras_term->name; ?><small style="font-size: 0.875rem;">
                </small></h4>
              </a>

              <div class="accordion-content" data-tab-content>
                  <?php include(locate_template( '/framework/views/proceso-de-compras/proceso-de-compras-index-year.php')); ?>
              </div>

            </li>
          <?php endforeach ?>

    <?php else: ?>

      <?php include(locate_template( '/framework/views/proceso-de-compras/proceso-de-compras-index-year.php')); ?>

    <?php endif ?>

  </ul>

</ul>