<?php

// =============================================================================
// VIEWS/PROCESOS DE COMPRAS/_INDEX.PHP
// -----------------------------------------------------------------------------
// Includes the index output.
// =============================================================================
$stack = x_get_stack();

$term = wp_get_post_terms();

if ( is_home() ) :
  $style     = x_get_option( 'x_blog_style' );
  $cols      = x_get_option( 'x_blog_masonry_columns' );
  $condition = is_home() && $style == 'masonry';
elseif ( is_archive() ) :
  $style     = x_get_option( 'x_archive_style' );
  $cols      = x_get_option( 'x_archive_masonry_columns' );
  $condition = is_archive() && $style == 'masonry';
elseif ( is_tax('proceso-de-compras', $term) ) :
  $style     = x_get_option( 'x_archive_style' );
  $cols      = x_get_option( 'x_archive_masonry_columns' );
  $condition = is_tax('proceso-de-compras', $term) && $style == 'masonry';
elseif ( is_search() ) :
  $condition = false;
endif;

$es_proceso_de_compras_object = get_queried_object();

if ( is_post_type_archive() ) {
 $es_proceso_de_compras_query_args = array(
   'post_type' => 'proceso-de-compras',
   'posts_per_archive_page'  => -1,
 );
} elseif ( is_tax() ) {

  $es_proceso_de_compras_tax = $es_proceso_de_compras_object->taxonomy;
  $es_proceso_de_compras_tax_parent = $es_proceso_de_compras_object->parent;

  $es_proceso_de_compras_terms_array = array(
   'parent' => $es_proceso_de_compras_object->term_id,
   'taxonomy' => $es_proceso_de_compras_object->taxonomy,
   'hide_empty' => false);

  $es_proceso_de_compras_terms = get_terms( $es_proceso_de_compras_object->taxonomy, $es_proceso_de_compras_terms_array);

  $es_proceso_de_compras_terms_child = get_terms( $es_proceso_de_compras_object->taxonomy, $es_proceso_de_compras_terms_array);

  $es_proceso_de_compras_query_tax_array = array(
    'taxonomy' => $es_proceso_de_compras_object->taxonomy,
    'terms' => $es_proceso_de_compras_object->term_id
  );

 $es_proceso_de_compras_query_args = array(
   'post_type' => 'proceso-de-compras',
   'posts_per_archive_page'  => -1,
   'tax_query' => array($es_proceso_de_compras_query_tax_array)
 );

}

$es_proceso_de_compras_query = new WP_Query( $es_proceso_de_compras_query_args );

?>

<?php if ( $condition) : ?>

    <div class="es-site-transparencia-docs entry-wrap">

      <?php x_get_view( 'proceso-de-compras', '_content', 'post-header-proceso-de-compras' ); ?>

      <div class="es-site-transparencia-docs-container">
          <div class="es-site-transparencia-docs-sidebar">
            <?php dynamic_sidebar('ups-sidebar-edesur-sidebar-transparencia'); ?>
          </div>
          <div class="es-site-transparencia-docs-content">

            <?php

              if ($es_proceso_de_compras_query->posts) {
                $es_proceso_de_compras_years = array();

                  foreach ($es_proceso_de_compras_query->posts as $es_proceso_de_compras_post) {

                    $es_proceso_de_compras_years[] = date_format( date_create( $es_proceso_de_compras_post->post_date ), 'Y' );

                  }

                  $es_proceso_de_compras_years = array_unique($es_proceso_de_compras_years);
              } else {
                $es_proceso_de_compras_years = array();
                $current_year = date('Y');

                $es_proceso_de_compras_years[] = $current_year;
                $es_proceso_de_compras_years[] = $current_year - 1;

                $es_proceso_de_compras_years = array_unique($es_proceso_de_compras_years);
              }

            ?>

            <small>Estas extensiones son recomendadas para visualizar los documentos en el explorador:</small>

            <div class="es-transparencia-download-acrobat media-object align-middle" style="margin-bottom: 0.125rem">
              <div class="es-transparencia-doc-file-pdf media-object-section">
                <i class="far fa-file-pdf"></i>
              </div>
              <div class="media-object-section">
                <small><a href="https://get.adobe.com/reader/?loc=es" title="Descargar Adobe Acrobat Reader DC" target="_blank" style="color: hsl(0, 100%, 50%);">Abobe Reader DC</a> para documentos PDF</small>
              </div>
            </div>

            <div class="es-transparencia-download-acrobat media-object align-middle">
              <div class="es-transparencia-doc-file-pdf media-object-section">
                <i class="fab fa-windows" style="color: #d83b01;"></i>
              </div>
              <div class="media-object-section">
                <small><a href="https://support.office.com/es-es/article/acceso-rápido-a-los-archivos-de-office-en-el-explorador-dc1024b4-92be-46eb-81a7-aea85368baa0" target="_blank" title="Acceso rápido a los archivos de Office en el explorador
                " style="color: #d83b01;">Office Online</a> para documentos Word y Excel  para <a href="https://chrome.google.com/webstore/detail/office-online/ndjpnladcallmjemlbaebfadecfhkepb?hl=es-419" title="Descargar: Office Online | Chrome Web Store" target="_blank" style="color: #d83b01;">Google Chrome</a> y <a href="https://www.microsoft.com/es-us/p/office-online/9nblggh4v88g?activetab=pivot%3aoverviewtab" title="Descargar: Office Online | Microsoft Store Online" style="color: #d83b01;">Microsoft Edge</a></small>
              </div>
            </div>

            <?php

              if ( is_tax() ) {
                include(locate_template( '/framework/views/proceso-de-compras/proceso-de-compras-index-category.php'));
              } else {
                include(locate_template( '/framework/views/proceso-de-compras/proceso-de-compras-index-year.php'));
              }

            ?>

          </div>
      </div>
    </div>

<?php endif; ?>

<?php wp_reset_postdata(); // Reset Post Data ?>