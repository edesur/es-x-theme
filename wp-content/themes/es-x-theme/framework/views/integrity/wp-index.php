<?php

// =============================================================================
// VIEWS/INTEGRITY/WP-INDEX.PHP
// -----------------------------------------------------------------------------
// Index page output for Integrity.
// =============================================================================

?>

  <?php get_header(); ?>

    <?php $term = wp_get_post_terms(); ?>

    <?php

      /*

        <?php if ( ( is_archive() && get_post_type() == 'proceso-de-compra') ||
                              is_archive() && ( is_tax( 'modalidad', $term ) ||
                              is_tax( 'estado', $term ) ) ): ?>
          <div class="x-container max width offset entry-wrap">
             <header class="entry-header">

             <?php if (is_tax()): ?>
               <h1 class="entry-title"><?php  echo term_description(); ?></h1>
             <?php else: ?>
               <h1 class="entry-title"><?php  post_type_archive_title(''); ?></h1>
             <?php endif ?>

             </header>
            <div class="entry-content">
              <div class="x-column x-sm x-1-4">
                <?php dynamic_sidebar('ups-sidebar-edesur-sidebar-transparencia'); ?>
              </div>

              <div class="x-column x-sm x-3-4">

                <?php if (is_tax('modalidad' ) || is_tax('estado' )): ?>
                  <?php x_get_view( 'es-proceso-de-compra', '_content-pending' ); ?>
                <?php else: ?>
                  <?php x_get_view( 'global', '_index' ); ?>
                <?php endif ?>

              </div>
            </div>
          </div>

      */

     ?>

    <?php if ( get_post_type() == 'proceso-de-compras' || ( is_tax( 'modalidad', $term ) ||
                          is_tax( 'estado', $term ) ) ): ?>

      <div class="x-container max width offset">

        <?php x_get_view( 'proceso-de-compras', '_index' ); ?>
      </div>


    <?php elseif ( is_archive() && get_post_type() == 'mantenimiento'  ): ?>

      <div class="x-container max width offset">
        <?php x_get_view( get_post_type(), 'content' ); ?>
      </div>

    <?php elseif ( is_archive() && ( get_post_type() == 'pregunta-frecuente' || get_post_type() == 'nomina') ): ?>

      <div class="x-container max width offset">
        <?php x_get_view( get_post_type(), '_index' ); ?>
      </div>

    <?php elseif (get_post_type() == 'transparencia-doc' || is_tax('categoria-transparencia', $term)): ?>

      <div class="x-container max width offset">

        <?php x_get_view( 'transparencia-doc', '_index' ); ?>
      </div>

    <?php else: ?>

      <div class="x-container max width offset">

        <div class="<?php x_main_content_class(); ?>" role="main">

          <?php x_get_view( 'global', '_index' ); ?>

        </div>

        <?php get_sidebar(); ?>

      </div>
    <?php endif ?>

  <?php get_footer(); ?>