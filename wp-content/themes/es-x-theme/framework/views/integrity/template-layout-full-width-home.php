<?php

// =============================================================================
// VIEWS/INTEGRITY/TEMPLATE-LAYOUT-FULL-WIDTH-HOME.PHP (Home | Template)
// -----------------------------------------------------------------------------
// Fullwidth Home page output for Integrity.
// =============================================================================

?>

<?php get_header(); ?>

  <?php /*putRevSlider( 'es-slider-home' );*/ ?>

  <h1>Home | Edesur</h1>

  <div class="x-container max width offset">
    <div class="<?php x_main_content_class(); ?>" role="main">

      <?php while ( have_posts() ) : the_post(); ?>
        <?php x_get_view( 'integrity', 'content', 'page' ); ?>
        <?php x_get_view( 'global', '_comments-template' ); ?>
      <?php endwhile; ?>

    </div>
  </div>

<?php get_footer(); ?>