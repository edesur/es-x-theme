<?php

// =============================================================================
// VIEWS/INTEGRITY/CONTENT.PHP
// -----------------------------------------------------------------------------
// Standard post output for Integrity.
// =============================================================================

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="entry-featured">
      <?php x_featured_image(); ?>
    </div>
    <?php if (is_archive() && get_post_type() == 'proceso-de-compra') : ?>
      <?php x_get_view(get_post_type(), '_es', 'content-post-header'); ?>
    <?php else : ?>

      <?php if (get_post_type() == 'informacion' || get_post_type() == 'servicio') : ?>
        <div class="entry-wrap es-site-servicios-info">
        <?php else : ?>
          <div class="entry-wrap">
          <?php endif ?>


          <?php if (
            get_post_type() == 'mantenimiento' ||
            get_post_type() == 'informacion' ||
            get_post_type() == 'servicio'
          ) : ?>

            <?php x_get_view('es-partials', '_content-post-header'); ?>

          <?php else : ?>
               <?php x_get_view('integrity', '_content', 'post-header'); ?>
          <?php endif ?>

          <?php x_get_view('global', '_content'); ?>

        </div>
      <?php endif ?>

    <?php x_get_view('integrity', '_content', 'post-footer'); ?>
</article>