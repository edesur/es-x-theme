 <?php if (get_fields($post->id)): ?>

  <pre> <?php  var_dump($es_servicio_info_choice_field); ?></pre>

  <div class="es-cell-item-the-content">
    <?php if ($es_servicio_description): ?>

              <p><?php echo $es_servicio_description; ?></p>

        <?php endif ?>
  </div>

  <ul class="es-accordion-servicios-info accordion" data-accordion>

    <?php if ($es_servicio_description): ?>
      <li class="accordion-item is-active" data-accordion-item>
        <a href="#" class="accordion-title"><h3 class="es-servicios-info-title h5"> <i class="fas fa-info-circle"></i> Descripción</h3></a>

        <div class="accordion-content" data-tab-content>
          <p><?php echo $es_servicio_description; ?></p>
        </div>
      </li>
    <?php endif ?>

    <?php if ($es_servicio_target): ?>
      <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title"><h3 class="es-servicios-info-title h5"><i class="fas fa-users"></i> A quién va dirigido el servicio</h3></a>

        <div class="accordion-content" data-tab-content>
          <p><?php echo $es_servicio_target; ?></p>
        </div>
      </li>
    <?php endif ?>

    <?php if ($es_servicio_responsable_area): ?>
      <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title"><h3 class="es-servicios-info-title h5"><i class="fas fa-building"></i> Área responsable</h3></a>

        <div class="accordion-content" data-tab-content>
          <p><?php echo $es_servicio_responsable_area; ?></p>

          <ul class="no-bullet">
            <li>Teléfono: <?php echo $es_servicio_responsable_area_phone; ?></li>
            <?php if ($es_servicio_responsable_area_fax): ?>
              <li>Fax: <?php echo $es_servicio_responsable_area_fax; ?></li>
            <?php endif ?>
            <li>Correo electrónico: <?php echo $es_servicio_responsable_area_email; ?></li>
            <li>Localidad física: <?php echo $es_servicio_responsable_area_location; ?></li>
          </ul>
        </div>
      </li>
    <?php endif ?>

    <?php if ($es_servicio_requirements): ?>
      <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title"><h3 class="es-servicios-info-title h5"><i class="fas fa-tasks"></i> Requerimientos o requisitos del servicio</h3></a>

        <div class="accordion-content" data-tab-content>
          <?php echo $es_servicio_requirements; ?>
        </div>
      </li>
    <?php endif ?>

    <?php if ($es_servicio_process): ?>
      <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title"><h3 class="es-servicios-info-title h5"><i class="fas fa-list-ol"></i> Procedimiento a seguir para obtener el servicio</h3></a>

        <div class="accordion-content" data-tab-content>
          <?php echo $es_servicio_process; ?>
        </div>
      </li>
    <?php endif ?>

    <?php if ($es_servicio_hours_select): ?>
      <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title"><h3 class="es-servicios-info-title h5"><i class="fas fa-clock"></i> Horario de prestación del servicio</h3></a>

        <div class="accordion-content" data-tab-content>

          <?php if ($es_servicio_hours_select == '24-hours'): ?>

            <p>24 horas los 365 días del año</p>

          <?php else: ?>

            <?php if ( have_rows('es_servicio_hours') ): ?>

              <table class="es-table-servicios-horario">
                <thead>
                  <tr>
                    <th>Día</th>
                    <th>Apertura</th>
                    <th>Cierre</th>
                  </tr>
                </thead>
                <tbody>

                  <?php while( have_rows('es_servicio_hours') ): the_row() ?>

                    <?php
                      $es_servicio_hours_day_field = get_sub_field('es_servicio_hours_day');
                      $es_servicio_hours_day = $es_servicio_hours_day_field['label'];
                      $es_servicio_hours_time_start = get_sub_field('es_servicio_hours_time_start');
                      $es_servicio_hours_time_end = get_sub_field('es_servicio_hours_time_end');
                     ?>

                     <tr>
                       <td><?php echo $es_servicio_hours_day; ?></td>
                       <td><?php echo $es_servicio_hours_time_start; ?></td>
                       <td><?php echo $es_servicio_hours_time_end; ?></td>
                     </tr>

                  <?php endwhile; ?><!-- end of the loop -->

                </tbody>
              </table>

            <?php endif ?>

          <?php endif ?>

        </div>
      </li>
    <?php endif ?>

    <?php if ($es_servicio_cost): ?>
      <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title"><h3 class="es-servicios-info-title h5"><i class="fas fa-file-invoice-dollar"></i> Costo del servicio</h3></a>

        <div class="accordion-content" data-tab-content>
          <p><?php echo $es_servicio_cost; ?></p>
        </div>
      </li>
    <?php endif ?>

    <?php if ($es_servicio_process_time): ?>
      <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title"><h3 class="es-servicios-info-title h5"><i class="fas fa-stopwatch"></i> Tiempo de realización</h3></a>

        <div class="accordion-content" data-tab-content>
          <p><?php echo $es_servicio_process_time; ?></p>
        </div>
      </li>
    <?php endif ?>

    <?php if ($es_servicio_url): ?>
      <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title"><h3 class="es-servicios-info-title h5"><i class="fas fa-globe"></i> URL del servicio</h3></a>

        <div class="accordion-content" data-tab-content>
          <p><a href="<?php echo $es_servicio_url; ?>" title="<?php the_title();?>"><?php echo $es_servicio_url; ?></a></p>
        </div>
      </li>
    <?php endif ?>

    <?php if ($es_servicio_type_field): ?>
      <li class="accordion-item" data-accordion-item>
      <a href="#" class="accordion-title"><h3 class="es-servicios-info-title h5"><i class="fas fa-store"></i> Canales de prestación del servicio</h3></a>

      <div class="accordion-content" data-tab-content>

        <ul class="no-bullet">
          <?php foreach ($es_servicio_type_field as $es_servicio_type): ?>
            <li><?php echo $es_servicio_type->name; ?></li>
          <?php endforeach ?>
        </ul>

      </div>
    </li>
    <?php endif ?>

    <?php if ($es_servicio_info_extra): ?>
      <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title"><h3 class="es-servicios-info-title h5"><i class="fas fa-plus-circle"></i> Información adicional</h3></a>

        <div class="accordion-content" data-tab-content>
          <?php echo $es_servicio_info_extra; ?>
        </div>
      </li>
    <?php endif ?>

    <?php if ($es_servicio_documentos): ?>
      <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title"><h3 class="es-servicios-info-title h5"><i class="fas fa-users"></i> Folletos disponibles</h3></a>

        <div class="accordion-content" data-tab-content>
          <?php include( locate_template( '/framework/views/servicio/_content-excerpt-download.php' ) ); ?>
        </div>
      </li>
    <?php endif ?>

  </ul>

  <?php if ( $es_servicio_form): ?>
    <div class="es-cell-item">
      <?php echo do_shortcode( $es_servicio_form );  ?>
    </div>
  <?php endif ?>

 <?php endif ?>

