<?php
if (get_fields($post->id)) {
  $es_servicio_info_choice_field = get_field('es_servicio_informacion_chioce');
  $es_servicio_description = get_field('es_servicio_description');
  $es_servicio_type_field = get_field('es_servicio_type');
  $es_servicio_target = get_field('es_servicio_target');
  $es_servicio_responsable_area = get_field('es_servicio_responsable_area');
  $es_servicio_responsable_area_phone = get_field('es_servicio_responsable_area_phone');
  $es_servicio_responsable_area_fax = get_field('es_servicio_responsable_area_fax');
  $es_servicio_responsable_area_email = get_field('es_servicio_responsable_area_email');
  $es_servicio_responsable_area_location = get_field('es_servicio_responsable_area_location');
  $es_servicio_requirements = get_field('es_servicio_requirements');
  $es_servicio_process = get_field('es_servicio_process');
  $es_servicio_info_extra = get_field('es_servicio_info_extra');
  $es_servicio_hours_select_field = get_field('es_servicio_hours_select');
  $es_servicio_hours_select = $es_servicio_hours_select_field['value'];
  $es_servicio_hours = get_field('es_servicio_hours');
  $es_servicio_cost = get_field('es_servicio_cost');
  $es_servicio_process_time = get_field('es_servicio_process_time');
  $es_servicio_url = get_field('es_servicio_url');
  $es_servicio_documentos = get_field('es_servicio_documentos');
  $es_servicio_form = get_field('es_servicio_form');
}

?>
