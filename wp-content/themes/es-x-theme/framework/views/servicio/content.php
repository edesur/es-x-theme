<?php

// =============================================================================
// VIEWS/INTEGRITY/CONTENT.PHP
// -----------------------------------------------------------------------------
// Standard post output for Integrity.
// =============================================================================

?>

  <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

        <div class="es-site-servicios-info entry-wrap">

           <div class="es-section-grid-servicios-info">
             <div class="es-cell-item-content-servicios-info">

                 <div class="es-cell-item-entry-featured ">
                   <?php x_featured_image(); ?>
                 </div>

                 <header class="es-cell-item-entry-header">
                   <h1 class="entry-title"><?php the_title(); ?></h1>

                   <?php if (get_post_type() == 'servicio'): ?>

                     <?php
                       $es_servicio_nickname = get_field('es_servicio_nickname');
                     ?>

                     <?php if ($es_servicio_nickname): ?>
                       <h2 class="subheader"><small><?php echo $es_servicio_nickname; ?></small></h2>
                     <?php endif ?>

                   <?php endif ?>
                 </header>

                 <div class="es-cell-item-the-content">
                  <?php

                      if (is_singular() && get_fields($post->id) ) {
                          include(locate_template('/framework/views/servicio/_content-servicio-fields.php'));
                          include( locate_template( '/framework/views/servicio/_content-servicio.php' ) );

                      }

                   ?>
                 </div>

                  <?php /*_content-wp-post-rating*/ ?>
                 <?php include( locate_template( '/framework/views/es-partials/_content-wp-post-rating.php' ) ); ?>
                 <?php /* _content-wp-post-rating */ ?>

                <?php /* _content-kk-star-ratings */ ?>
                 <?php /*include( locate_template( '/framework/views/es-partials/_content-kk-star-ratings.php' ) );*/ ?>
                 <?php /* _content-kk-star-ratings */ ?>

                <?php echo do_shortcode('[addthis tool="addthis_inline_share_toolbox_hfga"]');  ?>

             </div>

             <div class="es-cell-item-sidebar-servicios-info">
               <?php echo do_shortcode( '[the_grid name="Edesur Información de Servicios The Grid"]' ); ?>
             </div>

           </div>

        </div>

    <?php x_get_view( 'integrity', '_content', 'post-footer' ); ?>
  </article>