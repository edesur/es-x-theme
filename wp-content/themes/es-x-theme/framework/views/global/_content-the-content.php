<?php

// =============================================================================
// VIEWS/GLOBAL/_CONTENT-THE-CONTENT.PHP
// -----------------------------------------------------------------------------
// Display of the_content() for various entries.
// =============================================================================

?>

<?php do_action( 'x_before_the_content_begin' ); ?>

<div class="entry-content content">

<?php do_action( 'x_after_the_content_begin' ); ?>

  <?php
    if ( is_page( 'rehabilitacion-de-redes' ) ) {

      x_get_view( 'es-pages', '_content-rehabilitacion-redes');

    } elseif ( is_page( 'sur-contigo' ) ) {

      x_get_view( 'es-pages', '_content-sur-contigo');

    } elseif (is_page( 'pagos-recurrentes' )) {

      x_get_view( 'es-pages', '_content-pagos-recurrentes');

    } elseif (is_page( 'puntos-de-pago' )) {

      x_get_view( 'es-pages', '_content-puntos-de-pago');

    }  elseif ( is_page( 'buzon-de-sugerencias' ) ||
                      is_page( 'actualizacion-de-datos' ) ||
                      is_page( 'denuncia-de-fraude' ) ) {

      x_get_view( 'es-pages', '_content-page-servicios');

    } else {
      the_content();
    }

   ?>

  <?php x_link_pages(); ?>

<?php do_action( 'x_before_the_content_end' ); ?>

</div>

<?php do_action( 'x_after_the_content_end' ); ?>