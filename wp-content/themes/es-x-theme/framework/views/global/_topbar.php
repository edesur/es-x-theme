<?php

// =============================================================================
// VIEWS/GLOBAL/_TOPBAR.PHP
// -----------------------------------------------------------------------------
// Includes topbar output.
// =============================================================================

?>

<?php if ( x_get_option( 'x_topbar_display' ) == '1' ) : ?>

  <div class="x-topbar">
    <div class="x-topbar-inner x-container max width">
    <?php x_get_view( 'global', '_brand' ); ?>
      <?php if ( x_get_option( 'x_topbar_content' ) != '' ) : ?>
      <p class="p-info"><?php echo x_get_option( 'x_topbar_content' ); ?></p>
      <?php endif; ?>
      <div class="x-social-global">
        <img src="//localhost/edesur/es-x-theme-wp/wp-content/uploads/2016/10/es-site-escudo-rd-copy.png" alt="República Dominicana" width="176">
      </div>

      <?php /*x_social_global();*/ ?>
    </div>
  </div>

<?php endif; ?>