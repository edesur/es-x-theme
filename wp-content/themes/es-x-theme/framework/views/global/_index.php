<?php

// =============================================================================
// VIEWS/GLOBAL/_INDEX.PHP
// -----------------------------------------------------------------------------
// Includes the index output.
// =============================================================================
$stack = x_get_stack();

if ( is_home() ) :
  $style     = x_get_option( 'x_blog_style' );
  $cols      = x_get_option( 'x_blog_masonry_columns' );
  $condition = is_home() && $style == 'masonry';
elseif ( is_archive() ) :
  $style     = x_get_option( 'x_archive_style' );
  $cols      = x_get_option( 'x_archive_masonry_columns' );
  $condition = is_archive() && $style == 'masonry';
elseif ( is_search() ) :
  $condition = false;
endif;

?>

<?php if ( $condition && get_post_type() != 'proceso-de-compra' ) : ?>

  <?php x_get_view( 'global', '_script', 'isotope-index' ); ?>

  <div id="x-iso-container" class="x-iso-container x-iso-container-posts cols-<?php echo $cols; ?>">

    <?php if ( get_post_type() == 'informacion' || get_post_type() == 'servicio' ): ?>

      <?php include(locate_template( '/framework/views/servicio/content-query.php')); ?>

    <?php elseif ( have_posts() ) : ?>
      <?php while ( have_posts() ) : the_post(); ?>
        <?php if ( $stack != 'ethos' ) : ?>
          <?php x_get_view( $stack, 'content', get_post_format() ); ?>
        <?php else : ?>
          <?php x_ethos_entry_cover( 'main-content' ); ?>
        <?php endif; ?>
      <?php endwhile; ?>
    <?php else : ?>
      <?php x_get_view( 'global', '_content-none' ); ?>
    <?php endif; ?>

  </div>

<?php else : ?>


    <?php if ( have_posts() ) : ?>
      <?php while ( have_posts() ) : the_post(); ?>

        <?php if (is_search()): ?>
          <?php include(locate_template( '/framework/views/es-pages/_content-search.php')); ?>
        <?php else: ?>
          <?php x_get_view( $stack, 'content', get_post_format() ); ?>
        <?php endif ?>

      <?php endwhile; ?>
    <?php else : ?>
      <?php x_get_view( 'global', '_content-none' ); ?>
    <?php endif; ?>

<?php endif; ?>

<?php
  $es_post_type_index = get_post_type();
  if (!($es_post_type_index == 'informacion' || $es_post_type_index == 'servicio')) {
    pagenavi();
  }

?>