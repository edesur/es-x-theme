<?php

// =============================================================================
// VIEWS/GLOBAL/_CONTENT-404.PHP
// -----------------------------------------------------------------------------
// Output for the 404 page.
// =============================================================================

?>

<?php

  global $wp;

  $page = get_page_by_path( 'transparencia' );
  $page_url = get_permalink($page->ID);

  if ( preg_match( '/transparencia/', $wp->request ) ) {
    $es_url_404 = get_page_link($page->ID);
    $es_link_title_404 = $page->post_title;
    $es_text_404 =  'página de inicio de Transparencia';
  } else {
    $es_url_404 = home_url();
    $es_link_title_404 = 'Inicio | ' . get_bloginfo('name');
    $es_text_404 =  'página de inicio';
  }

?>




<div class="es-site-page-404 es-cell-item text-center">

  <h1>Error: 404</h1>

  <p>
    <?php _e('La página que está buscando no existe.', '__x__'); ?><br>
    <?php _e('Puede intentar buscar en el siguiente formulario.', '__x__'); ?>
  </p>

    <?php
        $es_site_page_search_404 = get_search_form();
    ?>

  <p><?php echo $es_site_page_search_404; ?></p>

  <p>Regresar a la <a href="<?php echo $es_url_404; ?>" title="<?php echo $es_link_title_404; ?>"><?php echo $es_text_404; ?></a> o la <a class="x-btn" href="https://ov.edesur.com.do" target="_blank" title="Oficina Virtual | Edesur Dominicana">Oficina Virtual</a></p>



</div>