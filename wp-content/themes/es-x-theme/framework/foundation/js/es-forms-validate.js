jQuery(document).ready(function($) {

    $es_form_field_validate_error_msg = function () {

      $es_form_input_req_field = $('.ninja-forms-field');

      $es_error_msg_text = '';

      $es_validate_error_msg = $('<div></div>', {
          class: 'nf-error-msg es-form-valitate-msg',
          html: $es_error_msg_text
        });

  $('.ninja-forms-field').on('keypress', function(e) {
    var $this = $(this);

    if ( $this.hasClass('es-form-input-number') || $this.attr('type') === 'tel' ) {
      $es_error_msg_text = 'Solo se permiten numeros';
      $es_validate_error_msg.html( $es_error_msg_text );
    } else {
      $es_error_msg_text = 'No se permiten caracteres como "< #\\ $  % & > "';
      $es_validate_error_msg.html( $es_error_msg_text );
    }

    if ( $this.parents('.field-wrap').hasClass('nf-fail') || $this.val() == '' ) {
      // $this.parents('.nf-field-container').addClass('es-form-container-error');
    } else {
      // $this.parents('.nf-field-container').removeClass('es-form-container-error');
    }

    if ( !$this.parents('.field-wrap').find('.nf-error-msg').length ) {
      $this.parents('.field-wrap').removeClass('nf-pass').addClass('nf-fail nf-error')
      $this.parents('.nf-field-container').find('.nf-error-wrap.nf-error').append($es_validate_error_msg);
    }

    if ( (e.which >= 48 && e.which <= 57) && ( $this.hasClass('es-form-input-number') || $this.attr('type') == 'tel' ) ||
    !(e.which == 35 || e.which == 36 || e.which == 37 || e.which == 38 || e.which == 40 || e.which == 41 || e.which == 45 || e.which == 60 || e.which == 62  || e.which == 91 || e.which == 92 || e.which == 93 || e.which == 123 || e.which == 124 || e.which == 125) && !($this.hasClass('es-form-input-number')  || $this.attr('type') == 'tel') ) {
      $this.parents('.field-wrap').removeClass('nf-fail nf-error').addClass('nf-pass');
      $this.parents('.nf-field-container').find('.nf-error-msg').remove();
    }

    if ( e.which == 35 || e.which == 36 || e.which == 37 || e.which == 38 || e.which == 40 || e.which == 41 || e.which == 45 || e.which == 60 || e.which == 62  || e.which == 91 || e.which == 92 || e.which == 93 || e.which == 123 || e.which == 124 || e.which == 125 ) {
      return false;
    }
  });

    $('.ninja-forms-field').on('blur', function(e) {
      var $this = $(this);
      if ( $this.parents('.nf-field-container').find('.nf-error-msg.es-form-valitate-msg').length ) {
        $this.parents('.nf-field-container').find('.nf-error-msg.es-form-valitate-msg').remove();
      }

      if ($this.parents('.field-wrap').hasClass('nf-fail') || $this.val() == '' ) {
        $this.parents('.nf-field-container').css('margin-bottom', '7px');
      } else {
        $this.parents('.nf-field-container').css('margin-bottom', '25px');
      }
    });

    $('.ninja-forms-field').on('focus', function(e) {
       var $this = $(this);
      if ( $this.parents('.field-wrap').hasClass('nf-fail')/* || $this.val() == '' */) {
        $this.parents('.nf-field-container').css('margin-bottom', '7px');
      } else {
       $this.parents('.nf-field-container').css('margin-bottom', '25px');
      }
    });

    $('.ninja-forms-field').on('keyup', function(e) {
       var $this = $(this);

      if ( $this.parents('.field-wrap').hasClass('nf-fail') || $this.val() == '' ) {
        $this.parents('.nf-field-container').css('margin-bottom', '7px');
      } else {
        $this.parents('.nf-field-container').css('margin-bottom', '25px');
      }

      if ( e.which == 35 || e.which == 36 || e.which == 37 || e.which == 38 || e.which == 40 || e.which == 41 || e.which == 45 || e.which == 60 || e.which == 62  || e.which == 91 || e.which == 92 || e.which == 93 || e.which == 123 || e.which == 124 || e.which == 125 ) {

        // return false;
      }
    });

    $('.ninja-forms-field[maxlength]').on('focus', function(e) {
       var $this = $(this);

      if ( $this.parents('.nf-field-container').find('.nf-input-limit').length && !($this.parents('.field-wrap').hasClass('nf-fail'))) {
        $this.parents('.nf-field-container').find('.nf-input-limit').css({
          'display': 'block'/*,
          'margin-top': 0*/
        });
        $this.parents('.nf-field-container').css('margin-bottom', '7px');
      }
    });

    $('.ninja-forms-field[maxlength]').on('blur', function(e) {
      var $this = $(this);
      if ( $this.parents('.nf-field-container').find('.nf-input-limit').length ) {
        $this.parents('.nf-field-container').find('.nf-input-limit').css('display', 'none');
        // $this.parents('.nf-field-container').find('.nf-input-limit').addClass('es-input-limit');
      }
    });

    $('.ninja-forms-field[maxlength]').on('keyup', function(e) {
        var $this = $(this),
       $this_value = $this.val();

      if ( $this.parents('.nf-field-container').find('.nf-input-limit').length && !($this.parents('.field-wrap').hasClass('nf-fail'))) {
        $this.parents('.nf-field-container').find('.nf-input-limit').css('display', 'block');
        $this.parents('.nf-field-container').css('margin-bottom', '7px');

      }
    });

    $('.ninja-forms-field[maxlength]').on('keydown keyup change', function(e) {
        var $this = $(this),
       $this_value = $this.val();

       if ( e.which == 8 && $this_value == '' ) {
         $this.parents('.nf-field-container').find('.nf-input-limit').css('display', 'none');
         $this.parents('.nf-field-container').css('margin-bottom', '25px');
         console.log('It has .nf-fail and key: ' + e.which);
       }

      if ( /*($this.parents('.field-wrap').hasClass('nf-fail')) &&*/ ($this_value.length < 1 &&  e.which == 8)) {
        // $this.parents('.nf-field-container').find('.nf-input-limit').css('display', 'none');
        $this.parents('.nf-field-container').css('margin-bottom', '7px');
      }
    });

  };

  $(window).load(function() {
    $es_form_field_validate_error_msg();

    $('.es-form-input-wrap-radio-address').find('input[type=radio]').on('change', function(e) {
      var $this = $(this);
      if ($this.val() === 'Si') {

      }

      $es_form_field_validate_error_msg();

    });
  });





});