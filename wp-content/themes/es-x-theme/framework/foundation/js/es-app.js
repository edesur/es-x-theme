jQuery(document).ready(function($) {

// Add required attribute to top nav
$('.es-site-top-nav-search').find('input').attr('required', 'required');

$('.entry-404').find('input').attr('required', 'required');
//
var es_reveal_es_transparencia_doc = (function () {

  $('.button[data-toggle]').on('click', function(event) {
    event.preventDefault();

     var $this = $(this),
     $es_transparencia_doc_url  = $this.attr('href'),
     $es_transparencia_doc_url_download  = $this.parents('.es-transparencia-doc-single').find('.es-transparencia-file-tools').find('.es-doc-tools-btn-down').attr('href'),
     $es_transparencia_doc_mime_type  = $this.attr('type'),
     $es_transparencia_doc_file_id  = $this.attr('data-toggle'),
     $es_transparencia_doc_download_icon = $this.parents('.es-transparencia-doc-single').find('.es-transparencia-doc-file-icon').find('i').attr('class'),
     $es_transparencia_doc_download_icon_title = $this.parents('.es-transparencia-doc-single').find('.es-transparencia-doc-file-icon').find('i').attr('title');

     if ($this.parents('.es-transparencia-datos-abiertos-file').length) {
        var $es_transparencia_doc_title = $this.parents('.es-transparencia-datos-abiertos-file').find('.es-transparencia-doc-copy-title.h6').text(),
        $es_transparencia_doc_url_download  = $this.parents('.es-transparencia-datos-abiertos-file').find('.es-transparencia-file-tools').find('.es-doc-tools-btn-down').attr('href'),
                $es_transparencia_doc_desc = $this.parents('.es-transparencia-datos-abiertos-file').find('.es-transparencia-doc-file-desc').text(),
             $es_transparencia_doc_file_meta_type = $this.parents('.es-transparencia-datos-abiertos-file').find('.es-transparencia-doc-file-meta-copy').find('.es-transparencia-doc-file-meta-type').text(),
             $es_transparencia_doc_file_meta_size = $this.parents('.es-transparencia-datos-abiertos-file').find('.es-transparencia-doc-file-meta-copy').find('.es-transparencia-doc-file-meta-size').text();

     } else {
      var $es_transparencia_doc_title = $this.parents('.es-transparencia-doc-single').find('.es-transparencia-doc-copy-title').text(),
              $es_transparencia_doc_desc = $this.parents('.es-transparencia-doc-single').find('.es-transparencia-doc-copy-desc').text(),
             $es_transparencia_doc_file_meta_type = $this.parents('.es-transparencia-doc-single').find('.es-transparencia-doc-file-meta-copy').find('.es-transparencia-doc-file-meta-type').text(),
             $es_transparencia_doc_file_meta_size = $this.parents('.es-transparencia-doc-single').find('.es-transparencia-doc-file-meta-copy').find('.es-transparencia-doc-file-meta-size').text();
     }

    console.log( 'File Type: ' + $es_transparencia_doc_file_meta_type);
    console.log( 'File size: ' + $es_transparencia_doc_file_meta_size);

    $('.es-transparencia-doc-reveal').find('.es-transparencia-doc-copy-title').text($es_transparencia_doc_title);
    $('.es-transparencia-doc-reveal').find('.es-transparencia-doc-copy-desc').text($es_transparencia_doc_desc);
    $('.es-transparencia-doc-reveal').find('.es-transparencia-doc-file-icon').find('i').addClass($es_transparencia_doc_download_icon);
    $('.es-transparencia-doc-reveal').find('.es-transparencia-doc-file-icon').find('i').attr('title', $es_transparencia_doc_download_icon_title);
    $('.es-transparencia-doc-reveal').find('.es-transparencia-doc-file-meta-copy').find('.es-transparencia-doc-file-meta-type').text($es_transparencia_doc_file_meta_type);
    $('.es-transparencia-doc-reveal').find('.es-transparencia-doc-file-meta-copy').find('.es-transparencia-doc-file-meta-size').text($es_transparencia_doc_file_meta_size);

    $('.es-transparencia-doc-reveal').attr('id', $(this).attr('data-toggle'));
    $('.es-transparencia-doc-reveal').find('.es-transparencia-doc-tools-btn').attr('title', 'Descargar: ' + $es_transparencia_doc_title);

    $('.es-transparencia-doc-reveal').find('.es-transparencia-doc-tools-btn').attr('href', $es_transparencia_doc_url_download) ;

     console.log('Doc icon: ' + $es_transparencia_doc_download_icon);

    console.log('MIME type: ' + $es_transparencia_doc_url_download);

    if ( $es_transparencia_doc_mime_type == 'application/vnd.oasis.opendocument.spreadsheet' ||
           $es_transparencia_doc_mime_type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
           $es_transparencia_doc_mime_type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ||
           $es_transparencia_doc_mime_type == 'application/vnd.ms-excel' ||
           $es_transparencia_doc_mime_type == 'application/vnd.ms-excel.sheet.macroEnabled.12' ||
           $es_transparencia_doc_mime_type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
           $es_transparencia_doc_mime_type == 'application/msword') {

        $('#es_transparencia_doc_reveal_iframe').attr('src', '//view.officeapps.live.com/op/view.aspx?src=' + $es_transparencia_doc_url);

        //     $.ajax({
        //             type: "get",
        //             url: "//view.officeapps.live.com/op/view.aspx?src=" + $es_transparencia_doc_url,
        //             data: "data",
        //             //     dataType: "dataType",
        //             done: function (response) {
        //                     console.log("Ajax: " + response);
        //             }
        //     });


    } else if ( $es_transparencia_doc_mime_type == 'text/csv' ) {
            $('#es_transparencia_doc_reveal_iframe').attr('src', '//sheet.zoho.com/sheet/view.do?url=' + $es_transparencia_doc_url);
    } else {
        $('#es_transparencia_doc_reveal_iframe').attr('src', '//docs.google.com/viewer?url=' + $es_transparencia_doc_url + '&embedded=true');
     }

    $('#es_transparencia_doc_reveal_iframe').on('load', function () {
      console.log('iFrame loaded');
    });

  });

  $('.es-transparencia-doc-reveal').on('closeme.zf.reveal', function (event) {
      var $this = $(this);

      console.log('Opening the Modal');

    });

  $('.es-transparencia-doc-reveal').on('closeme.zf.reveal', function (event) {
    var $this = $(this);

    console.log('Modal opened');

  });

  $('.es-transparencia-doc-reveal').on('closed.zf.reveal', function(event) {
    var $this = $(this);

    // $(this).find('.es-transparencia-doc-copy-title').text();
    // $(this).find('.button[data-toggle]').attr('href');

    $this.find('.es-transparencia-doc-copy-title').text('');
    $this.find('.es-transparencia-doc-copy-desc').text('');
    $this.find('.es-transparencia-doc-tools-btn').removeAttr('href');
    $this.find('.es-transparencia-doc-tools-btn').removeAttr('title');

    $this.find('.es-transparencia-doc-file-icon').find('i').removeAttr('class');
    $this.find('.es-transparencia-doc-file-icon').find('i').removeAttr('title');
    $this.find('.es-transparencia-doc-file-meta-copy').find('.es-transparencia-doc-file-meta-type').text('');
    $this.find('.es-transparencia-doc-file-meta-copy').find('.es-transparencia-doc-file-meta-size').text('');

    $('#es_transparencia_doc_reveal_iframe').removeAttr('src');

    // console.log( 'This Reveal title cleared: ' + $(this).find('.es-transparencia-doc-copy-title').text()  );

    // console.log( 'This Download link cleared: ' + $(this).find('.es-transparencia-doc-tools-btn').attr('href')  );

    // console.log( 'iframe URL cleared  ' + $('#es_transparencia_doc_reveal_iframe').attr('src') );

  });

})();

$('.x-off-canvas-content.x-off-canvas-content-right').css({
  // 'top':'128px',
  // 'width': '80%'
});

$(window).scroll(function(event) {
  /* Act on the event */
  $header_bot_pos = $('.x-masthead').outerHeight(true) - $(this).scrollTop();

  /*if ($('.es-off-canvas-sticky.es-site-top-nav-reponsive.x-off-canvas.x-off-canvas-right.x-active').is(':visible') ) {
    $('.es-off-canvas-sticky.es-site-top-nav-reponsive.x-off-canvas.x-off-canvas-right.x-active').css({
       'top': $('.x-bar-absolute x-bar-is-sticky.es-site-top-nav-reponsive-sticky').outerHeight(true) + $(this).scrollTop(),
       'position': 'absolute'
     });
    // $('.es-site-top-nav-reponsive').find('.x-off-canvas-content.x-off-canvas-content-right').css('top', $('.x-masthead').outerHeight(true) - $(this).scrollTop());
  } else {
    $('.es-site-top-nav-reponsive.x-off-canvas.x-off-canvas-right.x-active').css({
       'top': $('.x-masthead').outerHeight(true) - $(this).scrollTop()*,
       'position': 'absolute'
     });
  }*/

  $('.es-site-top-nav-reponsive.x-off-canvas.x-off-canvas-right.x-active').css({
       // 'top': $('.x-masthead').outerHeight(true) - $(this).scrollTop()
       'top': $('.x-masthead').outerHeight(true) - $(this).scrollTop()
     });

  if ($(this).scrollTop() > 0 && $(this).scrollTop() <= 128 ) {
    $('.es-site-top-nav-reponsive.x-off-canvas.x-off-canvas-right.x-active').css({
         'position': 'fixed',
          'top': $('.x-masthead').outerHeight(true) - $(this).scrollTop()
       });

  } else if ($(this).scrollTop() > 128) {

    $('.es-site-top-nav-reponsive.x-off-canvas.x-off-canvas-right.x-active').css({
         'position': 'absolute',
          'top': $('.x-masthead').outerHeight(true)
       });
  } else if ($(this).scrollTop() < 1) {
    $('.es-site-top-nav-reponsive.x-off-canvas.x-off-canvas-right.x-active').css({
          'position': 'fixed',
          'top': $('.x-masthead').outerHeight(true)
       });

      // console.log('Height: ' +  $('.x-masthead').outerHeight(true) );
  }





  // console.log( $('.x-bar-absolute x-bar-is-sticky.es-site-top-nav-reponsive-sticky').outerHeight(true) + $(this).scrollTop());
  // console.log('Scroll top: '  + $(this).scrollTop() );
  // console.log( $('.x-masthead').outerHeight(true) - $(this).scrollTop() );


// && $('.es-top-nav-tools-responsive').hasClass('x-bar-fixed')

  if ($header_bot_pos >= 48 ) {
    // console.log(jQuery('.x-masthead').outerHeight(true) - $(this).scrollTop());
    // $('.x-off-canvas-content.x-off-canvas-content-right').css('top', jQuery('.x-masthead').outerHeight(true) - $(this).scrollTop());
  }


});

 $('.rmp-feedback-button-div').find('button').addClass('x-btn');

// console.log( 'Loaded  URL: ' + $('#es_transparencia_doc_reveal_iframe').attr('src') );

// console.log( 'Modal count: ' + $('.es-transparencia-doc-reveal').length );

});