// =============================================================================
// TABLE OF CONTENTS
// -----------------------------------------------------------------------------
//   01. Custom Functionality
// =============================================================================

// Custom Functionality
// =============================================================================

jQuery(document).ready(function($) {

  (function es_site_form_field_req_validate() {
    var $es_site_form = $('.cred-form'),
      $es_site_form_field_req = $es_site_form.find('[class^=wpt-form-text]'),
      $es_site_form_field_req_error_msg = function ($required_field) {

        var $error_text = 'Caracter no permitido',
          $es_site_form_field_req_error = $('<small></small>', {
          html: $error_text
          });

            $required_field.on('keypress', function(e) {
             if ( e.which == 36 || e.which == 37 || e.which == 40 || e.which == 41 || e.which == 45 || e.which == 60 || e.which == 62 || e.which == 63 || e.which == 91 || e.which == 92 || e.which == 93 || e.which == 123 || e.which == 124 || e.which == 125 ) {

              $(this).parent().addClass('wpt-form-error');
              $(this).after($es_site_form_field_req_error);
               return false;
             } else {
              $(this).parent().removeClass('wpt-form-error');
             }
           });

            $required_field.on('blur', function(e) {
              if ($(this).val() == '' && !($(this).val().match(/([<>&[\]\\{}%])/g))) {
                $(this).parent().removeClass('wpt-form-error');
                $es_site_form_field_req_error.remove();
              } else {
              $(this).parent().removeClass('wpt-form-error');
              $(this).val() == ''
             }

              if ($(this).val().match(/([<>&[\]\\{}%])/g)) {
                $(this).parent().addClass('wpt-form-error');
                $(this).after($es_site_form_field_req_error);
              }
            });

            $required_field.on('focusin', function(e) {
              if (($(this).val().match(/([<>&[\]\\{}%])/g))) {
              $(this).val('');
              $(this).parent().removeClass('wpt-form-error');
              $es_site_form_field_req_error.remove();
             }
            });
       };
        $es_site_form_field_req_error_msg( $es_site_form_field_req);
  })();

  (function es_site_add_attr_to_social_cards_btns() {

    if ($('.es-site-page-redes-sociales').length) {
      // console.log($('.es-site-page-redes-sociales').find('.x-face-button').length);
      $('.es-site-page-redes-sociales').find('.x-face-button').attr('target', '_blank');
    }

  })();

});