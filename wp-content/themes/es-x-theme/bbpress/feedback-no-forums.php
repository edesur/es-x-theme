<?php

/**
 * No Forums Feedback Part
 *
 * @package bbPress
 * @subpackage Theme
 */

?>

<div class="bbp-template-notice">
	<p><?php _e( 'No se encontraron foros', 'bbpress' ); ?></p>
</div>
