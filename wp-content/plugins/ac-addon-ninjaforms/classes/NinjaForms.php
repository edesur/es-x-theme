<?php

namespace ACA\NF;

use AC;

/**
 * @since 1.0
 */
final class NinjaForms extends AC\Plugin {

	/**
	 * @var string
	 */
	protected $file;

	public function __construct( $file ) {
		$this->file = $file;
	}

	protected function get_file() {
		return $this->file;
	}

	/**
	 * Register hooks
	 */
	public function register() {
		add_action( 'ac/column_groups', array( $this, 'register_column_group' ) );
		add_action( 'ac/list_screens', array( $this, 'register_list_screen' ) );
		add_action( 'ac/list_screen_groups', array( $this, 'register_list_screen_group' ) );
		add_action( 'acp/column_types', array( $this, 'register_columns' ) );
		add_action( 'acp/editing/saved', array( $this, 'add_form_id_to_ajax_save' ) );

		$table = new TableScreen( $this->get_version(), $this->get_url() );
		$table->register();
	}

	protected function get_version_key() {
		return 'aca_ninja_forms';
	}

	/**
	 * The 'ninja_forms_custom_columns' ( which display the column value ) depends on $_GET['form_id'] to be set.
	 *
	 * @param AC\Column $column
	 */
	public function add_form_id_to_ajax_save( $column ) {
		$list_screen = $column->get_list_screen();

		if ( ! $list_screen instanceof ListScreen\Submission ) {
			return;
		}

		$_GET['form_id'] = $list_screen->get_form_id();
	}

	/**
	 * @param string $type Field type (Textbox, Email, Textarea etc.)
	 *                     Full list in ninja-forms/includes/Fields
	 *
	 * @return AC\Column|false
	 */
	private function get_column_by_type( $type ) {

		if ( 'list' === $type ) {
			$type = 'lists';
		}

		$class_name = 'ACA\NF\Column\Submission\\' . ucfirst( $type );

		if ( ! class_exists( $class_name ) ) {
			return false;
		}

		return new $class_name;
	}

	/**
	 * @param AC\ListScreen $list_screen
	 */
	public function register_columns( $list_screen ) {
		if ( ! $list_screen instanceof ListScreen\Submission ) {
			return;
		}

		$form = Ninja_Forms()->form( $list_screen->get_form_id() );

		// Ninja Forms default headings
		foreach ( $list_screen->get_stored_default_headings() as $name => $label ) {

			// skip checkbox
			if ( 'cb' === $name ) {
				continue;
			}

			// register publish date
			if ( 'sub_date' === $name ) {
				$list_screen->register_column_type( new Column\Submission\DatePublished );
				continue;
			}

			// register Id
			if ( 'id' === $name ) {
				$list_screen->register_column_type( new Column\Submission\Id );
				continue;
			}

			$field = $form->get_field( $name );

			if ( $column = $this->get_column_by_type( $field->get_setting( 'type' ) ) ) {
				$column
					->set_type( $name )
					->set_label( $label );

				$list_screen->register_column_type( $column );
			}
		}

	}

	/**
	 * @param AC\Groups $groups
	 */
	public function register_list_screen_group( $groups ) {
		$groups->register_group( 'ninjaforms', __( 'Ninja Forms', 'codepress-admin-columns' ), 8 );
	}

	/**
	 * @param AC\Groups $groups
	 */
	public function register_column_group( $groups ) {
		$groups->register_group( 'ninjaforms', __( 'Ninja Forms', 'codepress-admin-columns' ), 11 );
	}

	public function register_list_screen() {

		if ( $forms = Ninja_Forms()->form()->get_forms() ) {

			/* @var \NF_Database_Models_Form $form */
			foreach ( $forms as $form ) {
				AC()->register_list_screen( new ListScreen\Submission( $form->get_id() ) );
			}
		}
	}

}