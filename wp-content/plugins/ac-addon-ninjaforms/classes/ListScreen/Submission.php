<?php

namespace ACA\NF\ListScreen;

use ACP;

class Submission extends ACP\ListScreen\Post {

	/**
	 * @var int $form_id Ninja Forms Form ID
	 */
	private $form_id;

	/**
	 * @var \NF_Abstracts_Model
	 */
	private $form;

	public function __construct( $form_id ) {
		parent::__construct( 'nf_sub' );

		$this->set_form_id( $form_id );
		$this->set_group( 'ninjaforms' );
		$this->set_key( $this->get_key() . '_' . $form_id );
	}

	private function set_form_id( $form_id ) {
		$this->form_id = $form_id;
	}

	/**
	 * @return string
	 */
	public function get_label() {
		return $this->get_form()->get_setting( 'title' );
	}

	/**
	 * @return string
	 */
	public function get_singular_label() {
		return $this->get_label();
	}

	/**
	 * @return int
	 */
	public function get_form_id() {
		return $this->form_id;
	}

	/**
	 * Display column value through Ninja Form Hooks
	 */
	public function set_manage_value_callback() {
		add_filter( 'ninja_forms_custom_columns', array( $this, 'change_columns_value' ), 10, 3 );
		add_filter( 'nf_sub_table_seq_num', array( $this, 'change_id_column_value' ), 10, 2 );
	}

	/**
	 * @return bool
	 */
	public function is_current_screen( $wp_screen ) {
		return parent::is_current_screen( $wp_screen ) && $this->get_form_id() === (int) filter_input( INPUT_GET, 'form_id' );
	}

	/**
	 * @return string Url
	 */
	/**
	 * @return string Url
	 */
	public function get_screen_link() {
		return add_query_arg( array( 'form_id' => $this->get_form_id() ), parent::get_screen_link() );
	}

	/**
	 * @param                           $value
	 * @param \NF_Database_Models_Field $field
	 */
	public function change_columns_value( $original_value, $field, $sub_id ) {
		$value = $this->get_display_value_by_column_name( $field->get_id(), $sub_id );

		if ( ! $value ) {
			return $original_value;
		}

		return $value;
	}

	/**
	 * @param                           $value
	 * @param \NF_Database_Models_Field $field
	 */
	public function change_id_column_value( $original_value, $sub_id ) {
		$value = $this->get_display_value_by_column_name( 'id', $sub_id );

		if ( ! $value ) {
			return $original_value;
		}

		return $value;
	}

	/**
	 * Set Ninja Forms NF_Abstracts_Model object
	 */
	private function set_form() {
		$this->form = Ninja_Forms()->form( $this->get_form_id() )->get();
	}

	/**
	 * @return \NF_Abstracts_Model
	 */
	private function get_form() {
		if ( null === $this->form ) {
			$this->set_form();
		}

		return $this->form;
	}

}