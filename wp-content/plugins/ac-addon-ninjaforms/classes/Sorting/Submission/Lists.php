<?php

namespace ACA\NF\Sorting\Submission;

use ACA\NF\Column;
use ACP;

/**
 * @property Column\Submission\Lists $column
 */
class Lists extends ACP\Sorting\Model {

	public function __construct( Column\Submission $column ) {
		parent::__construct( $column );
	}

	public function get_sorting_vars() {
		return array(
			'ids' => $this->get_sorted_ids(),
		);
	}

	/**
	 * @return array
	 */
	public function get_sorted_ids() {
		$return_posts = array();

		foreach ( $posts = $this->strategy->get_results() as $post_id ) {
			$meta_value = (array) get_post_meta( $post_id, $this->column->get_meta_key(), true );
			$return_posts[ $post_id ] = $meta_value;

			if ( ! empty( $meta_value ) && $label = $this->column->get_choice_label( $meta_value ) ) {
				$return_posts[ $post_id ] = $label[0];
			}

		}

		asort( $return_posts );

		$ids = array_keys( $return_posts );

		if ( 'DESC' === $this->get_order() ) {
			$ids = array_reverse( $ids );
		}

		return $ids;
	}

}