<?php

namespace ACA\NF\Search\Submission;

use AC;
use ACP\Search;
use ACP\Search\Operators;

class Lists extends Search\Comparison\Meta
	implements Search\Comparison\Values {

	/** @var array */
	private $options;

	public function __construct( $meta_key, $options ) {
		$this->options = $options;

		$operators = new Operators( array(
			Operators::EQ,
			Operators::NEQ,
			Operators::IS_EMPTY,
			Operators::NOT_IS_EMPTY,
		) );

		parent::__construct( $operators, $meta_key, 'post', Search\Value::STRING );
	}

	public function get_values() {
		return AC\Helper\Select\Options::create_from_array( $this->options );
	}

}