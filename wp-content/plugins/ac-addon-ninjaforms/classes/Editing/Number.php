<?php

namespace ACA\NF\Editing;

use ACA\NF\Column;
use ACA\NF\Editing;

/**
 * @property Column\Submission $column
 */
class Number extends Editing {

	public function get_view_settings() {
		$data = parent::get_view_settings();

		$data['type'] = 'number';

		return $data;
	}

}