<?php

namespace ACA\NF\Editing;

use ACA\NF\Column;
use ACA\NF\Editing;

/**
 * @property Column\Submission $column
 */
class Checkbox extends Editing {

	public function get_view_settings() {
		$data = parent::get_view_settings();

		$data['type'] = 'togglable';
		$data['options'] = array(
			'0' => __( 'False', 'codepress-admin-columns' ),
			'1' => __( 'True', 'codepress-admin-columns' ),
		);

		return $data;
	}

}