<?php

namespace ACA\NF\Filtering;

use ACA\NF\Column;
use ACP;

/**
 * @property Column\Submission\Lists $column
 */
class Lists extends ACP\Filtering\Model\Meta {

	public function get_filtering_data() {
		if ( $this->column->is_serialized() ) {
			$values = $this->get_meta_values_unserialized();
		} else {
			$values = $this->get_meta_values();
		}
		$options = array();

		$choices = (array) $this->column->get_field_choices();

		foreach ( $values as $value ) {
			if ( $choices && isset( $choices[ $value ] ) ) {
				$options[ $value ] = $choices[ $value ];
			}
		}

		return array(
			'empty_option' => true,
			'options'      => $options,
		);
	}

	public function get_filtering_vars( $vars ) {
		$value = $this->get_filter_value();

		if ( in_array( $value, array( 'cpac_empty', 'cpac_nonempty' ) ) ) {
			return $this->get_filtering_vars_empty_nonempty( $vars );
		}

		// Exact, is also used for single values in multiselect fields in Ninja Forms
		$vars['meta_query'][ $this->column->get_meta_key() ][] = array(
			'key'   => $this->column->get_meta_key(),
			'value' => $value,
			'type'  => $this->get_data_type(),
		);

		if ( $this->column->is_serialized() ) {

			$vars['meta_query'][ $this->column->get_meta_key() ]['relation'] = 'OR';
			$vars['meta_query'][ $this->column->get_meta_key() ][] = array(
				'key'     => $this->column->get_meta_key(),
				'value'   => serialize( $value ),
				'compare' => 'LIKE',
			);
		}

		return $vars;
	}

}