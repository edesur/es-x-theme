<?php

namespace ACA\NF\Filtering;

use ACA\NF\Column;
use ACP;

/**
 * @property Column\Submission $column
 */
class Checkbox extends ACP\Filtering\Model\Meta {

	public function get_filtering_data() {
		$data = array();

		$values = $this->get_meta_values();
		$data['options'][0] = __( 'False', 'codepress-admin-columns' );
		foreach ( $values as $value ) {
			if ( 0 !== $value ) {
				$data['options'][ $value ] = __( 'True', 'codepress-admin-columns' );
			}
		}

		return $data;
	}

}