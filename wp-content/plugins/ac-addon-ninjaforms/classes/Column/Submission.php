<?php

namespace ACA\NF\Column;

use ACA\NF\ListScreen;
use AC;
use ACP;

class Submission extends AC\Column\Meta
	implements ACP\Sorting\Sortable, ACP\Filtering\Filterable, ACP\Editing\Editable, ACP\Search\Searchable {

	public function __construct() {
		$this->set_group( 'ninjaforms' );
		$this->set_original( true );
	}

	public function get_meta_key() {
		return '_field_' . $this->get_name();
	}

	/**
	 * @param int $id Submission ID
	 *
	 * @return string
	 */
	public function get_value( $id ) {
		return null;
	}

	public function get_raw_value( $id ) {
		$sub = $this->get_form()->get_sub( $id );

		return $sub->get_field_value( $this->get_name() );
	}

	public function apply_conditional() {
		return 'nf_sub' === $this->get_post_type();
	}

	/**
	 * @return \NF_Abstracts_ModelFactory
	 */
	public function get_form() {
		/* @var ListScreen\Submission $list_screen */
		$list_screen = $this->get_list_screen();

		return Ninja_Forms()->form( $list_screen->get_form_id() );
	}

	/**
	 * @return string
	 */
	public function get_field_id() {
		return $this->get_name();
	}

	/**
	 * @return \NF_Database_Models_Field
	 */
	public function get_field() {
		return $this->get_form()->get_field( $this->get_field_id() );
	}

	/**
	 * @param string $setting
	 *
	 * @return bool|int|string
	 */
	public function get_field_setting( $setting ) {
		return $this->get_field()->get_setting( $setting );
	}

	public function sorting() {
		return new ACP\Sorting\Model\Meta( $this );
	}

	public function filtering() {
		return new ACP\Filtering\Model\Disabled( $this );
	}

	public function editing() {
		return new ACP\Editing\Model\Disabled( $this );
	}

	public function search() {
		return false;
	}

}