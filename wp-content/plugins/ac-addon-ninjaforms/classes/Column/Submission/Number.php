<?php

namespace ACA\NF\Column\Submission;

use ACA\NF\Column\Submission;
use ACA\NF\Editing;
use ACP;
use ACP\Search;

class Number extends Submission {

	public function sorting() {
		$model = new ACP\Sorting\Model\Meta( $this );
		$model->set_data_type( 'numeric' );

		return $model;
	}

	public function filtering() {
		$model = new ACP\Filtering\Model\Meta( $this );
		$model->set_ranged( true );
		$model->set_data_type( 'numeric' );

		return $model;
	}

	public function editing() {
		return new Editing\Number( $this );
	}

	public function search() {
		return new Search\Comparison\Meta\Numeric( $this->get_meta_key(), $this->get_meta_type() );
	}

}