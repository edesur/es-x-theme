<?php

namespace ACA\NF\Column\Submission;

use ACA\NF\Column\Submission;
use ACP;

class Date extends Submission {

	public function sorting() {
		$model = new ACP\Sorting\Model\Meta( $this );
		$model->set_data_type( 'date' );

		return $model;
	}

}