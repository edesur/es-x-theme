<?php

namespace ACA\NF\Column\Submission;

use ACA\NF\Column\Submission;
use ACA\NF\Editing;
use ACP;

class Product extends Submission {

	public function sorting() {
		$model = new ACP\Sorting\Model\Meta( $this );
		$model->set_data_type( 'numeric' );

		return $model;
	}

	public function filtering() {
		$model = new ACP\Filtering\Model\Meta( $this );
		$model->set_ranged( true );
		$model->set_data_type( 'numeric' );

		return $model;
	}

	public function editing() {
		return new Editing\Number( $this );
	}

}