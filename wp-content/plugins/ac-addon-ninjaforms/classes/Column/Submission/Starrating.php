<?php

namespace ACA\NF\Column\Submission;

use ACA\NF\Column\Submission;
use ACP\Filtering;
use ACP\Search;
use ACP\Sorting;

class Starrating extends Submission {

	public function sorting() {
		$model = new Sorting\Model\Meta( $this );
		$model->set_data_type( 'numeric' );

		return $model;
	}

	public function filtering() {
		$model = new Filtering\Model\Meta( $this );
		$model->set_ranged( true );
		$model->set_data_type( 'numeric' );

		return $model;
	}

	public function search() {
		return new Search\Comparison\Meta\Numeric( $this->get_meta_key(), $this->get_meta_type() );
	}

}