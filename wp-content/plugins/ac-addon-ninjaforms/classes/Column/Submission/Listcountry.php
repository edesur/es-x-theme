<?php

namespace ACA\NF\Column\Submission;

class Listcountry extends Lists {

	protected function set_field_choices() {
		$this->choices = array_flip( Ninja_Forms()->config( 'CountryList' ) );
	}

}