<?php

namespace ACA\NF\Column\Submission;

use ACP;

class Address extends Textbox {

	public function editing() {
		return new ACP\Editing\Model\Meta( $this );
	}

}