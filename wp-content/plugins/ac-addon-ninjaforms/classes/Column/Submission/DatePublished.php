<?php

namespace ACA\NF\Column\Submission;

use ACA\NF\Column\Submission;
use ACA\NF\Filtering;
use ACP\Sorting;
use ACP\Search;

class DatePublished extends Submission {

	public function __construct() {
		parent::__construct();

		$this->set_type( 'sub_date' );
	}

	public function sorting() {
		return new Sorting\Model\Disabled( $this );
	}

	public function filtering() {
		return new Filtering\Date( $this );
	}

	public function search() {
		return new Search\Comparison\Post\Date\PostDate();
	}

}