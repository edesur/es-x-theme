<?php

namespace ACA\NF\Column\Submission;

use ACA\NF\Column\Submission;
use ACP;
use ACP\Search;

class City extends Submission {

	public function filtering() {
		return new ACP\Filtering\Model\Meta( $this );
	}

	public function editing() {
		return new ACP\Editing\Model\Meta( $this );
	}

	public function search() {
		return new Search\Comparison\Meta\Text( $this->get_meta_key(), $this->get_meta_type() );
	}

}