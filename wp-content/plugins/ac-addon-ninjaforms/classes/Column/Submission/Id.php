<?php

namespace ACA\NF\Column\Submission;

use ACA\NF\Column\Submission;
use ACP;
use ACP\Search;

class Id extends Submission {

	public function __construct() {
		parent::__construct();
		$this->set_type( 'id' );
	}

	public function get_meta_key() {
		return '_seq_num';
	}

	public function sorting() {
		$model = new ACP\Sorting\Model\Meta( $this );
		$model->set_data_type( 'numeric' );

		return $model;
	}

	public function filtering() {
		$model = new ACP\Filtering\Model\Meta( $this );
		$model->set_ranged( true );
		$model->set_data_type( 'numeric' );

		return $model;
	}

	public function search() {
		return new Search\Comparison\Meta\Numeric( $this->get_meta_key(), $this->get_meta_type() );
	}

}