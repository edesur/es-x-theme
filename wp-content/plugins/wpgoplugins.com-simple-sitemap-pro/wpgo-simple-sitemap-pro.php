<?php
/*
Plugin Name: Simple Sitemap Pro
Plugin URI: http://www.wpgoplugins.com
Description: An effective way to display a sitemap of content on your site. Rendered sitemap is fully responsive and looks great on all devices!
Version: 1.4
Author: David Gwyer
Author URI: http://www.wpgoplugins.com
*/

/*  Copyright 2009 David Gwyer (email : david@wpgothemes.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// Main Plugin class definition
class WPGO_Simple_Sitemap_Pro {

	/* Class properties. */
	protected $_plugin_options_class;
	protected $_edd_updater_manager_class;
	protected $_thumb_size = 22; // pixels

	/* Main class constructor. */
	public function __construct() {

		$this->constants(); // define framework constants
		$this->bootstrap();	// load plugin classes

		/* Setup the plugin text domain and .mo file for translation. */
		add_action( 'plugins_loaded', array( &$this, 'localize_plugin' ) );
		add_shortcode( 'simple-sitemap-group', array( &$this, 'render_sitemap_group' ) );
		add_shortcode( 'simple-sitemap', array( &$this, 'render_sitemap' ) );
		add_shortcode( 'simple-sitemap-tax', array( &$this, 'render_sitemap_taxonomy' ) );
		add_shortcode( 'simple-sitemap-child', array( &$this, 'render_sitemap_child' ) );
		add_filter( 'widget_text', 'do_shortcode' ); // make sitemap shortcode work in text widgets
	}

	/**
	 * Defines the framework constants.
	 *
	 * @since 0.1.0
	 */
	public function constants() {

		/* Define main plugin name constants. */
		define( "WPGO_SIMPLE_SITEMAP_PRO_NAME", "Simple Sitemap Pro" );
		define( "WPGO_SIMPLE_SITEMAP_PRO_NAME_U", strtolower( str_replace( " ", "_", WPGO_SIMPLE_SITEMAP_PRO_NAME ) ) ); // underscored lower case plugin name
		define( "WPGO_SIMPLE_SITEMAP_PRO_NAME_H", strtolower( str_replace( " ", "-", WPGO_SIMPLE_SITEMAP_PRO_NAME ) ) ); // hyphenated lower case plugin name

		/* Define plugin options constants. */
		define( "WPGO_SIMPLE_SITEMAP_PRO_OPTIONS_DB_NAME", WPGO_SIMPLE_SITEMAP_PRO_NAME_U . "_plugin_options" );
		define( "WPGO_SIMPLE_SITEMAP_PRO_CUSTOMIZE_DB_NAME", WPGO_SIMPLE_SITEMAP_PRO_NAME_U . "_customize_options" );
	}

	/**
	 * Defines the framework constants.
	 *
	 * @since 0.1.0
	 */
	public function bootstrap() {

		// Load classes/resources
		if ( ! class_exists( 'EDD_Plugin_Updater_Manager' ) ) // this could be defined in other plugins too
			require_once( plugin_dir_path( __FILE__ ) . 'classes/updater/EDD_Plugin_Updater_Manager.php' );
		require_once( plugin_dir_path( __FILE__ ) . 'classes/hooks.php' );
		require_once( plugin_dir_path( __FILE__ ) . 'classes/plugin-options.php' );
		require_once( plugin_dir_path( __FILE__ ) . 'classes/class-wpgo-walker-page.php' );

		$plugin_options_args = array(
			'plugin_name' => WPGO_SIMPLE_SITEMAP_PRO_NAME,
			'options_group' => WPGO_SIMPLE_SITEMAP_PRO_NAME_U . "_plugin_options_group",
			'menu_slug' => WPGO_SIMPLE_SITEMAP_PRO_NAME_U . "_admin_options_menu",
			'options_section' => 'simple_sitemap_pro_default',
			'options_db_name' => WPGO_SIMPLE_SITEMAP_PRO_OPTIONS_DB_NAME, // use this where possible, i.e. in non-static methods
			'plugin_root' => __FILE__, // don't edit this
		);

		$update_manager_args = array(
			'edd_store_url'				=> 'http://www.wpgoplugins.com',
			'edd_item_name'				=> '', //Plugin Name',
			'edd_item_id'               => '805',
			'page_title'				=> 'Simple Sitemap Pro Options Page',
			'menu_title'				=> 'Simple Sitemap Pro',
			'options_page_title'		=> 'Simple Sitemap Pro Plugin',
			'license_key_status_expiry'	=> DAY_IN_SECONDS,
			'plugin_root'   			=> __FILE__, // don't edit this
			'plugin_name_slug'          => WPGO_SIMPLE_SITEMAP_PRO_NAME_H, // plugin name slug label (used mainly in options page)
			'plugin_dir_path'           => plugin_dir_path( __FILE__ ),
			'options_group'             => WPGO_SIMPLE_SITEMAP_PRO_NAME_U . "_plugin_options_group",
			'menu_slug'                 => WPGO_SIMPLE_SITEMAP_PRO_NAME_U . "_admin_options_menu",
			'edd_auto_updates_option'   => 'edd_auto_updates_simple_sitemap_pro_option',
			'options_section'           => 'simple_sitemap_pro_default'
		);

		// Instantiate plugin classes
		$this->_plugin_options_class        = new WPGO_Simple_Sitemap_Pro_Options($plugin_options_args);
		$this->_edd_updater_manager_class	= new EDD_Plugin_Updater_Manager($update_manager_args);
	}

	/**
	 * Add Plugin localization support.
	 *
	 * @since 0.2.0
	 */
	public function localize_plugin() {

		load_plugin_textdomain( 'wpgo-simple-sitemap-pro', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
	}

	public function get_query_args($args, $post_type) {

		// convert comma separated string into array (array required in post query)
		if ( empty( $args['exclude'] ) ) {
			$args['exclude'] = array();
		} else {
			$args['exclude'] = array_map( 'trim', explode( ',', $args['exclude']) );
		}

		if ( empty( $args['include'] ) ) {
			$args['include'] = array();
		} else {
			$args['include'] = array_map( 'trim', explode( ',', $args['include']) );
		}

		$args['tax_query'] = empty( $args['tax_query'] ) ? '' : $args['tax_query'];

		return array(
			'posts_per_page' => -1,
			'post_type' => $post_type,
			'order' => $args['order'],
			'orderby' => $args['orderby'],
			'post__not_in' => $args['exclude'],
			'post__in' => $args['include'],
			'tax_query' => $args['tax_query']
		);
	}

	/**
	 * Render the sitemap list items.
	 *
	 * @since 0.2.0
	 */
	public function render_list_items($args, $post_type, $query_args) {

		$args['image_size'] = $this->_thumb_size;
		$sitemap_query = new WP_Query( $query_args );

		if ( $sitemap_query->have_posts() ) :

			if( $post_type == 'page') :
				echo $this->list_pages( $sitemap_query->posts, $args );

			else :
				$horizontal_sep = "";
				$ul_class = 'simple-sitemap-' . $post_type . ' main';
				if($args['shortcode_type'] == 'normal') { // not group shortcode
					if($args['horizontal'] == 'true') {
						$ul_class .= ' horizontal';
						$args['page_depth'] = 1;
						$horizontal_sep = $args['horizontal_separator'];
					}
				}
				echo '<ul class="' . esc_attr($ul_class) . '">';

				// start of the loop
				while ( $sitemap_query->have_posts() ) : $sitemap_query->the_post();

					// Only display public posts
					if( $args['visibility'] == 'public') {
						if( get_post_status( get_the_ID() ) == 'private' ) {
							continue;
						}
					}

					// check if we're on the last post
					if( ($sitemap_query->current_post + 1) == $sitemap_query->post_count ) {
						$horizontal_sep = "";
					}

					$image_html = '';
					if($args['image'] == "true") {
						$image_html = get_the_post_thumbnail( null, array($args['image_size'], $args['image_size']) );
						$image_html = !empty($image_html) ? '<span class="simple-sitemap-fi">' . $image_html . '</span>' : '';
					}

					// @todo can combine this into one line in the future when minimum PHP version allows using function return value as an argument
					$title_text = get_the_title();
					$title_text = WPGO_Simple_Sitemap_Pro_Hooks::simple_sitemap_pro_title_text($title_text);

					$permalink = get_permalink();
					$title = self::get_the_title( $title_text, $permalink, $args );
					$excerpt = $args['show_excerpt'] == 'true' ? '<' . $args['excerpt_tag'] . ' class="excerpt">' . get_the_excerpt() . '</' . $args['excerpt_tag'] . '>' : '';
					$separator_html = ($args['separator'] == "true") ? '<div class="separator"></div>' : '';

					// render list item
					// @todo add this to a template (static method?) so we can reuse it in this and other classes?
					echo '<li>';
					echo $image_html;
					echo $title;
					echo $excerpt;
					echo $separator_html;
					echo $horizontal_sep;
					echo '</li>';

				endwhile; // end of post loop -->

				echo '</ul>';

				echo '</div>'; // .simple-sitemap-wrap

				// put pagination functions here
				wp_reset_postdata();
			endif;

		else:

			$post_type_obj  = get_post_type_object( $post_type );
			$post_type_name = strtolower($post_type_obj->labels->name);

			echo '<p>Sorry, no ' . $post_type_name . ' found.</p>';
			echo '</div>'; // .simple-sitemap-wrap

		endif;

	}

	public function render_sitemap($attr) {

		/* Get attributes from the shortcode. */
		$args = shortcode_atts( array(
			'types' => 'page',
			'show_label' => 'true',
			'show_excerpt' => 'false',
			'container_tag' => 'ul',
			'title_tag' => '',
			'post_type_tag' => 'h3',
			'excerpt_tag' => 'div',
			'links' => 'true',
			'page_depth' => 0,
			'order' => 'asc',
			'orderby' => 'title',
			'exclude' => '',
			'include' => '',
			'nofollow' => 0,
			'visibility' => 'all',
			'render' => '',
			'list_icon' => 'true',
			'separator' => 'false',
			'horizontal' => 'false', // great for adding to footer
			'horizontal_separator' => ', ',
			'image' => 'false',
			'shortcode_type' => 'normal',
			'parent_page_link' => '1', // is this used anymore, or only plugin settings version?
			'id' => '1' // if using multiple sitemaps with tabs then set this to a unique id for each sitemap to avoid CSS id conflicts
		), $attr );

		// escape tag names
		$args['container_tag'] = tag_escape( $args['container_tag'] );
		$args['title_tag'] = tag_escape( $args['title_tag'] );
		$args['excerpt_tag'] = tag_escape( $args['excerpt_tag'] );
		$args['post_type_tag'] = tag_escape( $args['post_type_tag'] );

		// force 'ul' or 'ol' to be used as the container tag
		$allowed_container_tags = array('ul', 'ol');
		if(!in_array($args['container_tag'], $allowed_container_tags)) {
			$args['container_tag'] = 'ul';
		}

		// validate numeric values
		$args['id'] = intval( $args['id'] );
		$args['page_depth'] = intval( $args['page_depth'] );

		$container_format_class = ($args['list_icon'] == "true") ? '' : ' hide-icon';
		$render_class = empty($args['render']) ? '' : ' ' . sanitize_html_class( $args['render'] );

		// ******************
		// ** OUTPUT START **
		// ******************

		// Start output caching (so that existing content in the [simple-sitemap] post doesn't get shoved to the bottom of the post
		ob_start();

		$post_types = array_map( 'trim', explode( ',', $args['types'] ) ); // convert comma separated string to array
		$registered_post_types = get_post_types();

		//echo "<pre>";
		//print_r($post_types);
		//print_r($sitemap_query->posts);
		//echo "</pre>";

		$this->add_separator_script($args['horizontal_separator']);

		$container_classes = 'simple-sitemap-container' . $render_class . $container_format_class;
		echo '<div class="' . esc_attr($container_classes) . '">';

		// conditionally output tab headers
		if( $args['render'] == 'tab' ):

			echo '<ul>'; // tab header open

			// create tab headers
			$header_tab_index = 1; // initialize to 1
			foreach( $post_types as $post_type ) {

				if( !array_key_exists( $post_type, $registered_post_types ) )
					break; // bail if post type isn't valid

				$post_type_label = $this->get_post_type_label($args['show_label'], $post_type, $args['post_type_tag']);
				echo '<li><a href="#simple-sitemap-tab-' . $header_tab_index . '-' . $args['id'] . '">' .  $post_type_label . '</a></li>';

				$header_tab_index++;
			}

			echo '</ul>'; // tab header close

		endif;

		// conditionally create tab panels
		$header_tab_index = 1; // reset to 1
		foreach( $post_types as $post_type ) :

			if( !array_key_exists( $post_type, $registered_post_types ) )
				break; // bail if post type isn't valid

			// set opening and closing title tag
			if( !empty($args['title_tag']) ) {
				$args['title_open'] = '<' . $args['title_tag'] . '>';
				$args['title_close'] = '</' . $args['title_tag'] . '>';
			}
			else {
				$args['title_open'] = $args['title_close'] = '';
			}

			$post_type_label = $this->get_post_type_label($args['show_label'], $post_type, $args['post_type_tag']);

			$list_item_wrapper_class = 'simple-sitemap-wrap' . $render_class;
			$tab_id = ($args['render'] == 'tab') ? ' id="simple-sitemap-tab-' . $header_tab_index . '-' . $args['id'] . '"' : '';
			$header_tab_index++;
			echo '<div ' . $tab_id . 'class="' . esc_attr($list_item_wrapper_class) . '">';
			if( $args['render'] != 'tab' ) echo $post_type_label;

			$query_args = $this->get_query_args($args, $post_type);
			$this->render_list_items($args, $post_type, $query_args);

		endforeach;

		echo '</div>'; // .simple-sitemap-container

		// @todo check we still need this
		echo '<br style="clear: both;">'; // make sure content after the sitemap is rendered properly if taken out

		$sitemap = ob_get_contents();
		ob_end_clean();

		// ****************
		// ** OUTPUT END **
		// ****************

		return $sitemap;
	}

	public function list_pages( $pages, $args ) {

		$output = '';

		if ( empty( $pages ) )
			return $output;

		$class =  'simple-sitemap-page main';
		if($args['shortcode_type'] == 'normal') { // not group shortcode
			if($args['horizontal'] == 'true') {
				$class .= ' horizontal';
				$args['page_depth'] = 1;
			}
		}

		$output = '<ul class="' . $class . '">';
		$output .= $this->walk_page_tree( $pages, $args['page_depth'], $args );
		$output .= '</ul>';
		$output .= '</div>'; // .simple-sitemap-wrap

		return $output;
	}

	public function walk_page_tree( $pages, $depth, $r ) {

		$walker = new WPGO_Walker_Page();
		$walker->ssp_args = $r;

		foreach ( (array) $pages as $page ) {
			if ( $page->post_parent )
				$r['pages_with_children'][ $page->post_parent ] = true;
		}

		$args = array($pages, $depth, $r);
		return call_user_func_array(array($walker, 'walk'), $args);
	}

	/* Shortcode function. */
	public function render_sitemap_group($attr) {

		/* Get attributes from the shortcode. */
		$args = shortcode_atts( array(
			'id' => '1', // if using multiple sitemaps with tabs then set this to a unique id for each sitemap to avoid CSS id conflicts
			'page_depth' => 0,
			'type' => 'post',
			'tax' => 'category', // single taxonomy
			'container_tag' => 'ul',
			'title_tag' => '',
			'excerpt_tag' => 'div',
			'post_type_tag' => 'h3',
			'term_order' => 'asc',
			'term_orderby' => 'name',
			'order' => 'asc',
			'orderby' => 'title',
			'exclude' => '',
			'exclude_terms' => '',
			'render' => '',
			'show_label' => 'true',
			'links' => 'true',
			'list_icon' => 'true',
			'show_excerpt' => 'false',
			'separator' => 'false',
			'horizontal' => 'false',
			'horizontal_separator' => ', ',
			'image' => 'false',
			'shortcode_type' => 'group',
			'nofollow' => 0,
			'parent_page_link' => '1', // is this used anymore, or only plugin settings version?
			'visibility' => 'all'
		), $attr );

		// escape tag names
		$args['container_tag'] = tag_escape( $atts['container_tag'] );
		$args['title_tag'] = tag_escape( $args['title_tag'] );
		$args['excerpt_tag'] = tag_escape( $args['excerpt_tag'] );
		$args['post_type_tag'] = tag_escape( $args['post_type_tag'] );

		// force 'ul' or 'ol' to be used as the container tag
		$allowed_container_tags = array('ul', 'ol');
		if(!in_array($args['container_tag'], $allowed_container_tags)) {
			$args['container_tag'] = 'ul';
		}

		// validate numeric values
		$args['id'] = intval( $args['id'] );

		$container_format_class = ($args['list_icon'] == "true") ? '' : ' hide-icon';
		$render_class = empty($args['render']) ? '' : ' ' . sanitize_html_class( $args['render'] );

		// check post type is valid
		$registered_post_types = get_post_types();
		if( !array_key_exists( $args['type'], $registered_post_types ) )
			return;

		// ******************
		// ** OUTPUT START **
		// ******************

		// Start output caching (so that existing content in the [simple-sitemap] post doesn't get shoved to the bottom of the post
		ob_start();

		$container_classes = 'simple-sitemap-container' . $render_class . $container_format_class;
		echo '<div class="' . esc_attr($container_classes) . '">';

		// set opening and closing title tag
		if( !empty($args['title_tag']) ) {
			$args['title_open'] = '<' . $args['title_tag'] . '>';
			$args['title_close'] = '</' . $args['title_tag'] . '>';
		}
		else {
			$args['title_open'] = $args['title_close'] = '';
		}

		$post_type_label = $this->get_post_type_label($args['show_label'], $args['type'], $args['post_type_tag']);

		$list_item_wrapper_class = 'simple-sitemap-wrap' . $render_class;
		//if( $args['render'] != 'tab' ) echo $post_type_label;
		echo $post_type_label; // remove this line if the one above is reinstated

		$taxonomy_arr = get_object_taxonomies( $args['type'] );

		//echo "<pre>";
		//print_r($taxonomy_arr);
		//print_r($sitemap_query->posts);
		//echo "</pre>";

		// sort via specified taxonomy
		if ( !empty($args['tax']) && in_array( $args['tax'], $taxonomy_arr ) ) {

			$term_attr = array(
				'orderby'           => $args['term_orderby'],
				'order'             => $args['term_order']
			);

			$terms = get_terms( $args['tax'], $term_attr );
			foreach ( $terms as $term ) {

				// get array of taxonomy terms to exclude
				$exclude_terms = array_map( array(&$this, 'process_csl'), explode( ',', $args['exclude_terms']) );

				//echo "<pre>";
				//print_r($term->name);
				//print_r($exclude_terms);
				//echo "</pre>";
				//echo "[" . empty($exclude_terms) . "]";

				$tmp = strtolower($term->name);
				if( in_array($tmp, $exclude_terms) ) {
					continue; // skip to next loop iteration if the current term is to be excluded
				}

				echo '<div class="' . esc_attr($list_item_wrapper_class) . ' ' . esc_attr(strtolower($term->name)) . '">';

				$args['tax_query'] = array(
					array(
						'taxonomy' => $args['tax'],
						'field' => 'slug',
						'terms' => $term ),
				);

				echo '<h3>' . $term->name . '</h3>';

				$query_args = $this->get_query_args($args, $args['type']);
				$this->render_list_items($args, $args['type'], $query_args);
			}
		}
		else {
			echo "No posts found.";
		}

		echo '</div>'; // .simple-sitemap-container

		// @todo check we still need this
		echo '<br style="clear: both;">'; // make sure content after the sitemap is rendered properly if taken out

		$sitemap = ob_get_contents();
		ob_end_clean();

		// ****************
		// ** OUTPUT END **
		// ****************

		return $sitemap;
	}

	/* Shortcode function. */
	public function render_sitemap_child($attr) {

		/* Get attributes from the shortcode. */
		$args = shortcode_atts( array(
			'child_of' => '0',
			'title_li' => '',
			'nofollow' => '0',
			'post_type' => 'page'
		), $attr );

		$args['echo'] = 0; // don't echo out output immediately as we need to optionally add a nofollow attribute

		if ( $args[ 'title_li' ] == '@' ) {
			if ( ! empty( $args[ 'child_of' ] ) ) {
				$args[ 'title_li' ] = "<a href=" . esc_url( get_permalink( $args[ 'child_of' ] ) ) . ">" . get_the_title( $args[ 'child_of' ] ) . "</a>";
			} else {
				$args[ 'title_li' ] = '';
			}
		}

		if ( $args['nofollow'] === '1' ) {
			$list = wp_list_pages( $args );
			$list = stripslashes(self::wp_rel_nofollow( $list ));
		} else {
			$list = wp_list_pages( $args );
		}

		return "<ul class='ssp-top-level'>" . $list . "</ul>";
	}

	/* Shortcode function. */
	public function render_sitemap_taxonomy($attr) {

		/* Get attributes from the shortcode. */
		$args = shortcode_atts( array(
			'include' => '',
			'exclude' => '',
			'depth' => '0',
			'child_of' => '0',
			'title_li' => '',
			'nofollow' => 0,
			'show_count' => '0',
			'orderby' => 'name',
			'order' => 'ASC',
			'taxonomy' => 'category',
			'hide_empty' => '0',
			'echo' => '0'
		), $attr );

		// convert comma separated strings into array (array required in post query)
		$args['include'] = empty( $args['include'] ) ? array() : array_map( 'trim', explode( ',', $args['include']) );
		$args['exclude'] = empty( $args['exclude'] ) ? array() : array_map( 'trim', explode( ',', $args['exclude']) );
		$args['echo'] = 0; // don't echo out output immediately as we need to optionally add a nofollow attribute

		$terms = wp_list_categories( array(
			'hide_empty' => $args['hide_empty'],
			'orderby' => $args['orderby'],
			'order' => $args['order'],
			'show_count' => $args['show_count'],
			'title_li' => '',
			'child_of' => $args['child_of'],
			'depth' => $args['depth'],
			'include' => $args['include'],
			'exclude' => $args['exclude'],
			'taxonomy' => $args['taxonomy'],
			'echo' => $args['echo']
		) );
		return "<ul>" . $terms . "</ul>";
	}

	// process comma separated list of arguments for a shortcode attribute
	public function process_csl($item) {
		$item = trim($item);
		$item = strtolower($item);
		return $item;
	}

	public function get_post_type_label( $show_label, $post_type, $post_type_tag ) {

		$label = '';

		// conditionally show label for each post type
		if( $show_label == 'true' ) {
			$post_type_obj  = get_post_type_object( $post_type );
			$post_type_name = $post_type_obj->labels->name;
			$label = '<' . $post_type_tag . '>' . $post_type_name . '</' . $post_type_tag . '>';
		}

		return $label;
	}

	public static function get_the_title( $title_text, $permalink, $args, $parent_page = false, $parent_page_link = '1' ) {

		$links = $args['links'];
		$title_open = $args['title_open'];
		$title_close = $args['title_close'];
		$nofollow = $args['nofollow'];
		if( $nofollow === '1' ) { $nofollow = ' rel="nofollow"'; } else { $nofollow = ''; }

		if( !empty( $title_text ) ) {
			if ( $links == 'true' && $parent_page === false ) {
				$title = $title_open . '<a href="' . esc_url($permalink) . '"' . $nofollow . '>' . esc_html($title_text) . '</a>' . $title_close;
			} elseif ( $links == 'true' && $parent_page && $parent_page_link != '1' ) {
				$title = $title_open . '<a href="' . esc_url($permalink) . '"' . $nofollow . '>' . esc_html($title_text) . '</a>' . $title_close;
			}else {
				$title = $title_open . esc_html($title_text) . $title_close;
			}
		}
		else {
			if ( $links == 'true' && $parent_page === false ) {
				$title = $title_open . '<a href="' . esc_url($permalink) . '"' . $nofollow . '>' . '(no title)' . '</a>' . $title_close;
			} elseif ( $links == 'true' && $parent_page && $parent_page_link != '1' ) {
				$title = $title_open . '<a href="' . esc_url($permalink) . '"' . $nofollow . '>' . '(no title)' . '</a>' . $title_close;
			} else {
				$title = $title_open . '(no title)' . $title_close;
			}
        }

		return $title;
	}

	// @todo: replace jQuery code with PHP to remove the last separator before it's rendered
	public function add_separator_script( $horizontal_separator ) {
		?>
		<script>
			jQuery(document).ready(function($) {
				$('ul.simple-sitemap-page li:last').html(function(index,html){
					var hs = '<?php echo $horizontal_separator; ?>';
					return html.replace(hs, '');
				});
			});
		</script>
		<?php
	}

	// FUNCTIONS IMPORTED AND CUSTOMISED FROM WP CORE TO ADD REL NOFOLLOW TO INTERNAL LINKS
	public static function wp_rel_nofollow( $text ) {
		// This is a pre save filter, so text is already escaped.
		$text = stripslashes($text);
		$text = preg_replace_callback('|<a (.+?)>|i', array( 'WPGO_Simple_Sitemap_Pro' ,'wp_rel_nofollow_callback'), $text);
		return wp_slash( $text );
	}

	public static function wp_rel_nofollow_callback( $matches ) {
		$text = $matches[1];
		$atts = shortcode_parse_atts( $matches[1] );
		$rel  = 'nofollow';

		// the code below was comment out as it prevents adding nofollow to external links
		/*if ( preg_match( '%href=["\'](' . preg_quote( set_url_scheme( home_url(), 'http' ) ) . ')%i', $text ) ||
		     preg_match( '%href=["\'](' . preg_quote( set_url_scheme( home_url(), 'https' ) ) . ')%i', $text )
		) {
			return "<a $text>";
		}*/

		if ( ! empty( $atts['rel'] ) ) {
			$parts = array_map( 'trim', explode( ' ', $atts['rel'] ) );
			if ( false === array_search( 'nofollow', $parts ) ) {
				$parts[] = 'nofollow';
			}
			$rel = implode( ' ', $parts );
			unset( $atts['rel'] );

			$html = '';
			foreach ( $atts as $name => $value ) {
				$html .= "{$name}=\"$value\" ";
			}
			$text = trim( $html );
		}
		return "<a $text rel=\"$rel\">";
	}

} /* End class definition */

/* Create Plugin class instance */
$wpgo_simple_sitemap_pro_plugin = new WPGO_Simple_Sitemap_Pro();