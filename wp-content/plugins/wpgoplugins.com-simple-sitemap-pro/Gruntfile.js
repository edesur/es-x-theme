'use strict';
module.exports = function(grunt) {

	var fullPath = __dirname;
	var filename = fullPath.replace(/^.*[\\\/]/, '');

// When 'grunt' is run from the command line it:
//
// - Builds the theme pot file.
// - Does an initial build of style.css.
// - Automatically prefixes the generated CSS in style.css with correct browser vendor prefixes.
// - Watches the theme sass folder for any changes to:
//   - *.scss files and rebuilds style.css when any changes are detected.
//   - style.css and adds browser vendor prefixes.

// Project configuration.
grunt.initConfig({

	pkg: grunt.file.readJSON('package.json'),

	// compile theme pot file
	makepot: {
		target: {
			options: {
				domainPath: '/languages',
				mainFile: 'wpgo-simple-sitemap-pro.php',
				potFilename: 'wpgo-simple-sitemap-pro.pot', //filename + '.pot',
				exclude: [
					'.svn/.*',
					'languages/.*',
					'node_modules/.*',
					'Gruntfile.js',
					'package.json'
				],
				processPot: function( pot ) {
                    var translation,
                        excluded_meta = [
                            'Plugin Name of the plugin/theme',
                            'Plugin URI of the plugin/theme',
                            'Author of the plugin/theme',
                            'Author URI of the plugin/theme'
                        ];

                    for ( translation in pot.translations[''] ) {
                        if ( 'undefined' !== typeof pot.translations[''][ translation ].comments.extracted ) {
                            if ( excluded_meta.indexOf( pot.translations[''][ translation ].comments.extracted ) >= 0 ) {
                                console.log( 'Excluded meta: ' + pot.translations[''][ translation ].comments.extracted );
                                delete pot.translations[''][ translation ];
                            }
                        }
                    }

                    return pot;
                },
                type: 'wp-plugin'
			}
		}
	}
 });

// Dependent plugins
grunt.loadNpmTasks( 'grunt-wp-i18n' );

// Default and custom task(s)
grunt.registerTask( 'default', [ 'makepot' ] );

};