<?php

namespace ACA\Types\Filtering\Post;

use ACA\Types\Column;
use ACP;

/**
 * @property Column $column
 */
class ParentPost extends ACP\Filtering\Model\Meta {

	public function get_filtering_data() {
		$values = $this->get_meta_values();

		return array(
			'empty_option' => true,
			'options'      => acp_filtering()->helper()->get_post_titles( $values ),
		);
	}

}