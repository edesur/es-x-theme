<?php

namespace ACA\Types\Field;

use ACA\Types\Editing;
use ACA\Types\Field;
use ACA\Types\Filtering;
use ACA\Types\Search;
use ACP;
use ACP\Sorting;

class Select extends Field {

	public function editing() {
		return new Editing\Select( $this->column );
	}

	public function sorting() {
		return new Sorting\Model\Meta( $this->column );
	}

	public function filtering() {
		return new Filtering\Select( $this->column );
	}

	public function search() {
		return new Search\Select(
			$this->column->get_meta_key(),
			$this->column->get_meta_type(),
			$this->get_field_options()
		);
	}

	public function export() {
		return new ACP\Export\Model\StrippedValue( $this->column );
	}

	/**
	 * @return array
	 */
	private function get_field_options() {
		$result = array();
		$options = (array) $this->get( 'options' );

		foreach ( $options as $option ) {
			if ( ! is_array( $option ) ) {
				continue;
			}

			$result[ $option['value'] ] = $option['title'];
		}

		return $result;
	}

}