<?php

namespace ACA\Types\Field;

use ACA\Types\Editing;
use ACA\Types\Field;
use ACA\Types\Filtering;
use ACP\Search\Comparison;
use ACP\Sorting;

class Numeric extends Field {

	public function sorting() {
		$model = new Sorting\Model\Meta( $this->column );
		$model->set_data_type( 'numeric' );

		return $model;
	}

	public function editing() {
		return new Editing\Numeric( $this->column );
	}

	public function filtering() {
		return new Filtering\Numeric( $this->column );
	}

	public function search() {
		return new Comparison\Meta\Numeric(
			$this->column->get_meta_key(),
			$this->column->get_meta_type()
		);
	}

}