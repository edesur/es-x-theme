<?php

namespace ACA\Types\Field;

use ACA\Types\Editing;
use ACA\Types\Export;
use ACA\Types\Field;
use ACA\Types\Filtering;
use ACA\Types\Search;
use ACP\Sorting;

class Checkboxes extends Field {

	public function get_value( $id ) {
		$labels = $this->get_values_as_labels( $id );

		return ac_helper()->html->small_block( $labels );
	}

	public function get_values_as_labels( $id ) {
		$raw = $this->get_raw_value( $id );

		if ( ! $raw ) {
			return false;
		}

		$options = $this->get( 'options' );

		if ( ! $options ) {
			return false;
		}

		// Checkbox keys
		$keys = array();
		foreach ( $raw as $value ) {
			$keys[] = $value[0];
		}

		// Checkbox Labels
		$labels = array();
		foreach ( $options as $option ) {
			if ( in_array( $option['set_value'], $keys ) ) {
				$labels[ $option['set_value'] ] = $option['title'];
			}
		}

		return $labels;
	}

	public function is_serialized() {
		return true;
	}

	public function filtering() {
		return new Filtering\Checkboxes( $this->column );
	}

	public function editing() {
		return new Editing\Checkboxes( $this->column );
	}

	public function sorting() {
		return new Sorting\Model\Meta( $this->column );
	}

	public function export() {
		return new Export\Field\Checkboxes( $this->column );
	}

	public function search() {
		return new Search\Checkboxes(
			$this->column->get_meta_key(),
			$this->column->get_meta_type(),
			$this->get_field_options()
		);
	}

	private function get_field_options() {
		$result = array();

		$options = (array) $this->get( 'options' );

		foreach ( $options as $option ) {
			$result[ $option['set_value'] ] = $option['title'];
		}

		return $result;
	}

}