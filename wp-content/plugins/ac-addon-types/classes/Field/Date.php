<?php

namespace ACA\Types\Field;

use AC;
use ACA\Types\Editing;
use ACA\Types\Export;
use ACA\Types\Field;
use ACA\Types\Filtering;
use ACP\Sorting;
use ACP\Search;

class Date extends Field {

	public function get_value( $id ) {
		$time = $this->get_raw_value( $id );

		if ( ! $time ) {
			return false;
		}

		return $this->column->get_formatted_value( date( 'c', $time ) );
	}

	public function editing() {
		return new Editing\Date( $this->column );
	}

	public function filtering() {
		return new Filtering\Date( $this->column );
	}

	public function sorting() {
		$model = new Sorting\Model\Meta( $this->column );
		$model->set_data_type( 'numeric' );

		return $model;
	}

	public function export() {
		return new Export\Field\Date( $this->column );
	}

	public function search() {
		return new Search\Comparison\Meta\DateTime\Timestamp( $this->column->get_meta_key(), $this->column->get_meta_type() );
	}

	public function get_dependent_settings() {
		return array(
			new AC\Settings\Column\Date( $this->column ),
		);
	}

}