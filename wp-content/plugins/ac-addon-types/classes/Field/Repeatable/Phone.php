<?php

namespace ACA\Types\Field\Repeatable;

use ACA\Types\Editing;
use ACA\Types\Field;

class Phone extends Field\Phone {

	public function editing() {
		return new Editing\Repeatable( $this->column );
	}

}