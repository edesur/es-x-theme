<?php

namespace ACA\Types\Field\Repeatable;

use ACA\Types\Editing;
use ACA\Types\Field;

class File extends Field\File {

	public function get_value( $id ) {
		$urls = $this->get_raw_value( $id );

		if ( ! $urls ) {
			return false;
		}

		$values = array();

		foreach ( (array) $urls as $url ) {
			$label = $url;

			$upload_dir = wp_upload_dir();

			if ( $upload_dir ) {
				$label = str_replace( $upload_dir['baseurl'], '', $url );
			}

			$values[] = ac_helper()->html->link( $url, $label );
		}

		return ac_helper()->html->small_block( $values );
	}

	public function editing() {
		return new Editing\Repeatable\File( $this->column );
	}

}