<?php

namespace ACA\Types\Field\Repeatable;

use ACA\Types\Editing;
use ACA\Types\Field;

class Date extends Field\Date {

	public function get_value( $id ) {
		$values = array();

		foreach ( (array) $this->get_raw_value( $id ) as $timestamp ) {
			$values[] = $this->column->get_formatted_value( date( 'c', $timestamp ) );
		}

		return ac_helper()->html->small_block( $values );
	}

	public function editing() {
		return new Editing\Repeatable\Date( $this->column );
	}

}