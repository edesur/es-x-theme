<?php

namespace ACA\Types\Field\Repeatable;

use ACA\Types\Editing;
use ACA\Types\Field;

class Textarea extends Field\Textarea {

	public function get_value( $id ) {
		$values = array();

		foreach ( (array) $this->get_raw_value( $id ) as $string ) {
			$values[] = $this->column->get_formatted_value( $string );
		}

		return ac_helper()->html->small_block( $values );
	}

	public function editing() {
		return new Editing\Repeatable\Textarea( $this->column );
	}

}