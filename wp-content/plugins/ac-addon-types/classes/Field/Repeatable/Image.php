<?php

namespace ACA\Types\Field\Repeatable;

use ACA\Types\Editing;
use ACA\Types\Field;

class Image extends Field\Image {

	public function editing() {
		return new Editing\Repeatable\File( $this->column );
	}

}