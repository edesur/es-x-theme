<?php

namespace ACA\Types\Field\Repeatable;

use ACA\Types\Editing;
use ACA\Types\Field;

class Embed extends Field\Embed {

	public function get_value( $id ) {
		$values = array();

		foreach ( (array) $this->get_raw_value( $id ) as $url ) {
			$values[] = $this->column->get_formatted_value( $url );
		}

		return ac_helper()->html->small_block( $values );
	}

	public function editing() {
		return new Editing\Repeatable( $this->column );
	}

}