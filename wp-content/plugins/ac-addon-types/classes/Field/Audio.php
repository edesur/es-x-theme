<?php

namespace ACA\Types\Field;

use ACA\Types\Editing;

class Audio extends File {

	public function editing() {
		return new Editing\Audio( $this->column );
	}

}