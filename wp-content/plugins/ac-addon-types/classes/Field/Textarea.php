<?php

namespace ACA\Types\Field;

use AC;
use ACA\Types\Editing;
use ACA\Types\Field;
use ACA\Types\Filtering;
use ACP\Search\Comparison;
use ACP\Sorting;

class Textarea extends Field {

	public function get_value( $id ) {
		return $this->column->get_formatted_value( $this->get_raw_value( $id ) );
	}

	public function editing() {
		return new Editing\Textarea( $this->column );
	}

	public function sorting() {
		return new Sorting\Model\Meta( $this->column );
	}

	public function filtering() {
		return new Filtering( $this->column );
	}

	public function search() {
		return new Comparison\Meta\Text(
			$this->column->get_meta_key(),
			$this->column->get_meta_type()
		);
	}

	public function get_dependent_settings() {
		return array( new AC\Settings\Column\WordLimit( $this->column ) );
	}

}