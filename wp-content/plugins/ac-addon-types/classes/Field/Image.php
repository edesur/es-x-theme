<?php

namespace ACA\Types\Field;

use AC;
use ACA\Types\Editing;
use ACA\Types\Filtering;

class Image extends File {

	public function get_value( $id ) {
		$ids = array_unique( (array) $this->get_raw_value( $id ) );
		$values = array();

		foreach ( $ids as $url ) {
			$values[] = $this->column->get_formatted_value( $this->get_attachment_id_by_url( $url ) );
		}

		return implode( $values );
	}

	public function get_dependent_settings() {
		$image = new AC\Settings\Column\Image( $this->column );
		$image->set_default( 'cpac-custom' );

		return array( $image );
	}

	public function editing() {
		return new Editing\Image( $this->column );
	}

	public function filtering() {
		return new Filtering\Image( $this->column );
	}

}