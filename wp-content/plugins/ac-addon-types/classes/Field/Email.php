<?php

namespace ACA\Types\Field;

use ACA\Types\Editing;
use ACA\Types\Field;
use ACA\Types\Filtering;
use ACP\Search\Comparison;
use ACP\Sorting;

class Email extends Field {

	public function editing() {
		return new Editing\Email( $this->column );
	}

	public function sorting() {
		return new Sorting\Model\Meta( $this->column );
	}

	public function filtering() {
		return new Filtering( $this->column );
	}

	public function search() {
		return new Comparison\Meta\Text(
			$this->column->get_meta_key(),
			$this->column->get_meta_type()
		);
	}

}