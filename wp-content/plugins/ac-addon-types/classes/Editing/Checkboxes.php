<?php

namespace ACA\Types\Editing;

use ACA\Types\Editing;

class Checkboxes extends Editing {

	public function get_edit_value( $id ) {
		$raw_value = parent::get_edit_value( $id );

		$values = array();
		if ( $raw_value ) {
			foreach ( $raw_value as $value ) {
				$values[] = $value[0];
			}
		}

		return $values;
	}

	public function get_view_settings() {
		return array(
			'type'    => 'checklist',
			'options' => $this->get_choices(),
		);
	}

	public function get_choices() {
		$options = array();

		foreach ( (array) $this->column->get_field()->get( 'options' ) as $option ) {
			$options[ $option['set_value'] ] = $option['title'];
		}

		return $options;
	}

	/**
	 * @param int   $id
	 * @param array $value
	 *
	 * @return bool
	 */
	public function save( $id, $value ) {
		$values = array();

		foreach ( (array) $this->column->get_field()->get( 'options' ) as $key => $option ) {
			if ( in_array( $option['set_value'], $value ) ) {
				$values[ $key ][] = $option['set_value'];
			}
		}

		return parent::save( $id, $values );
	}

}