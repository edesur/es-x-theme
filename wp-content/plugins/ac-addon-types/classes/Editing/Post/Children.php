<?php

namespace ACA\Types\Editing\Post;

use AC;
use ACA\Types\Column;
use ACP;

/**
 * @property Column\Post\Children $column
 */
class Children extends ACP\Editing\Model implements ACP\Editing\PaginatedOptions {

	public function __construct( Column\Post\Children $column ) {
		parent::__construct( $column );
	}

	public function get_view_settings() {
		return array(
			'type'               => 'select2_dropdown',
			'ajax_populate'      => true,
			'multiple'           => true,
			'clear_button'       => true,
			'store_single_value' => true,
		);
	}

	public function get_paginated_options( $s, $paged, $id = null ) {
		$args = array(
			's'          => $s,
			'post_type'  => $this->column->get_child_post_type(),
			'paged'      => $paged,
			'meta_query' => array(
				'relation' => 'OR',
				array(
					'key'   => $this->column->get_meta_key(),
					'value' => '',
				),
				array(
					'key'     => $this->column->get_meta_key(),
					'compare' => 'NOT EXISTS',
				),
			),
		);

		$entities = new ACP\Helper\Select\Entities\Post( $args );

		return new AC\Helper\Select\Options\Paginated(
			$entities,
			new ACP\Helper\Select\Formatter\PostTitle( $entities )
		);
	}

	public function get_edit_value( $id ) {
		$post_ids = parent::get_edit_value( $id );
		$value = array();

		if ( $post_ids ) {
			foreach ( (array) $post_ids as $_id ) {
				$value[ $_id ] = get_post_field( 'post_title', $_id );
			}
		}

		return $value;
	}

	/**
	 * @param int   $id
	 * @param array $value
	 *
	 * @return bool
	 */
	public function save( $id, $value ) {
		$old_ids = $this->column->get_raw_value( $id );

		foreach ( $old_ids as $post_id ) {
			if ( ! in_array( $post_id, $value ) ) {
				delete_post_meta( $post_id, $this->column->get_meta_key(), $id );
			}
		}

		foreach ( $value as $post_id ) {
			update_post_meta( $post_id, $this->column->get_meta_key(), $id );
		}

		return true;
	}

}