<?php

namespace ACA\Types\Editing\Post;

use AC;
use ACA\Types\Column;
use ACP;

/**
 * @property Column\Post\ParentPost $column
 */
class ParentPost extends ACP\Editing\Model\Meta implements ACP\Editing\PaginatedOptions {

	public function __construct( Column\Post\ParentPost $column ) {
		parent::__construct( $column );
	}

	public function get_view_settings() {
		return array(
			'type'               => 'select2_dropdown',
			'ajax_populate'      => true,
			'multiple'           => false,
			'clear_button'       => true,
			'store_single_value' => true,
		);
	}

	public function get_paginated_options( $s, $paged, $id = null ) {
		$args = array(
			's'         => $s,
			'post_type' => $this->column->get_available_parent_post_types(),
			'paged'     => $paged,
		);

		$entities = new ACP\Helper\Select\Entities\Post( $args );

		return new AC\Helper\Select\Options\Paginated(
			$entities,
			new ACP\Helper\Select\Group\PostType(
				new ACP\Helper\Select\Formatter\PostTitle( $entities )
			)
		);
	}

	public function get_edit_value( $id ) {
		$edit_value = parent::get_edit_value( $id );
		if ( ! $edit_value ) {
			return false;
		}

		if ( ! $post = get_post( parent::get_edit_value( $id ) ) ) {
			return false;
		}

		return array(
			$post->ID => $post->post_title,
		);
	}

}