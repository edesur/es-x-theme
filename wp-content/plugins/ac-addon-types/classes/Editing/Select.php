<?php

namespace ACA\Types\Editing;

use ACA\Types\Editing;

class Select extends Editing {

	public function get_view_settings() {
		return array(
			'type'    => 'select',
			'options' => $this->get_choices(),
		);
	}

	protected function get_choices() {
		$options = $this->column->get_field()->get( 'options' );

		if ( ! $options ) {
			return false;
		}

		$choices = array();

		if ( 'no-default' === $options['default'] ) {
			$choices[] = __( '--- not set ---', 'wpv-views' );
		}

		foreach ( $options as $key => $option ) {
			if ( 'default' === $key ) {
				continue;
			}

			$choices[ $option['value'] ] = $option['title'];
		}

		return $choices;
	}

}