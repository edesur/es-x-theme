<?php

namespace ACA\Types\Editing\Repeatable;

use ACA\Types\Editing\Repeatable;

class Date extends Repeatable {

	public function get_view_settings() {
		$data = parent::get_view_settings();

		$data['subtype'] = 'date';

		return $data;
	}

	public function get_edit_value( $id ) {
		$timestamps = parent::get_edit_value( $id );

		if ( ! $timestamps ) {
			return false;
		}

		$dates = array();
		foreach ( (array) $timestamps as $timestamp ) {
			if ( ! $timestamp ) {
				continue;
			}

			$dates[] = date( 'Y-m-d', $timestamp );
		}

		return $dates;
	}

	public function save( $id, $values ) {
		$formatted_values = array();

		foreach ( (array) $values as $value ) {
			$formatted_values[] = strtotime( $value );
		}

		return parent::save( $id, $formatted_values );
	}

}