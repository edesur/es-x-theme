<?php

namespace ACA\Types\Editing\Repeatable;

use ACA\Types\Editing\Repeatable;

class Url extends Repeatable {

	public function get_view_settings() {
		$data = parent::get_view_settings();

		$validation = $this->column->get_field()->get( 'validate' );

		if ( $validation && isset( $validation['url']['active'] ) && 1 === (int) $validation['url']['active'] ) {
			$data['subtype'] = 'url';
		}

		return $data;
	}

}