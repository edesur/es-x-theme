<?php

namespace ACA\Types\Editing;

class Audio extends File {

	public function get_view_settings() {
		$data = parent::get_view_settings();
		$data['attachment']['library']['type'] = 'audio';

		return $data;
	}

}