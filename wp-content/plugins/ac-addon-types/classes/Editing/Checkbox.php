<?php

namespace ACA\Types\Editing;

use ACA\Types\Editing;

class Checkbox extends Editing {

	public function get_view_settings() {
		$value = $this->column->get_field()->get( 'set_value' );

		// todo: test
		return array(
			'type'    => 'togglable',
			'options' => array(
				''     => __( 'False' ),
				$value => $value,
			),
		);
	}

}