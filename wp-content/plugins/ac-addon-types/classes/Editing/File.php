<?php

namespace ACA\Types\Editing;

use ACA\Types\Editing;
use ACA\Types\Field;

class File extends Editing {

	public function get_view_settings() {
		$data = parent::get_view_settings();
		$data['type'] = 'media';

		if ( ! $this->column->get_field()->is_required() ) {
			$data['clear_button'] = true;
		}

		return $data;
	}

	public function get_edit_value( $id ) {
		$field = $this->column->get_field();

		if ( ! $field instanceof Field\File ) {
			return false;
		}

		return $field->get_attachment_id_by_url( $field->get_raw_value( $id ) );
	}

	/**
	 * @param int $id
	 * @param int $attachment_id
	 *
	 * @return bool
	 */
	public function save( $id, $attachment_id ) {
		return parent::save( $id, wp_get_attachment_url( $attachment_id ) );
	}

}