<?php

namespace ACA\Types\Editing;

use ACA\Types\Editing;

class Date extends Editing {

	public function get_view_settings() {
		$data = parent::get_view_settings();

		$data['type'] = 'date';

		if ( $this->has_time() ) {
			$data['type'] = 'date_time';
		}

		return $data;
	}

	public function get_edit_value( $id ) {
		$timestamp = parent::get_edit_value( $id );
		$format = 'Ymd';

		if ( ! $timestamp ) {
			return false;
		}

		if ( $this->has_time() ) {
			$format = 'Y-m-d h:i:s';
		}

		// Single. Format for date picker.
		return date( $format, $timestamp );
	}

	/**
	 * Store as timestamp
	 *
	 * @param int    $id
	 * @param string $value
	 */
	public function save( $id, $value ) {
		return parent::save( $id, strtotime( $value ) );
	}

	/**
	 * @return bool
	 */
	private function has_time() {
		return 'and_time' === $this->column->get_field()->get( 'date_and_time' );
	}

}