<?php

namespace ACA\Types\Editing;

use ACA\Types\Editing;

class Disabled extends Editing {

	public function is_active() {
		return false;
	}

	public function register_settings() {
	}

}