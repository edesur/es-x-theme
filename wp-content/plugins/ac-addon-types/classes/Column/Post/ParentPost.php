<?php

namespace ACA\Types\Column\Post;

use AC;
use ACA\Types\Editing;
use ACA\Types\Filtering;
use ACA\Types\Settings;
use ACP;

class ParentPost extends AC\Column\Meta
	implements ACP\Editing\Editable, ACP\Filtering\Filterable, ACP\Export\Exportable {

	public function __construct() {
		$this->set_type( 'column-types_parent' )
		     ->set_label( 'Toolset Types - Parent' )
		     ->set_group( 'types' );
	}

	public function is_valid() {
		// Only works with the old relationship manager
		if ( apply_filters( 'toolset_is_m2m_enabled', false ) ) {
			return false;
		}

		$post_types = $this->get_available_parent_post_types();

		if ( empty( $post_types ) ) {
			return false;
		}

		return true;
	}

	public function get_value( $id ) {
		$parent_id = $this->get_raw_value( $id );

		if ( ! $parent_id ) {
			return $this->get_empty_char();
		}

		return $this->get_formatted_value( $parent_id, $parent_id );
	}

	protected function register_settings() {
		$this->add_setting( new Settings\ParentPostType( $this ) );
		$this->add_setting( new AC\Settings\Column\Post( $this ) );
	}

	public function editing() {
		return new Editing\Post\ParentPost( $this );
	}

	public function filtering() {
		return new Filtering\Post\ParentPost( $this );
	}

	public function export() {
		return new ACP\Export\Model\StrippedValue( $this );
	}

	public function get_available_parent_post_types() {
		$belongs_to = wpcf_pr_admin_get_belongs( $this->get_post_type() );

		if ( empty( $belongs_to ) || ! is_array( $belongs_to ) ) {
			return array();
		}

		return array_keys( $belongs_to );
	}

	public function get_meta_key() {
		return '_wpcf_belongs_' . $this->get_setting( 'post_type' )->get_value() . '_id';
	}

}