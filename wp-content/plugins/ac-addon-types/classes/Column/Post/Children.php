<?php

namespace ACA\Types\Column\Post;

use AC;
use AC\Collection;
use ACA\Types\Editing;
use ACA\Types\Export;
use ACA\Types\Settings;
use ACP;

class Children extends AC\Column
	implements ACP\Editing\Editable, ACP\Export\Exportable {

	public function __construct() {
		$this->set_type( 'column-types_children' )
		     ->set_label( 'Toolset Types - Children' )
		     ->set_group( 'types' );
	}

	public function is_valid() {
		// Only works with the old relationship manager
		if ( apply_filters( 'toolset_is_m2m_enabled', false ) ) {
			return false;
		}

		$post_types = $this->get_available_child_post_types();

		if ( empty( $post_types ) ) {
			return false;
		}

		return true;
	}

	public function get_value( $id ) {
		$values = $this->get_formatted_values( $id );

		if ( ! $values ) {
			return $this->get_empty_char();
		}

		return $values->implode( '<br>' );
	}

	/**
	 * @param $id
	 *
	 * @return Collection
	 */
	public function get_formatted_values( $id ) {
		$values = new Collection();

		foreach ( $this->get_raw_value( $id ) as $post_id ) {
			$values->push( $this->get_formatted_value( $post_id, $post_id ) );
		}

		return $values;
	}

	public function get_raw_value( $id ) {
		$posts = get_posts( array(
			'fields'     => 'ids',
			'post_type'  => $this->get_child_post_type(),
			'meta_query' => array(
				array(
					'key'   => $this->get_meta_key(),
					'value' => $id,
				),
			),
		) );

		return $posts;
	}

	public function editing() {
		return new Editing\Post\Children( $this );
	}

	public function export() {
		return new Export\Children( $this );
	}

	protected function register_settings() {
		$this->add_setting( new Settings\ChildPostType( $this ) );
		$this->add_setting( new AC\Settings\Column\Post( $this ) );
	}

	// Helpers

	public function get_meta_key() {
		return '_wpcf_belongs_' . $this->get_post_type() . '_id';
	}

	public function get_available_child_post_types() {
		$has_children = wpcf_pr_admin_get_has( $this->get_post_type() );

		if ( empty( $has_children ) || ! is_array( $has_children ) ) {
			return array();
		}

		return array_keys( $has_children );
	}

	public function get_child_post_type() {
		return $this->get_setting( 'post_type' )->get_value();
	}

}