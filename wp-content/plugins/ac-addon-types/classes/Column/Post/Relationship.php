<?php

namespace ACA\Types\Column\Post;

use AC;
use ACA\Types\Settings;
use IToolset_Relationship_Definition;

class Relationship extends AC\Column {

	public function __construct() {
		$this->set_type( 'column-types_relationship' )
		     ->set_label( 'Toolset Types - Relationship' )
		     ->set_group( 'types' );
	}

	public function get_value( $id ) {
		$raw_value = $this->get_raw_value( $id );

		if ( ! $raw_value ) {
			return $this->get_empty_char();
		}

		return parent::get_value( $id );
	}

	public function get_raw_value( $id ) {
		$relationship = $this->get_relationship_setting()->get_relationship_object();

		if ( ! $relationship ) {
			return false;
		}

		return new AC\Collection( toolset_get_related_posts( $id, $this->get_relationship_setting()->get_relationship(), $this->get_relationship_type( $relationship ) ) );
	}

	public function is_valid() {
		return apply_filters( 'toolset_is_m2m_enabled', false );
	}

	public function register_settings() {
		$this->add_setting( new Settings\Relationship( $this ) );
	}

	/**
	 * @param IToolset_Relationship_Definition $relationship
	 *
	 * @return bool|string
	 */
	private function get_relationship_type( $relationship ) {
		$parent_type = $relationship->get_parent_type();

		if ( ! in_array( $this->get_post_type(), $parent_type->get_types() ) ) {
			return 'child';
		}

		return 'parent';

	}

	/**
	 * @return false|Settings\Relationship
	 */
	public function get_relationship_setting() {
		$setting = $this->get_setting( 'relationship' );

		if ( ! $setting instanceof Settings\Relationship ) {
			return false;
		}

		return $setting;
	}

}