<?php

namespace ACA\Types\Export;

use ACA\Types\Column;
use ACP;

/**
 * @property Column\Post\Children $column
 */
class Children extends ACP\Export\Model {

	/**
	 * @param Column\Post\Children $column
	 */
	public function __construct( Column\Post\Children $column ) {
		parent::__construct( $column );
	}

	public function get_value( $id ) {
		$values = $this->column->get_formatted_values( $id );

		if ( ! $values ) {
			return false;
		}

		foreach ( $values as $k => $value ) {
			$value = strip_tags( $value );
			$values->put( $k, $value );
		}

		return $values->implode( ', ' );
	}

}