== Changelog ==

= 1.5.4 =
Release Date: August 30th, 2019

* [Fixed] Relation column fixed and made available again

= 1.5.3 =
Release Date: August 27th, 2019

* [Removed] Removed Relationship column for now, since it is not compatible anymore with Types 3.3
* [Improved] Updated deprecated methods
* [Dependency] Requires Admin Columns Pro 4.7.1

= 1.5.2 =
Release Date: February 12th, 2019

* [Fixed] Rebuid of 1.5.1
* [Fixed] Removed reference to deprecated xeditable file

= 1.5.1 =
Release Date: February 12th, 2019

* [Fixed] Removed reference to deprecated xeditable file

= 1.5 =
Release Date: January 24th, 2019

* [Added] Bulk Edit
* [Improved] Now uses the Smart Filtering logic from the core

= 1.4 =
Release Date: November 14th, 2018

* [Added] Smart Filtering

= 1.3.3 =
Release Date: October 31st, 2018

* [Fixed] Sorting on date fields not works as expected
* [Improved] Made compatible with Admin Columns Pro 4.4, prepared for Smart Filtering

= 1.3.2 =
Release Date: July 20th, 2018

* [Added] New relationship column that works with the new Types Relationship API

= 1.3.1 =
Release Date: June 29th, 2018

* [Fixed] Removed wrong api file

= 1.3 =
Release Date: June 25th, 2018

* [Improved] Removed the old relationship columns when using the new Toolset version
* [Improved] Support date time editing for date fields
* [Important] Dropped PHP 5.2 support

= 1.2.1 =
Release Date: March 7th, 2018

* [Improved] Adds a clear button for inline edit columns with a value

= 1.2 =
Release Date: January 29th, 2018

* [Important] Only compatible with Admin Columns Pro 4.2 and higher

= 1.1 =
Release Date: December 13th, 2017

* [Added] Support for the Toolset Types Parent/Child relationships
* [Added] Parent column added for Parent/Child relations
* [Added] Children column added for Parent/Child relations
* [Improved] Made compatible with the Admin Columns Pro Export feature
* [Improved] Changed numeric input type to text input type for the Types numeric field

= 1.0.4 =
Release Date: July 3rd, 2017

* [Improved] Meta changes
* [Improved] Optimized performance for edit columns screen
* [Improved] Types fields that don't have a specific group attached are now also available to choose from

= 1.0.3 =
Release Date: May 3rd, 2017

* [Fixed] Date filtering fixed
* [Improved] Multi input editing script improvements

= 1.0.2 =
Release Date: April 25th, 2017

* [Important] Only compatible with Admin Columns Pro 4.0.3
* [Improved] Uses label instead of values in filtering dropdown for Types Select field

= 1.0.1 =
Release Date: April 11th, 2017

* [Improved] Editing a Types numeric field now only accepts numbers
* [Improved] The first selected Types field now contains the correct settings

= 1.0 =
Release Date: April 4th, 2017

* [Added] Initial release of the Types add-on, works with Admin Columns version 4.0 and higher