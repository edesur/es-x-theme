<?php

if (!defined('ABSPATH')) die('No direct access allowed');

// works since WP 4.6
if (function_exists('add_filter')) {
	add_filter('wpo_cache_cookies', 'wpo_cache_cookies_add_wc_currency_converter');
	add_filter('wpo_cache_query_variables', 'wpo_cache_query_variables_woocommerce');
}

/**
 * Filter GET query variable names used for building cache file name.
 *
 * @param array $variables
 *
 * @return array
 */
function wpo_cache_query_variables_woocommerce($variables) {

	// WPML multi currency plugin.
	if (defined('WCML_VERSION')) {
		$variables[] = 'wcmlc';
	}

	// check if active WPML plugin.
	if (defined('ICL_SITEPRESS_VERSION')) {
		$variables[] = 'lang';
	}

	// Aelia Currency switcher
	if (class_exists('WC_Aelia_CurrencySwitcher')) {
		$variables[] = 'aelia_cs_currency';
	}

	return $variables;
}

/**
 * Filter COOKIE variable names used for building cache file name.
 *
 * @param array $cookies
 *
 * @return array
 */
function wpo_cache_cookies_add_wc_currency_converter($cookies) {

	// Aelia currency switcher
	if (class_exists('WC_Aelia_CurrencySwitcher')) {
		$cookies[] = 'aelia_cs_selected_currency';
	}
	
	return $cookies;
}
