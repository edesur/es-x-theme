��          \      �       �   	   �      �   !   �        :   
  (   E     n     �     �     �  %   �  
   �  O   �  +   E     q                                       Dark Mode David Gwyer Enable Dark Mode in the dashboard Feedback Lets your users make the WordPress admin dashboard darker. https://wordpress.org/plugins/dark-mode/ https://www.wpgoplugins.com/ PO-Revision-Date: 2020-01-29 10:47:30+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.4.0-alpha
Language: es
Project-Id-Version: Plugins - Dark Mode - Stable (latest release)
 Modo oscuro David Gwyer Activar el modo oscuro del escritorio Respuestas Permite a los usuarios de WordPress oscurecer el escritorio de administración. https://es.wordpress.org/plugins/dark-mode/ https://www.wpgoplugins.com/ 